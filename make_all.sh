cd "$(dirname "$0")"

find . -name "*.o" -type f -delete
find . -name "*.a" -type f -delete

cd ./library/
for d in ./*/
do
	cd $d
	if [ -e "Makefile" ]; then
		echo " "
		echo "\033[33m----------------------------------------\033[0m"
		echo " Make library on \033[32m"$d
		echo "\033[33m----------------------------------------\033[0m"
		make clean
		make -j4
		cd ..
	else
		echo " "
		echo "Makefile"
		echo "\033[33m----------------------------------------\033[0m"
		echo " No makefile exist, removing \033[32m"$d
		echo "\033[33m----------------------------------------\033[0m"
		cd ..
		rm -r $d
	fi
done

cd ../project/
for d in ./*/
do
	cd $d
	if [ -e "Makefile" ]; then
		echo " "
		echo "\033[33m----------------------------------------\033[0m"
		echo " Make project on \033[32m"$d
		echo "\033[33m----------------------------------------\033[0m"
		make clean
		make -j4
		cd ..
	else
		echo " "
		echo "Makefile"
		echo "\033[33m----------------------------------------\033[0m"
		echo " No makefile exist, removing \033[32m"$d
		echo "\033[33m----------------------------------------\033[0m"
		cd ..
		rm -r $d
	fi
done

echo " "
cd ../library/
for d in ./*/
do
	if [ ! -e $d"lib.a" ]
	then
		echo "\033[31mMake library on \033[33m"$d"\033[31m failed\033[0m"
	fi
done

cd ../project/
for d in ./*/
do
	if [ ! -e $d"run" ]
	then
		echo "\033[31mMake project on \033[33m"$d"\033[31m failed\033[0m"
	fi
done
echo " "
