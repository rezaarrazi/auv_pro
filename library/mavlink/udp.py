import socket
from collections import namedtuple
from struct import *
import threading
import time
from MAVLink import MAVLink

from os import system, name 

class udpMavlink:
    RANGE_CHANNEL = 0x00000000

    KEY_ROBOT_CHANNEL_PITCH             = RANGE_CHANNEL + 0
    KEY_ROBOT_CHANNEL_ROLL              = RANGE_CHANNEL + 1
    KEY_ROBOT_CHANNEL_THROTTLE          = RANGE_CHANNEL + 2
    KEY_ROBOT_CHANNEL_YAW               = RANGE_CHANNEL + 3
    KEY_ROBOT_CHANNEL_FORWARD           = RANGE_CHANNEL + 4
    KEY_ROBOT_CHANNEL_LATERAL           = RANGE_CHANNEL + 5

    exitFlag = 0
    
    def __init__(self, UDP_IP, UDP_PORT):
        self.sock = socket.socket(socket.AF_INET, # Internet
                        socket.SOCK_DGRAM) # UDP
        self.sock.bind((UDP_IP, UDP_PORT))

        self.CHANNEL_PITCH    = 1500
        self.CHANNEL_ROLL     = 1500
        self.CHANNEL_THROTTLE = 1500
        self.CHANNEL_YAW      = 1500
        self.CHANNEL_FORWARD  = 1500
        self.CHANNEL_LATERAL  = 1500

    def update_data(self, autopilot):
        self.autopilot = autopilot

        system('clear') 
        print("[CHANNEL_PITCH] %d, [CHANNEL_ROLL] %d, [CHANNEL_THROTTLE] %d"% (self.CHANNEL_PITCH, self.CHANNEL_ROLL, self.CHANNEL_THROTTLE))
        print("[CHANNEL_YAW] %d, [CHANNEL_FORWARD] %d, [CHANNEL_LATERAL] %d"% (self.CHANNEL_YAW, self.CHANNEL_FORWARD, self.CHANNEL_LATERAL))

        data, addr = self.sock.recvfrom(1024) # buffer size is 1024 bytes
        MessageData = namedtuple('MessageData', 'header version length flags')._make(unpack('LHHL', data[:12]))
        
        keyval_data = data[12:]
        keyval_data_size = 8
        for i in range(MessageData.length):
            KeyValData = namedtuple('KeyValData', 'key value')._make(unpack('Ll', keyval_data[keyval_data_size*(i):keyval_data_size*(i+1)]))
            
            if KeyValData.key == self.KEY_ROBOT_CHANNEL_PITCH:
                self.CHANNEL_PITCH = KeyValData.value
            elif KeyValData.key == self.KEY_ROBOT_CHANNEL_ROLL :
                self.CHANNEL_ROLL = KeyValData.value
            elif KeyValData.key == self.KEY_ROBOT_CHANNEL_THROTTLE:
                self.CHANNEL_THROTTLE = KeyValData.value
            elif KeyValData.key == self.KEY_ROBOT_CHANNEL_YAW:
                self.CHANNEL_YAW = KeyValData.value
            elif KeyValData.key == self.KEY_ROBOT_CHANNEL_FORWARD:
                self.CHANNEL_FORWARD = KeyValData.value
            elif KeyValData.key == self.KEY_ROBOT_CHANNEL_LATERAL:
                self.CHANNEL_LATERAL = KeyValData.value
        # print (MessageData)

        self.__set_channel_value__()

    def __set_channel_value__(self):
        self.autopilot.set_rc_channel_pwm(self.KEY_ROBOT_CHANNEL_PITCH+1, self.CHANNEL_PITCH)
        self.autopilot.set_rc_channel_pwm(self.KEY_ROBOT_CHANNEL_ROLL+1, self.CHANNEL_ROLL)
        self.autopilot.set_rc_channel_pwm(self.KEY_ROBOT_CHANNEL_THROTTLE+1, self.CHANNEL_THROTTLE)
        self.autopilot.set_rc_channel_pwm(self.KEY_ROBOT_CHANNEL_YAW+1, self.CHANNEL_YAW)
        self.autopilot.set_rc_channel_pwm(self.KEY_ROBOT_CHANNEL_FORWARD+1, self.CHANNEL_FORWARD)
        self.autopilot.set_rc_channel_pwm(self.KEY_ROBOT_CHANNEL_LATERAL+1, self.CHANNEL_LATERAL)
