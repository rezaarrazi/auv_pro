from MAVLink import MAVLink
from udp import udpMavlink
import time
import socket

autopilot = MAVLink("ardupilotmega", '/dev/ttyACM0')
autopilot.set_mode(autopilot.MANUAL)

autopilot.set_vehicle_arm(autopilot.ARM)

# Create udp communication
UDP_IP = "192.168.2.255" #127.0.0.1
UDP_PORT = 4003
udp_mavlink = udpMavlink(UDP_IP, UDP_PORT)

while True:
    udp_mavlink.update_data(autopilot)
    # print(udp_mavlink.CHANNEL_FORWARD)