from pymavlink import mavutil

class MAVLink:
    STABILIZE = 0
    MANUAL = 19
    DISARM = 0
    ARM = 1

    def __init__(self, dialect, interface):
        mavutil.set_dialect(dialect)
        self.master = mavutil.mavlink_connection(interface)

        msg = None

        # wait for autopilot connection
        while msg is None:
            msg = self.master.recv_msg()

    def set_mode(self, mode_id):
        self.master.set_mode(mode_id)

    def set_rc_channel_pwm(self, id, pwm=1500):
        """ Set RC channel pwm value
        Args:
            id (TYPE): Channel ID
            pwm (int, optional): Channel pwm value 1100-1900
        """
        if id < 1:
            print("Channel does not exist.")
            return

        if id < 9:
            rc_channel_values = [65535 for _ in range(8)]
            rc_channel_values[id - 1] = pwm
            self.master.mav.rc_channels_override_send(
                self.master.target_system,                  # target_system
                self.master.target_component,               # target_component
                *rc_channel_values)                         # RC channel list, in microseconds.

    def send_heartbeat(self):
        self.master.mav.heartbeat_send(
            6, # type
            8, # autopilot
            192, # base_mode
            0, # custom_mode
            4, # system_status
            3  # mavlink_version
        )

    def set_vehicle_arm(self, arm=0):
        self.master.mav.command_long_send(
            1, # autopilot system id
            1, # autopilot component id
            400, # command id, ARM/DISARM
            0, # confirmation
            arm, # arm!
            0,0,0,0,0,0 # unused parameters for this command
        )