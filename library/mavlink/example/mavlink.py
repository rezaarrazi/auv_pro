import time
from pymavlink import mavutil

mavutil.set_dialect("ardupilotmega")

master = mavutil.mavlink_connection('/dev/ttyACM0')

msg = None

# wait for autopilot connection
while msg is None:
	msg = master.recv_msg()

# Choose a mode
mode_id = 0 #0 STABILIZE, 19 MANUAL

# Set new mode
master.set_mode(mode_id)

#print msg
def set_rc_channel_pwm(id, pwm=1500):
    """ Set RC channel pwm value
    Args:
        id (TYPE): Channel ID
        pwm (int, optional): Channel pwm value 1100-1900
    """
    if id < 1:
        print("Channel does not exist.")
        return

    if id < 9:
        rc_channel_values = [65535 for _ in range(8)]
        rc_channel_values[id - 1] = pwm
        master.mav.rc_channels_override_send(
            master.target_system,                # target_system
            master.target_component,             # target_component
            *rc_channel_values)                  # RC channel list, in microseconds.

# The values of these heartbeat fields is not really important here
# I just used the same numbers that QGC uses
# It is standard practice for any system communicating via mavlink emit the HEARTBEAT message at 1Hz! Your autopilot may not behave the way you want otherwise!
master.mav.heartbeat_send(
    6, # type
    8, # autopilot
    192, # base_mode
    0, # custom_mode
    4, # system_status
    3  # mavlink_version
)

master.mav.command_long_send(
    1, # autopilot system id
    1, # autopilot component id
    400, # command id, ARM/DISARM
    0, # confirmation
    1, # arm!
    0,0,0,0,0,0 # unused parameters for this command
)
print 'this is arm'

lastHeartbeat_t = 0
move_t = 0
move_last_t = 0
move_delta_t = 0
i=5
while(i):
    t = time.time()
    move_delta_t = t - move_last_t

    if (t > lastHeartbeat_t + 1):
        master.mav.heartbeat_send(
            6, # type
            8, # autopilot
            192, # base_mode
            0, # custom_mode
            4, # system_status
            3  # mavlink_version
        )
        lastHeartbeat_t = t

    if(move_t < 2):
        # Set some roll
        # set_rc_channel_pwm(2, 1600) #2 roll, 3 up down, 4 yaw, 5 fb, 6 lateral

        # Set some yaw
        set_rc_channel_pwm(6, 1600)

    else:
        set_rc_channel_pwm(6, 1500)
        if(move_t > 4):
            move_t = 0
            i=i-1
    
    move_t += move_delta_t
    move_last_t = t
	
master.mav.command_long_send(
    1, # autopilot system id
    1, # autopilot component id
    400, # command id, ARM/DISARM
    0, # confirmation
    0, # disarm!
    0,0,0,0,0,0 # unused parameters for this command
)
print 'disarm'