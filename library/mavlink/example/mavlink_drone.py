from __future__ import print_function
from dronekit import connect, VehicleMode, LocationGlobalRelative, LocationGlobal, Command
import numpy as np
import imutils
import time
import math
from pymavlink import mavutil
import config
import srfalt
import waypoints as wp
import argparse


parser = argparse.ArgumentParser(description='Commands vehicle using vehicle.simple_goto.')
parser.add_argument('--connect',
                    help="Vehicle connection target string. If not specified, SITL automatically started and used.")
args = parser.parse_args()

connection_string = args.connect
sitl = None

# Start SITL if no connection string specified
if not connection_string:
    import dronekit_sitl
    sitl = dronekit_sitl.start_default()
    connection_string = sitl.connection_string()

altdroprel=2.2 #ketinggian relativ ketika menjatuhkan barang
altwayrel=2.5  #ketinggian relativ ketika ingin naik kembaliq

#altsrf=srfalt.ketinggian()/100
altway=2.2
alttakeof=2.2

send_ned_const=0.22

LATITUDE  = 0
LONGITUDE = 1

# Waypoints
# Initialize data from vtol.waypoints
wp.setup()

# Get first waypoint
wp1  = wp.get(1)
lat1 = wp1[LATITUDE]
long1= wp1[LONGITUDE]

print(lat1,long1)

# Get second waypoint
wp2  = wp.get(2)
lat2 = wp2[LATITUDE]
long2= wp2[LONGITUDE]

print(lat2,long2)


# Get third waypoint
wp3  = wp.get(3)
lat3 = wp3[LATITUDE]
long3= wp3[LONGITUDE]

print(lat3,long3)


# Get Fourth waypoint




# Connect to the Vehicle
print('Connecting to vehicle on: %s' % connection_string)
vehicle = connect(connection_string, wait_ready=True)

check=altway+(vehicle.location.global_relative_frame.alt-(srfalt.ketinggian()/100))

print(mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT_INT)

def arm_and_takeoff(aTargetAltitude):
    """
    Arms vehicle and fly to aTargetAltitude.
    """
    print("Arming motors")
    # Copter should arm in GUIDED mode
    vehicle.mode = VehicleMode("GUIDED")
    vehicle.armed = True

    # Confirm vehicle armed before attempting to take off
    while not vehicle.armed:
        print(" Waiting for arming...")
        time.sleep(1)
        
    srfalt.start()
    print("Taking off!")
    vehicle.simple_takeoff(aTargetAltitude)  # Take off to target altitude

    # Wait until the vehicle reaches a safe height before processing the goto
    #  (otherwise the command after Vehicle.simple_takeoff will execute
    #   immediately).
    while True:
        print(" Altitude: ", srfalt.ketinggian()/100)
        # Break and return from function just below target altitude.
        if srfalt.ketinggian()/100>= aTargetAltitude * 0.95:
            print("Reached target altitude")
            break
        time.sleep(1)


def set_speed(speed):
    """
    Send MAV_CMD_DO_CHANGE_SPEED to change the current speed when travelling to a point.
	
    In AC3.2.1 Copter will accelerate to this speed near the centre of its journey and then 
    decelerate as it reaches the target. In AC3.3 the speed changes immediately.
	
    This method is only useful when controlling the vehicle using position/goto commands. 
    It is not needed when controlling vehicle movement using velocity components.
	
    For more information see: 
    http://copter.ardupilot.com/common-mavlink-mission-command-messages-mav_cmd/#mav_cmd_do_change_speed
    """
    # create the MAV_CMD_DO_CHANGE_SPEED command
    msg = vehicle.message_factory.command_long_encode(
        0, 0,    # target system, target component
        mavutil.mavlink.MAV_CMD_DO_CHANGE_SPEED, #command
        0, #confirmation
        0, #param 1
        speed, # speed
        0, 0, 0, 0, 0 #param 3 - 7
        )
    # send command to vehicle
    vehicle.send_mavlink(msg)
    vehicle.flush()


def get_distance_metres(aLocation1, aLocation2):
    dlat        = aLocation2.lat - aLocation1.lat
    dlong       = aLocation2.lon - aLocation1.lon
    return math.sqrt((dlat*dlat) + (dlong*dlong)) * 1.113195e5

def goto_position_target_global_int(aLocation):
    """
    Send SET_POSITION_TARGET_GLOBAL_INT command to request the vehicle fly to a specified location.

    For more information see: https://pixhawk.ethz.ch/mavlink/#SET_POSITION_TARGET_GLOBAL_INT

    See the above link for information on the type_mask (0=enable, 1=ignore). 
    At time of writing, acceleration and yaw bits are ignored.
    """
    msg = vehicle.message_factory.set_position_target_global_int_encode(
        0,       # time_boot_ms (not used)
        0, 0,    # target system, target component
        mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT_INT, # frame      
        0b0000111111111000, # type_mask (only speeds enabled)
        aLocation.lat*1e7, # lat_int - X Position in WGS84 frame in 1e7 * meters
        aLocation.lon*1e7, # lon_int - Y Position in WGS84 frame in 1e7 * meters
        aLocation.alt, # alt - Altitude in meters in AMSL altitude, not WGS84 if absolute or relative, above terrain if GLOBAL_TERRAIN_ALT_INT
        0, # X velocity in NED frame in m/s
        0, # Y velocity in NED frame in m/s
 +       0, # Z velocity in NED frame in m/s
        0, 0, 0, # afx, afy, afz acceleration (not supported yet, ignored in GCS_Mavlink)
        0, 0)    # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink) 
    # send command to vehicle
    vehicle.send_mavlink(msg)
    vehicle.flush()



def gotoGPS(location):
    currentLocation = vehicle.location.global_frame
    targetDistance = get_distance_metres(currentLocation, location)
    #set_speed(2.5)
    goto_position_target_global_int(location)
    vehicle.groundspeed = 2.7
    set_speed(2.7)
    vehicle.flush()
    while True:
        remainingDistance=get_distance_metres(vehicle.location.global_frame, location)
        print ("altdistance",remainingDistance)
        print ("altsrf",srfalt.ketinggian()/100)
	print ("altgps",vehicle.location.global_relative_frame.alt)
	print ("altgps-srf",vehicle.location.global_relative_frame.alt-(srfalt.ketinggian()/100))
        if remainingDistance<=targetDistance*0.05: #Just below target, in case of undershoot.
            print ("Reached target")
            break
        time.sleep(2)

def send_ned_velocity(velocity_x, velocity_y, velocity_z,duration,perintah):
    """
    Move vehicle in direction based on specified velocity vectors. 
	
    This uses the SET_POSITION_TARGET_LOCAL_NED command with a type mask enabling only 
    velocity components (https://pixhawk.ethz.ch/mavlink/#SET_POSITION_TARGET_LOCAL_NED).
    
    See the above link for information on the type_mask (0=enable, 1=ignore). 
    At time of writing, acceleration and yaw bits are ignored.
    """
    msg = vehicle.message_factory.set_position_target_local_ned_encode(
        0,       # time_boot_ms (not used)
        0, 0,    # target system, target component
        mavutil.mavlink.MAV_FRAME_BODY_NED, # frame
        0b0000111111000111, # type_mask (only speeds enabled)
+++++        0, 0, 0, # x, y, z positions (not used)
        velocity_x, velocity_y, velocity_z, # x, y, z velocity in m/s
        0, 0, 0, # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
        0, 0)    # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink) 
    #1 Turun 2 Naik
    if(perintah==1 and srfalt.ketinggian()/100 >1) :
        while duration>0 or srfalt.ketinggian()/100 >1.8 :
            vehicle.send_mavlink(msg)
            print("ketinggian",srfalt.ketinggian()/100)
            duration=duration-1
            vehicle.flush()
            time.sleep(1)
    elif(perintah==2 and srfalt.ketinggian()/100 <3):
        while duration>0 or vehicle.location.global_relative_frame.alt <1.95 :
            vehicle.send_mavlink(msg)
            print("ketinggian",srfalt.ketinggian()/100)
            duration=duration-1
            vehicle.flush()
            time.sleep(1)    
    vehicle.flush()

#--------TAKE-OFF ----------------   
arm_and_takeoff(alttakeof)
vehicle.airspeed = 2.5
i=30
time.sleep(2)
        
print ("altsrf",srfalt.ketinggian()/100)
print ("altgps",vehicle.location.global_relative_frame.alt)
print ("altgps-srf",vehicle.location.global_relative_frame.alt-(srfalt.ketinggian()/100))

#----------GotoWP1-----------------------------------
newLoc = LocationGlobalRelative(lat2, long2, altway+(vehicle.location.global_relative_frame.alt-(srfalt.ketinggian()/100)))
gotoGPS(newLoc)
time.sleep(2)
#_____TURUN_______
send_ned_velocity(0,0,send_ned_const,1,1)
vehicle.flush()
print("down")
config.set('servo1value', 100)
time.sleep(2)
send_ned_velocity(0,0,-send_ned_const,1,2)
time.sleep(2)

print ("alt-srf",srfalt.ketinggian()/100)
print ("alt-gps",vehicle.location.global_relative_frame.alt)
print ("altgps-srf",vehicle.location.global_relative_frame.alt-(srfalt.ketinggian()/100))


#----------GotoWP2-----------------------------------
newLoc2 = LocationGlobalRelative(lat1, long1, altway+(vehicle.location.global_relative_frame.alt-(srfalt.ketinggian()/100)))
gotoGPS(newLoc2)
time.sleep(3)

#_____TURUN_______
send_ned_velocity(0,0,send_ned_const,1,1)
vehicle.flush()
print("down")
config.set('servo2value', 62)
time.sleep(2)
send_ned_velocity(0,0,-send_ned_const,1,2)
time.sleep(2)


#----------GotoWP3-----------------------------------
newLoc3 = LocationGlobalRelative(lat3, long3, altway+(vehicle.location.global_relative_frame.alt-(srfalt.ketinggian()/100)))
gotoGPS(newLoc3)
time.sleep(3)

#_____TURUN_______
send_ned_velocity(0,0,send_ned_const,1,1)
vehicle.flush()
print("down")
config.set('servo2value', 100)
time.sleep(2)	
send_ned_velocity(0,0,-send_ned_const,1,2)
time.sleep(2)

#---------------------------

#----------RETURN TO LAUNCH----------------------------
vehicle.mode = VehicleMode("RTL")
srfalt.end()

#Close vehicle object before exiting script
print("Close vehicle object")
vehicle.close()
