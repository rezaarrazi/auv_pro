import time
# Import mavutil
from pymavlink import mavutil

def wait_conn(master):
    msg = None
    while not msg:
        master.mav.ping_send(
            time.time(), # Unix time
            0, # Ping number
            0, # Request ping of all systems
            0 # Request ping of all components
        )
        msg = master.recv_match()
        time.sleep(0.5)

# # Create the connection
# master = mavutil.mavlink_connection('udpin:127.0.0.1:14550')
# # Wait a heartbeat before sending commands
# master.wait_heartbeat()

# Create the connection
# Need to provide the serial port and baudrate
master = mavutil.mavlink_connection(
            '/dev/ttyACM0',
            baud=115200)

# wait_conn(master)

# Wait a heartbeat before sending commands
# master.wait_heartbeat()

# http://mavlink.org/messages/common#MAV_CMD_COMPONENT_ARM_DISARM

# Arm
# master.arducopter_arm() or:
print("AUV Armed")
master.mav.command_long_send(
    master.target_system,
    master.target_component,
    mavutil.mavlink.MAV_CMD_COMPONENT_ARM_DISARM,
    0,
    1, 0, 0, 0, 0, 0, 0)

time.sleep(3.0)

# Disarm
# master.arducopter_disarm() or:
print("AUV Disarmed")
master.mav.command_long_send(
    master.target_system,
    master.target_component,
    mavutil.mavlink.MAV_CMD_COMPONENT_ARM_DISARM,
    0,
    0, 0, 0, 0, 0, 0, 0)