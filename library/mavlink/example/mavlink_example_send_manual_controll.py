# Import mavutil
import time
from pymavlink import mavutil

# Create the connection
master = mavutil.mavlink_connection('udpin:0.0.0.0:14550')
# Wait a heartbeat before sending commands
master.wait_heartbeat()

master.mav.heartbeat_send(
    6, # type
    8, # autopilot
    192, # base_mode
    0, # custom_mode
    4, # system_status
    3  # mavlink_version
)

master.mav.command_long_send(
    1, # autopilot system id
    1, # autopilot component id
    400, # command id, ARM/DISARM
    0, # confirmation
    1, # arm!
    0,0,0,0,0,0 # unused parameters for this command
)
print 'this is arm'

# Send a positive x value, negative y, negative z,
# positive rotation and no button.
# http://mavlink.org/messages/common#MANUAL_CONTROL
# Warning: Because of some legacy workaround, z will work between [0-1000]
# where 0 is full reverse, 500 is no output and 1000 is full throttle.
# x,y and r will be between [-1000 and 1000].
master.mav.manual_control_send(
    master.target_system,
    500,
    0,
    0,
    0,
    0)

time.sleep(2)

# To active button 0 (first button), 3 (fourth button) and 7 (eighth button)
# It's possible to check and configure this buttons in the Joystick menu of QGC
buttons = 1 + 1 << 3 + 1 << 7
master.mav.manual_control_send(
    master.target_system,
    0,
    0,
    0,
    0,
    buttons)