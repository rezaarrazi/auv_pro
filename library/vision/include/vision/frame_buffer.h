#ifndef VISION_FRAME_BUFFER_H
#define VISION_FRAME_BUFFER_H

#include "vision/image.h"

#include <opencv2/core.hpp>

class FrameBuffer
{
public:

	Image *m_YUVFrame;
	Image *m_RGBFrame;
	cv::Mat m_RGBInput;
	cv::Mat m_RGBLapangan;
	cv::Mat m_HSVInput;
	cv::Mat m_GrayInput;

	FrameBuffer();
	FrameBuffer(int width, int height);
	~FrameBuffer();

	void yuv2rgb();
	void rgb2hsv();
	void rgb2gray();
};

#endif