#ifndef VISION_LBP_CLASSIFIER_H
#define VISION_LBP_CLASSIFIER_H

#include <opencv2/core.hpp>
#include <opencv2/objdetect.hpp>
#include <vector>
#include <string>

class LBPClassifier
{
public:

  enum
  {
    CLASSIFIER_TYPE_BALL    = 0
  };

  LBPClassifier();
  LBPClassifier(int type);

  void setClassifierType(int type);

  std::vector<cv::Rect> classify(cv::Mat input);

private:

  bool loadClassifier(std::string classifier_file_path);

  cv::CascadeClassifier cascade_classifier_;
  bool classifier_loaded_;
  int classifier_type_;
};

#endif