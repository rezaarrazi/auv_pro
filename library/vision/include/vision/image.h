#ifndef VISION_IMAGE_H
#define VISION_IMAGE_H

#include <opencv2/core.hpp>

class Image
{
public:
	static const int YUV_PIXEL_SIZE = 4;
	static const int RGB_PIXEL_SIZE = 3;
	static const int HSV_PIXEL_SIZE = 4;

	unsigned char *m_ImageData; /* pointer to aligned image data */
	int m_Width;                /* image width in pixels */
	int m_Height;               /* image height in pixels */
	int m_PixelSize;            /* pixel size in bytes */
	int m_NumberOfPixels;       /* number of pixels */
	int m_WidthStep;            /* size of aligned image row in bytes */
	int m_ImageSize;            /* image data size in bytes (=image->m_Height*image->m_WidthStep) */

	Image(int width, int height, int pixelsize);
	virtual ~Image();

	Image &operator = (Image &img);
	
	void fromMat(cv::Mat mat);
	void filterMat(cv::Mat mat, uint8_t r, uint8_t g, uint8_t b);
};

#endif
