#ifndef VISION_CAMERA_H
#define VISION_CAMERA_H

#include "vision/frame_buffer.h"
#include "config.h"

#include <string>

#include <linux/videodev2.h>

namespace Robot
{
class Camera
{
public:

  Camera();
  ~Camera();

  static Camera *getInstance();

  FrameBuffer *getBuffer() { return fbuffer_; }

  void loadConfig();
  void saveConfig();

  bool isPortOpen() { return (camera_fd_ >= 0); }

  bool isPortExist();
  bool openPort();
  bool closePort();

  bool readFrame();
  bool captureFrame();

  int getWidth() { return width_; }
  int getHeight() { return height_; }

  int getCenterX() { return width_ / 2; }
  int getCenterY() { return height_ / 2; }

  float getViewHAngle() { return view_h_angle_; }
  float getViewVAngle() { return view_v_angle_; }

  float calculatePanFromXPos(float x_pos) { return ((x_pos / (float)width_) - 0.5) * view_h_angle_; }
  float calculateTiltFromYPos(float y_pos) { return ((y_pos / (float)height_) - 0.5) * view_v_angle_; }

  int getControl(int control);
  int setControl(int control, int value);
  int resetControl(int control);

  int getBrightness() { return (brightness_ = getControl(V4L2_CID_BRIGHTNESS)); }
  int getContrast() { return (contrast_ = getControl(V4L2_CID_CONTRAST)); }
  int getSaturation() { return (saturation_ = getControl(V4L2_CID_SATURATION)); }
  int getGain() { return (gain_ = getControl(V4L2_CID_GAIN)); }
  int getExposure() { return (exposure_ = getControl(V4L2_CID_EXPOSURE_ABSOLUTE)); }
  int getTemperature() { return (temperature_ = getControl(V4L2_CID_WHITE_BALANCE_TEMPERATURE)); }

  int setBrightness(int value) { return setControl(V4L2_CID_BRIGHTNESS, brightness_ = value); }
  int setContrast(int value) { return setControl(V4L2_CID_CONTRAST, contrast_ = value); }
  int setSaturation(int value) { return setControl(V4L2_CID_SATURATION, saturation_ = value); }
  int setGain(int value) { return setControl(V4L2_CID_GAIN, gain_ = value); }
  int setExposure(int value) { return setControl(V4L2_CID_EXPOSURE_ABSOLUTE, exposure_ = value); }
  int setTemperature(int value) { return setControl(V4L2_CID_WHITE_BALANCE_TEMPERATURE, temperature_ = value); }

  int setAutoWhiteBalance(bool on) { return setControl(V4L2_CID_AUTO_WHITE_BALANCE, (int)on); }

  void getCameraSettings();
  void setCameraSettings();

private:

  struct Buffer
  {
    void *start;
    size_t length;
  };

  bool initCameraFormat();
  bool initFrameRate();
  bool initCameraSetting();

  bool setCameraStream(bool turn_on);

  bool mapMemory();
  bool unmapMemory();

  void calculateFieldOfView();

  static Camera *unique_instance_;

  Config config_;

  int camera_fd_;
  std::string camera_name_;

  Buffer *buffers_;
  unsigned int n_buffers_;

  FrameBuffer *fbuffer_;

  int width_;
  int height_;

  float field_of_view_;
  float view_h_angle_;
  float view_v_angle_;

  int brightness_;
  int contrast_;
  int saturation_;
  int gain_;
  int exposure_;
  int temperature_;
};
}

#endif
