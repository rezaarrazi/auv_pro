#ifndef VISION_HOG_BALL_DETECTION_H
#define VISION_HOG_BALL_DETECTION_H

#include "linux.h"

#include <vector>
#include <string>

#include <opencv2/core.hpp>
#include <opencv2/objdetect.hpp>

class HOGClassifier
{
public:

  enum
  {
    CLASSIFIER_TYPE_BALL    = 0
  };

  HOGClassifier();
  HOGClassifier(int type);

  void setClassifierType(int type);

  std::vector<cv::Rect> classify(cv::Mat input);

private:

  bool loadDescriptor(std::string descriptor_file_path);

  cv::HOGDescriptor hog_descriptor_;
  bool descriptor_loaded_;
  int classifier_type_;
};

#endif