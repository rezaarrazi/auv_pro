#ifndef VISION_COLOR_CLASSIFIER_H
#define VISION_COLOR_CLASSIFIER_H

#include "algebra.h"
#include "config.h"

#include <string>
#include <vector>
#include <map>

#include <opencv2/opencv.hpp>

class ColorClassifier
{
public:

  enum
  {
    CLASSIFIER_TYPE_RED         = 0,
    CLASSIFIER_TYPE_BLUE        = 1,
    CLASSIFIER_TYPE_YELLOW      = 2,
    CLASSIFIER_TYPE_CYAN        = 3,
    CLASSIFIER_TYPE_MAGENTA     = 4,
    CLASSIFIER_TYPE_BALL        = 11,
    CLASSIFIER_TYPE_FIELD       = 12,
    CLASSIFIER_TYPE_GOAL        = 13,
    CLASSIFIER_TYPE_MARATHON    = 14,
    CLASSIFIER_TYPE_BLACK       = 15,
    CLASSIFIER_TYPE_WHITE       = 16,
    CLASSIFIER_TYPE_BASKETBALL  = 17
  };

  ColorClassifier(int classifier_type);
  ~ColorClassifier();

  static ColorClassifier *getInstance(int classifier_type);
  static ColorClassifier *getInstance(std::string name);

  std::string getConfigName();
  void loadConfig() { config_.load(getConfigName()); }
  void saveConfig() { config_.save(getConfigName()); }

  cv::Mat classify(cv::Mat input);
  cv::Mat classifyGray(cv::Mat input);

  int getHue() { return hue_; }
  int getHueTolerance() { return hue_tolerance_; }
  int getMinSaturation() { return min_saturation_; }
  int getMaxSaturation() { return max_saturation_; }
  int getMinValue() { return min_value_; }
  int getMaxValue() { return max_value_; }

  void setHue(int value) { hue_ = alg::clampValue(value, 0, 360); }
  void setHueTolerance(int value) { hue_tolerance_ = alg::clampValue(value, 0, 179); }
  void setMinSaturation(int value) { min_saturation_ = alg::clampValue(value, 0, 100); }
  void setMaxSaturation(int value) { max_saturation_ = alg::clampValue(value, 0, 100); }
  void setMinValue(int value) { min_value_ = alg::clampValue(value, 0, 100); }
  void setMaxValue(int value) { max_value_ = alg::clampValue(value, 0, 100); }

private:

  static std::map<int, ColorClassifier *> unique_instances_;

  int classifier_type_;
  std::string name_;

  Config config_;

  int hue_;
  int hue_tolerance_;
  int min_saturation_;
  int max_saturation_;
  int min_value_;
  int max_value_;
};

#endif
