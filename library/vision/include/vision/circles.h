#ifndef VISION_CIRCLES_H
#define VISION_CIRCLES_H

#include <opencv2/opencv.hpp>
#include <vector>

class Circles
{
private:

    std::vector<cv::Point> centers_;
    std::vector<float> radiuses_;

public:

    Circles();
    Circles(std::vector<std::vector<cv::Point>> contours) { find(contours); }
    
    cv::Mat getBinaryMat(cv::Size mat_size) { return getBinaryMatLine(mat_size, CV_FILLED); }
    cv::Mat getBinaryMatLine(cv::Size mat_size, int line_size);

    std::vector<cv::Point> getCenters() { return centers_; }
    std::vector<float> getRadiuses() { return radiuses_; }

    cv::Point getFirstCenter();
    float getFirstRadiuses();

    void find(std::vector<std::vector<cv::Point>> contours);
};

#endif