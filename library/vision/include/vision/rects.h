#ifndef VISION_RECTS_H
#define VISION_RECTS_H

#include <opencv2/opencv.hpp>
#include <vector>

class Rects
{
private:

    std::vector<cv::Rect> rects_;

public:

    Rects() { rects_.clear(); }
    Rects(cv::Rect rect) { rects_.clear(); addRect(rect); }
    Rects(std::vector<cv::Rect> rects) { setRects(rects); }

    cv::Mat getBinaryMatLine(cv::Size mat_size, int line_size);
    cv::Mat getBinaryMat(cv::Size mat_size) { return getBinaryMatLine(mat_size, CV_FILLED); }

    std::vector<cv::Rect> getRects() { return rects_; }
    void setRects(std::vector<cv::Rect> rects) { rects_ = rects; }

    void addRect(cv::Rect rect) { rects_.push_back(rect); }

    cv::Rect getFirstRect();
    cv::Point getFirstRectCenter();

    void filterLargest();
};

#endif