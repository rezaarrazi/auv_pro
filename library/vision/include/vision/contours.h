#ifndef VISION_CONTOURS_H
#define VISION_CONTOURS_H

#include <opencv2/opencv.hpp>
#include <vector>

#include "camera.h"
#include "math/point.h"

class Contours
{
public:

  Contours() { contours_.clear(); }
  Contours(std::vector<std::vector<cv::Point>> contours) { setContours(contours); }
  Contours(cv::Mat binary_mat) { find(binary_mat); }

  cv::Mat getBinaryMat(cv::Size mat_size);
  cv::Mat getBinaryMatLine(cv::Size mat_size, int line_size);

  std::vector<std::vector<cv::Point>> getContours() { return contours_; }
  void setContours(std::vector<std::vector<cv::Point>> contours) { contours_ = contours; }

  void find(cv::Mat binary_mat);

  void joinAll();

  void filterSmallerThan(float value);
  void filterLargerThen(float value);
  void filterLargest();

  float centerX();
  float centerY();

  float minX();
  float minY();
  float maxX();
  float maxY();

  void expand(float value);
  void strecthUp(float value);

  std::vector<std::vector<cv::Point>> splitLeft(float x);
  std::vector<std::vector<cv::Point>> splitRight(float x);

  void filterRect(float x, float y, float width, float height, float value);
  void fillRect(float x, float y, float width, float height) { filterRect(x, y, width, height, 255); }
  void removeRect(float x, float y, float width, float height) { filterRect(x, y, width, height, 0); }

  void convexHull();

  std::vector<std::vector<cv::Point>> leftWallContours();
  std::vector<std::vector<cv::Point>> rightWallContours();

  float xOfMaxY();

private:

    std::vector<std::vector<cv::Point>> contours_;

};

#endif