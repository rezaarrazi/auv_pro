#include "vision/circles.h"

Circles::Circles()
{
    centers_.clear();
    radiuses_.clear();
}

cv::Mat Circles::getBinaryMatLine(cv::Size mat_size, int line_size)
{
    cv::Mat binary_mat(mat_size, CV_8UC1);
    binary_mat = cv::Scalar(0);

    if (centers_.size() > 0 && radiuses_.size() > 0)
    {
        for (unsigned int i = 0; i < centers_.size() && i < radiuses_.size(); i++)
        {
		    cv::circle(binary_mat, centers_[i], radiuses_[i], 255, line_size);
        }
    }

    return binary_mat;
}

cv::Point Circles::getFirstCenter()
{
    if (centers_.size() <= 0)
        return cv::Point(-1, -1);

    return centers_[0];
}

float Circles::getFirstRadiuses()
{
    if (radiuses_.size() <= 0)
        return 0;
    
    return radiuses_[0];
}

void Circles::find(std::vector<std::vector<cv::Point>> contours)
{
    centers_.clear();
    radiuses_.clear();

    for (std::vector<cv::Point> &contour : contours)
    {
        cv::Point2f center;
        float radius;
        cv::minEnclosingCircle(cv::Mat(contour), center, radius);
        
        centers_.push_back(center);
        radiuses_.push_back(radius);
    }
}