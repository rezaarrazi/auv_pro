#include "vision/color_classifier.h"
#include "algebra.h"

#include <sstream>

std::map<int, ColorClassifier *> ColorClassifier::unique_instances_;

ColorClassifier::ColorClassifier(int classifier_type)
{
  classifier_type_ = classifier_type;
  unique_instances_.insert(std::pair<int, ColorClassifier *>(classifier_type_, this));

  switch (classifier_type_)
  {
    case CLASSIFIER_TYPE_RED: name_ = "red"; break;
    case CLASSIFIER_TYPE_BLUE: name_ = "blue"; break;
    case CLASSIFIER_TYPE_YELLOW: name_ = "yellow"; break;
    case CLASSIFIER_TYPE_CYAN: name_ = "cyan"; break;
    case CLASSIFIER_TYPE_MAGENTA: name_ = "magenta"; break;
    case CLASSIFIER_TYPE_BALL: name_ = "ball"; break;
    case CLASSIFIER_TYPE_FIELD: name_ = "field"; break;
    case CLASSIFIER_TYPE_GOAL: name_ = "goal"; break;
    case CLASSIFIER_TYPE_MARATHON: name_ = "marathon"; break;
    case CLASSIFIER_TYPE_BLACK: name_ = "black"; break;
    case CLASSIFIER_TYPE_WHITE: name_ = "white"; break;
    case CLASSIFIER_TYPE_BASKETBALL: name_ = "basketball"; break;
  }

  Config::Section *section = config_.addSection("Color");
  section->addParam<int>("hue", 0)->setBindValue(&hue_);
  section->addParam<int>("hue_tolerance", 100)->setBindValue(&hue_tolerance_);
  section->addParam<int>("min_saturation", 0)->setBindValue(&min_saturation_);
  section->addParam<int>("max_saturation", 100)->setBindValue(&max_saturation_);
  section->addParam<int>("min_value", 0)->setBindValue(&min_value_);
  section->addParam<int>("max_value", 100)->setBindValue(&max_value_);

  config_.sync(getConfigName());
}

ColorClassifier::~ColorClassifier()
{
  unique_instances_.erase(classifier_type_);
}

ColorClassifier *ColorClassifier::getInstance(int classifier_type)
{
  if (unique_instances_.find(classifier_type) != unique_instances_.end())
    return unique_instances_[classifier_type];

  return (new ColorClassifier(classifier_type));
}

ColorClassifier *ColorClassifier::getInstance(std::string name)
{
  for (const std::pair<int, ColorClassifier*> &keyval: unique_instances_)
  {
    if (keyval.second->name_ == name)
      return keyval.second;
  }

  return nullptr;
}

std::string ColorClassifier::getConfigName()
{
  std::stringstream config_name;
  config_name << "color_classifier/" << name_ << ".yaml";

  return config_name.str();
}

cv::Mat ColorClassifier::classify(cv::Mat input)
{
  int h_min = ((hue_ - hue_tolerance_) * 255) / 360;
  int h_max = ((hue_ + hue_tolerance_) * 255) / 360;

  int s_min = (min_saturation_ * 255) / 100;
  int s_max = (max_saturation_ * 255) / 100;

  int v_min = (min_value_ * 255) / 100;
  int v_max = (max_value_ * 255) / 100;

  cv::Scalar hsv_min = cv::Scalar(h_min, s_min, v_min);
  cv::Scalar hsv_max = cv::Scalar(h_max, s_max, v_max);

  cv::Mat output = input.clone();

  cv::inRange(input, hsv_min, hsv_max, output);

  cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3), cv::Point(1, 1));
  cv::morphologyEx(output, output, cv::MORPH_CLOSE, element);

  return output;
}

cv::Mat ColorClassifier::classifyGray(cv::Mat input)
{
  int h_min = 0;
  int h_max = 0;

  int s_min = 0;
  int s_max = 0;

  int v_min = (min_value_* 255) / 100;
  int v_max = (max_value_* 255) / 100;

  cv::Mat output = input.clone();

  int pixelNum = input.cols * input.rows;
  int j = -1;
  for (int i = 0; i < pixelNum; i++)
  {
    if (i % input.cols == 0)
      j++;

    cv::Point pixel(i % input.cols, j);
    output.at<uint8_t>(pixel) = (alg::valueInside(input.at<uint8_t>(pixel), v_min, v_max));
  }

  return output;
}