#include "vision/contours.h"
#include "algebra.h"
#include "math/point.h"

cv::Mat Contours::getBinaryMat(cv::Size mat_size)
{
    cv::Mat binary_mat(mat_size, CV_8UC1);
    binary_mat = cv::Scalar(0);

    if (contours_.size() > 0)
    {
		cv::fillPoly(binary_mat, contours_, 255);
    }
    return binary_mat;
}

cv::Mat Contours::getBinaryMatLine(cv::Size mat_size, int line_size)
{
    cv::Mat binary_mat(mat_size, CV_8UC1);
    binary_mat = cv::Scalar(0);

    if (contours_.size() > 0)
    {
        for (unsigned int i = 0; i < contours_.size(); i++)
        {
		    cv::drawContours(binary_mat, contours_, i, 255, line_size);
        }
    }

    return binary_mat;
}

void Contours::find(cv::Mat binary_mat)
{
    std::vector<cv::Vec4i> hierarchy;

    contours_.clear();
    cv::findContours(binary_mat, contours_, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
}

void Contours::joinAll()
{
    if (contours_.size() <= 0)
        return;

    std::vector<cv::Point> join_contour;
    for (std::vector<cv::Point> &contour : contours_)
    {
        for (unsigned int i = 0; i < contour.size(); i++)
        {
            join_contour.push_back(contour[i]);
        }
    }

    contours_.clear();
    contours_.push_back(join_contour);
}

void Contours::filterSmallerThan(float value)
{
    if (contours_.size() <= 0)
        return;

    std::vector<std::vector<cv::Point>> large_contours;
    for (std::vector<cv::Point> &contour : contours_)
    {
        if (cv::contourArea(contour) < (double)value)
        {
            large_contours.push_back(contour);
        }
    }

    contours_ = large_contours;
}

void Contours::filterLargerThen(float value)
{
    if (contours_.size() <= 0)
        return;

    std::vector<std::vector<cv::Point>> large_contours;
    for (std::vector<cv::Point> &contour : contours_)
    {
        if (cv::contourArea(contour) > (double)value)
        {
            large_contours.push_back(contour);
        }
    }

    contours_ = large_contours;
}

void Contours::filterLargest()
{
    if (contours_.size() <= 0)
        return;

    std::vector<cv::Point> largest_contour;
    double largest_contour_area = 0.0;
    for (std::vector<cv::Point> &contour : contours_)
    {
        double contour_area = cv::contourArea(contour);
        if (contour_area > largest_contour_area)
        {
            largest_contour_area = contour_area;
            largest_contour = contour;
        }
    }

    if (largest_contour_area > 0.0)
    {
    	contours_.clear();
    	contours_.push_back(largest_contour);
    }
}

float Contours::centerX()
{
  std::vector<cv::Point> all_contour;
  for (std::vector<cv::Point> &contour : contours_)
  {
    for (cv::Point &point : contour)
    {
      all_contour.push_back(point);
    }
  }

  cv::Moments moments = cv::moments(all_contour, false);

  return (moments.m10 / moments.m00);
}

float Contours::centerY()
{
  std::vector<cv::Point> all_contour;
  for (std::vector<cv::Point> &contour : contours_)
  {
    for (cv::Point &point : contour)
    {
      all_contour.push_back(point);
    }
  }

  cv::Moments moments = cv::moments(all_contour, false);

  return (moments.m01 / moments.m00);
}

float Contours::minX()
{
    float min_x = alg::infinityFloat();
    for (std::vector<cv::Point> &contour : contours_)
    {
        for (cv::Point &point : contour)
        {
            min_x = alg::minValue(min_x, point.x);
        }
    }

    return min_x;
}

float Contours::minY()
{
    float min_y = alg::infinityFloat();
    for (std::vector<cv::Point> &contour : contours_)
    {
        for (cv::Point &point : contour)
        {
            min_y = alg::minValue(min_y, point.y);
        }
    }

    return min_y;
}

float Contours::maxX()
{
    float max_x = 0.0;
    for (std::vector<cv::Point> &contour : contours_)
    {
        for (cv::Point &point : contour)
        {
            max_x = alg::maxValue(max_x, point.x);
        }
    }

    return max_x;
}

float Contours::maxY()
{
    float max_y = 0.0;
    for (std::vector<cv::Point> &contour : contours_)
    {
        for (cv::Point &point : contour)
        {
            max_y = alg::maxValue(max_y, point.y);
        }
    }

    return max_y;
}

void Contours::expand(float value)
{
    for (std::vector<cv::Point> &contour : contours_)
    {
        if (contour.size() < 1)
            continue;

        float center_x = 0;
        float center_y = 0;

        for (cv::Point &point : contour)
        {
            center_x += point.x;
            center_y += point.y;
        }

        center_x /= contour.size();
        center_y /= contour.size();

        for (cv::Point &point : contour)
        {
            float range = alg::distance(point.x - center_x, point.y - center_y);

            point.x = center_x + ((point.x - center_x) * (range + value) / range);
            point.y = center_y + ((point.y - center_y) * (range + value) / range);
        }
    }
}

void Contours::strecthUp(float value)
{
    for (std::vector<cv::Point> &contour : contours_)
    {
        if (contour.size() < 1)
            continue;

        float center_y = 0;
        for (cv::Point &point : contour)
        {
            center_y += point.y;
        }
	    center_y /= contour.size();

        for (cv::Point &point : contour)
        {
            if (point.y < center_y)
            {
                point.y = point.y - value;
            }
        }
    }
}

std::vector<std::vector<cv::Point>> Contours::splitLeft(float x)
{
  std::vector<std::vector<cv::Point>> left_contours;

  for (std::vector<cv::Point> &contour : contours_)
  {
    std::vector<cv::Point> left_contour;
    for (cv::Point &point : contour)
    {
      if (point.x < x)
        left_contour.push_back(point);
    }

    if (left_contour.size() > 2)
      left_contours.push_back(left_contour);
  }

  return left_contours;
}

std::vector<std::vector<cv::Point>> Contours::splitRight(float x)
{
  std::vector<std::vector<cv::Point>> right_contours;

  for (std::vector<cv::Point> &contour : contours_)
  {
    std::vector<cv::Point> right_contour;
    for (cv::Point &point : contour)
    {
      if (point.x > x)
        right_contour.push_back(point);
    }

    if (right_contour.size() > 2)
      right_contours.push_back(right_contour);
  }

  return right_contours;
}

void Contours::filterRect(float x, float y, float width, float height, float value)
{
    float max_x = alg::maxValue(x + width, maxX());
    float max_y = alg::maxValue(y + height, maxY());

    cv::Mat binary_mat = getBinaryMat(cv::Size(max_x, max_y));
	cv::rectangle(binary_mat, cv::Rect(x, y, width, height), value, cv::FILLED);

    find(binary_mat);
}

void Contours::convexHull()
{
    if (contours_.size() <= 0)
        return;

    std::vector<std::vector<cv::Point>> convex_hulls;
    for (std::vector<cv::Point> &contour : contours_)
    {
        std::vector<int> hull;
        cv::convexHull(cv::Mat(contour), hull, true);

        std::vector<cv::Point> convex_hull;
        convex_hull.push_back(contour[hull[hull.size() - 1]]);
        for (unsigned int i = 0; i < hull.size(); i++)
        {
            convex_hull.push_back(contour[hull[i]]);
        }

        convex_hulls.push_back(convex_hull);
    }

    contours_ = convex_hulls;
}

std::vector<std::vector<cv::Point>> Contours::leftWallContours()
{
    std::vector<std::vector<cv::Point>> left_contour;
    float max_x = 0.0;
    float min_x = INT16_MAX;
    float con_y = 0.0;
    float current_y = 0.0;

    for (std::vector<cv::Point> &contour : contours_)
    {
        float current_min_x = INT16_MAX;
        for (cv::Point &point : contour)
        {
            max_x = alg::maxValue(max_x, point.x);
            current_min_x = alg::minValue(current_min_x, point.x);
            current_y = alg::maxValue(current_y, point.y);

        }

        if(max_x < Robot::Camera::getInstance()->getWidth()
            && (current_min_x < Robot::Camera::getInstance()->getWidth()/4)
            && current_min_x < min_x
            && current_y > con_y)
        {
            con_y = current_y;
            min_x = current_min_x;
            left_contour.clear();
            left_contour.push_back(contour);
        }
    }

    return left_contour;
}

std::vector<std::vector<cv::Point>> Contours::rightWallContours()
{
    std::vector<std::vector<cv::Point>> right_contour;
    float min_x = 0.0;
    float con_y = 0.0;
    float current_y = 0.0;

    for (std::vector<cv::Point> &contour : contours_)
    {
        float current_min_x = INT16_MAX;
        for (cv::Point &point : contour)
        {
            current_min_x = alg::minValue(current_min_x, point.x);
            current_y = alg::maxValue(current_y, point.y);

        }

        if(current_min_x > 0.0
            && current_min_x > (Robot::Camera::getInstance()->getWidth()/4)
            && current_min_x > min_x
            && current_y > con_y)
        {
            min_x = current_min_x;
            con_y = current_y;
            right_contour.clear();
            right_contour.push_back(contour);
        }
    }

    return right_contour;
}

float Contours::xOfMaxY()
{
    float max_y = 0.0;
    float x = 0.0;

    for (std::vector<cv::Point> &contour : contours_)
    {
        for (cv::Point &point : contour)
        {
            if(point.y > max_y)
            {
                max_y = point.y;
                x = point.x;
            }
        }
    }

    return x;
}