#include "vision/camera.h"
#include "algebra.h"

#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>

#define CAMERA_DEVICE_0 "/dev/video0"
#define CAMERA_DEVICE_1 "/dev/video0"

using namespace Robot;

Camera* Camera::unique_instance_ = NULL;

Camera::Camera()
{
  camera_fd_ = -1;
  camera_name_ = "/dev/video0";

  Config::Section *section = config_.addSection("Transform");
  section->addParam<int>("width", 320)->setBindValue(&width_);
  section->addParam<int>("height", 240)->setBindValue(&height_);
  section->addParam<float>("field_of_view", 78.0)->setBindValue(&field_of_view_);

  section = config_.addSection("Setting");
  section->addParam<int>("brightness", 100)->setBindValue(&brightness_);
  section->addParam<int>("contrast", 100)->setBindValue(&contrast_);
  section->addParam<int>("saturation", 100)->setBindValue(&saturation_);
  section->addParam<int>("gain", 50)->setBindValue(&gain_);
  section->addParam<int>("exposure", 300)->setBindValue(&exposure_);
  section->addParam<int>("temperature", 4000)->setBindValue(&temperature_);

  config_.sync("camera.yaml");

  calculateFieldOfView();
  setCameraSettings();
}

Camera::~Camera()
{
  closePort();
  delete fbuffer_;
}

Camera *Camera::getInstance()
{
  if (unique_instance_ == NULL)
    unique_instance_ = new Camera();

  return unique_instance_;
}

void Camera::loadConfig()
{
  config_.load("camera.yaml");
  calculateFieldOfView();
  setCameraSettings();
}

void Camera::saveConfig()
{
  getCameraSettings();
  config_.save("camera.yaml");
}

bool Camera::isPortExist()
{
  struct stat st;
  if (stat("/dev/video0", &st) != -1 && S_ISCHR(st.st_mode))
  {
    camera_name_ = "/dev/video0";
    return true;
  }
  else if (stat("/dev/video1", &st) != -1 && S_ISCHR(st.st_mode))
  {
    camera_name_ = "/dev/video1";
    return true;
  }

  fprintf(stderr, "Cannot identify camera: %d, %s\n", errno, strerror(errno));
  return false;
}

bool Camera::openPort()
{
  if (!isPortExist())
    return false;

  camera_fd_ = open(camera_name_.c_str(), O_RDWR | O_NONBLOCK, 0);
  if (camera_fd_ == -1)
  {
    fprintf (stderr, "Cannot open '%s': %d, %s\n", camera_name_.c_str(), errno, strerror(errno));
    return false;
  }

  n_buffers_ = 0;
  fbuffer_ = new FrameBuffer(width_, height_);

  if (!initCameraFormat())
  {
    closePort();
    return false;
  }

  if (!initFrameRate())
  {
    closePort();
    return false;
  }

  if (!mapMemory())
  {
    closePort();
    return false;
  }

  if (!setCameraStream(true))
  {
    closePort();
    return false;
  }

  if (!initCameraSetting())
  {
    closePort();
    return false;
  }

  return true;
}

bool Camera::initCameraFormat()
{
  struct v4l2_cropcap cropcap;
  memset(&cropcap, 0, sizeof(cropcap));

  cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (ioctl(camera_fd_, VIDIOC_CROPCAP, &cropcap) == 0)
  {
    struct v4l2_crop crop;
    crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    crop.c = cropcap.defrect;

    ioctl(camera_fd_, VIDIOC_S_CROP, &crop);
  }

  struct v4l2_format fmt;
  memset(&fmt, 0, sizeof(fmt));

  fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  fmt.fmt.pix.width       = width_;
  fmt.fmt.pix.height      = height_;
  fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
  fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;

  if (ioctl (camera_fd_, VIDIOC_S_FMT, &fmt))
  {
    fprintf (stderr, "Fail to format camera: %d, %s\n", errno, strerror(errno));
    return false;
  }

  unsigned int min = fmt.fmt.pix.width * 2;
  if (fmt.fmt.pix.bytesperline < min)
    fmt.fmt.pix.bytesperline = min;

  min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
  if (fmt.fmt.pix.sizeimage < min)
    fmt.fmt.pix.sizeimage = min;

  return true;
}

bool Camera::initFrameRate()
{
  struct v4l2_streamparm fps;
  memset(&fps, 0, sizeof(fps));

  fps.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (ioctl(camera_fd_, VIDIOC_G_PARM, &fps) == -1)
  {
    fprintf (stderr, "Fail to get camera framerate: %d, %s\n", errno, strerror(errno));
    return false;
  }

  fps.parm.capture.timeperframe.numerator = 1;
  fps.parm.capture.timeperframe.denominator = 30;

  if (ioctl(camera_fd_, VIDIOC_S_PARM, &fps) == -1)
  {
    fprintf (stderr, "Fail to set camera framerate: %d, %s\n", errno, strerror(errno));
    return false;
  }

  return true;
}

bool Camera::setCameraStream(bool turn_on)
{
  enum v4l2_buf_type type;
  type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (ioctl (camera_fd_, (turn_on) ? VIDIOC_STREAMON : VIDIOC_STREAMOFF, &type) == -1)
  {
    fprintf (stderr, "Fail to initialize camera stream: %d, %s\n", errno, strerror(errno));
    return false;
  }

  return true;
}

bool Camera::mapMemory()
{
  struct v4l2_requestbuffers req;
  memset(&req, 0, sizeof(req));

  req.count   = 4;
  req.type    = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  req.memory  = V4L2_MEMORY_MMAP;

  if (ioctl(camera_fd_, VIDIOC_REQBUFS, &req) == -1)
  {
    fprintf (stderr, "Fail to initialize memory mapping: %d, %s\n", errno, strerror(errno));
    return false;
  }

  if (req.count < 2) {
    fprintf (stderr, "Insufficient buffer memory on camera");
    return false;
  }

  buffers_ = (Buffer *)calloc(req.count, sizeof(*buffers_));
  if (!buffers_) {
    fprintf (stderr, "Out of memory\n");
    return false;
  }

  for (n_buffers_ = 0; n_buffers_ < req.count; ++n_buffers_)
  {
    struct v4l2_buffer buf;
    memset(&buf, 0, sizeof(buf));

    buf.type    = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory  = V4L2_MEMORY_MMAP;
    buf.index   = n_buffers_;

    if (ioctl(camera_fd_, VIDIOC_QUERYBUF, &buf) == -1)
    {
      fprintf (stderr, "Fail to query the status of a buffer: %d, %s\n", errno, strerror(errno));
      return false;
    }

    buffers_[n_buffers_].length = buf.length;
    buffers_[n_buffers_].start = mmap(NULL, buf.length, PROT_READ | PROT_WRITE,MAP_SHARED, camera_fd_, buf.m.offset);

    if (buffers_[n_buffers_].start == MAP_FAILED)
    {
      fprintf (stderr, "Fail to map memory\n");
      return false;
    }
  }

  for (unsigned int n = 0; n < n_buffers_; ++n)
  {
    struct v4l2_buffer buf;
    memset(&buf, 0, sizeof(buf));

    buf.type    = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory  = V4L2_MEMORY_MMAP;
    buf.index   = n;

    if (ioctl(camera_fd_, VIDIOC_QBUF, &buf) == -1)
    {
      fprintf (stderr, "Queue buffers failed: %d, %s\n", errno, strerror(errno));
      return false;
    }
  }

  return true;
}

bool Camera::unmapMemory()
{
  for (unsigned int i = 0; i < n_buffers_; i++)
  {
    if (munmap(buffers_[i].start, buffers_[i].length) == -1)
    {
      fprintf (stderr, "Fail to unmap memory\n");
      return false;
    }
  }

  free(buffers_);

  return true;
}

bool Camera::initCameraSetting()
{
  getCameraSettings();

  setControl(V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_MANUAL);
  setControl(V4L2_CID_AUTO_WHITE_BALANCE, false);
  setControl(V4L2_CID_SHARPNESS, 0);
  setControl(V4L2_CID_FOCUS_AUTO, false);
  setControl(V4L2_CID_FOCUS_ABSOLUTE, 0);
  setControl(V4L2_CID_POWER_LINE_FREQUENCY,V4L2_CID_POWER_LINE_FREQUENCY_50HZ);

  loadConfig();

  return true;
}

bool Camera::closePort()
{
  if (camera_fd_ < 0)
    return true;

  setCameraStream(false);
  unmapMemory();

  close(camera_fd_);
  camera_fd_ = -1;

  return true;
}

bool Camera::readFrame()
{
  struct v4l2_buffer buf;
  memset(&buf, 0, sizeof(buf));

  buf.type    = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory  = V4L2_MEMORY_MMAP;

  if (ioctl (camera_fd_, VIDIOC_DQBUF, &buf) == -1)
  {
    fprintf (stderr, "Fail to get camera frame: %d, %s\n", errno, strerror(errno));
    closePort();
    return false;
  }

  assert(buf.index < n_buffers_);

  for (int i = 0; i < fbuffer_->m_YUVFrame->m_ImageSize / 2; i++)
  {
    fbuffer_->m_YUVFrame->m_ImageData[i] = ((unsigned char*)buffers_[buf.index].start)[i];
  }

  fbuffer_->yuv2rgb();
  fbuffer_->rgb2hsv();
  fbuffer_->rgb2gray();

  if (ioctl(camera_fd_, VIDIOC_QBUF, &buf) == -1)
  {
    fprintf (stderr, "Queue buffer failed: %d, %s\n", errno, strerror(errno));
    closePort();
    return false;
  }

  return true;
}

bool Camera::captureFrame()
{
  if (!isPortOpen())
  {
    if (!openPort())
      return false;
  }

  do
  {
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(camera_fd_, &fds);

    struct timeval tv;
    tv.tv_sec = 2;
    tv.tv_usec = 0;

    int result = select(camera_fd_ + 1, &fds, NULL, NULL, &tv);

    if (result == -1)
    {
      if (errno == EINTR)
        continue;

      fprintf (stderr, "Capture frame failed: %d, %s\n", errno, strerror(errno));
      return false;
    }

    if (result == 0)
    {
      fprintf (stderr, "Capture frame timeout: %d, %s\n", errno, strerror(errno));
      return false;
    }
  }
  while (!readFrame() && isPortOpen());

  return true;
}

int Camera::getControl(int control)
{
  if (!isPortOpen())
    return -1;

  struct v4l2_queryctrl queryctrl;
  memset(&queryctrl, 0, sizeof(queryctrl));

  queryctrl.id = control;

  if (ioctl(camera_fd_, VIDIOC_QUERYCTRL, &queryctrl) == -1)
  {
    fprintf(stderr, "Query control failed: %d, %s\n", errno, strerror(errno));
    return -1;
  }

  struct v4l2_control control_s;
  memset(&control_s, 0, sizeof(control_s));

  control_s.id = control;

  if (ioctl(camera_fd_, VIDIOC_G_CTRL, &control_s) == -1)
  {
    fprintf(stderr, "Fail to get Control:  %d, %s\n", errno, strerror(errno));
    return -1;
  }

  return control_s.value;
}

int Camera::setControl(int control, int value)
{
  if (!isPortOpen())
    return -1;

  struct v4l2_queryctrl queryctrl;
  memset(&queryctrl, 0, sizeof(queryctrl));

  queryctrl.id = control;

  if (ioctl(camera_fd_, VIDIOC_QUERYCTRL, &queryctrl) == -1)
  {
    fprintf(stderr, "Query control failed: %d, %s\n", errno, strerror(errno));
    return -1;
  }

  if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)
  {
    fprintf(stderr, "Setting control is disabled on %d", control);
    return -1;
  }

  struct v4l2_control control_s;
  memset(&control_s, 0, sizeof(control_s));

  if (value < queryctrl.minimum)
    value = queryctrl.minimum;
  else if (value > queryctrl.maximum)
    value = queryctrl.maximum;

  control_s.id = control;
  control_s.value = value;

  if (ioctl (camera_fd_, VIDIOC_S_CTRL, &control_s) == -1)
  {
    fprintf(stderr, "Fail to set control: %d, %s\n", errno, strerror(errno));
    return -1;
  }

  return value;
}

int Camera::resetControl(int control)
{
  if (!isPortOpen())
    return -1;

  struct v4l2_queryctrl queryctrl;
  memset(&queryctrl, 0, sizeof(queryctrl));

  queryctrl.id = control;

  if (ioctl(camera_fd_, VIDIOC_QUERYCTRL, &queryctrl) == -1)
  {
    fprintf(stderr, "Query control failed: %d, %s\n", errno, strerror(errno));
    return -1;
  }

  if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)
  {
    fprintf(stderr, "Setting control is disabled on %d", control);
    return -1;
  }

  struct v4l2_control control_s;
  memset(&control_s, 0, sizeof(control_s));

  control_s.id = control;
  control_s.value = queryctrl.default_value;

  if (ioctl (camera_fd_, VIDIOC_S_CTRL, &control_s) == -1)
  {
    fprintf(stderr, "Fail to reset control: %d, %s\n", errno, strerror(errno));
    return -1;
  }

  return control_s.value;
}

void Camera::getCameraSettings()
{
  getBrightness();
  getContrast();
  getSaturation();
  getGain();
  getExposure();
  getTemperature();
}

void Camera::setCameraSettings()
{
  setBrightness(brightness_);
  setContrast(contrast_);
  setSaturation(saturation_);
  setGain(gain_);
  setExposure(exposure_);
  setTemperature(temperature_);
}

void Camera::calculateFieldOfView()
{
  float diagonal = alg::distance(width_, height_);
  float depth = (diagonal / 2) / tan(field_of_view_ * alg::deg2Rad() / 2);

  view_h_angle_ = 2 * atan2(width_ / 2, depth) * alg::rad2Deg();
  view_v_angle_ = 2 * atan2(height_ / 2, depth) * alg::rad2Deg();
}