#include <stdlib.h>
#include <string.h>
#include "vision/image.h"

#include <opencv2/opencv.hpp>

Image::Image(int width, int height, int pixelsize) :
        m_Width(width),
        m_Height(height),
        m_PixelSize(pixelsize),
        m_NumberOfPixels(m_Width * m_Height),
        m_WidthStep(m_Width * m_PixelSize),
        m_ImageSize(m_Height * m_WidthStep)
{
    m_ImageData = new unsigned char[m_ImageSize];
}

Image::~Image()
{
	delete[] m_ImageData;
    m_ImageData = 0;
}

Image &Image::operator = (Image &img)
{
	memcpy(m_ImageData, img.m_ImageData, m_ImageSize);
	return *this;
}


void Image::fromMat(cv::Mat mat)
{
    cv::cvtColor(mat, mat, CV_BGR2RGB);
    memcpy(m_ImageData, mat.data, m_ImageSize);
}

void Image::filterMat(cv::Mat mat, uint8_t r, uint8_t g, uint8_t b)
{
    int j = -1;
    for (int i = 0; i < m_NumberOfPixels; i++)
    {        
        if (i % m_Width == 0)
            j++;
        
        if (mat.at<uint8_t>(cv::Point(i % m_Width, j)) > 0)
        {
            m_ImageData[i * m_PixelSize + 0] = r;
            m_ImageData[i * m_PixelSize + 1] = g;
            m_ImageData[i * m_PixelSize + 2] = b;
        }
    }
}