#include "vision/rects.h"

cv::Mat Rects::getBinaryMatLine(cv::Size mat_size, int line_size)
{
    cv::Mat binary_mat(mat_size, CV_8UC1);
    binary_mat = cv::Scalar(0);
    
    if (rects_.size() > 0)
    {
        for (cv::Rect &rect : rects_)
        {
            cv::Point top_left(rect.x, rect.y);
            cv::Point bottom_right(rect.x + rect.width, rect.y + rect.height);
		    cv::rectangle(binary_mat, top_left, bottom_right, 255, line_size);
        }
    }

    return binary_mat;
}

cv::Rect Rects::getFirstRect()
{
    if (rects_.size() <= 0)
        return cv::Rect(0, 0, 0, 0);
    
    return rects_[0];
}

cv::Point Rects::getFirstRectCenter()
{
    cv::Point center;
    cv::Rect rect = getFirstRect();

    center.x = rect.x + (rect.width / 2);
    center.y = rect.y + (rect.height / 2);

    return center;
}

void Rects::filterLargest()
{
    if (rects_.size() <= 0)
        return;

    cv::Rect largest_rect;
    double largest_rect_area = 0.0;
    for (cv::Rect &rect : rects_)
    {
        double rect_area = rect.width * rect.height;
        if (rect_area > largest_rect_area)
        {
            largest_rect_area = rect_area;
            largest_rect = rect;
        }
    }

    if (largest_rect_area > 0.0)
    {
    	rects_.clear();
    	rects_.push_back(largest_rect);
    }
}