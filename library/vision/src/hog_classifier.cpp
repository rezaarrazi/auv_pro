#include "vision/hog_classifier.h"
#include "algebra.h"

#include <opencv2/imgproc.hpp>
#include <stdio.h>

HOGClassifier::HOGClassifier()
{
  descriptor_loaded_ = false;
  classifier_type_ = -1;
}

HOGClassifier::HOGClassifier(int type)
{
  descriptor_loaded_ = false;
  classifier_type_ = -1;

  setClassifierType(type);
}

void HOGClassifier::setClassifierType(int type)
{
  std::string descriptor_file_path;
  switch (type)
  {
  case CLASSIFIER_TYPE_BALL:
    descriptor_file_path = "../../data/" + linux::getHostName() + "/hog_classifier/ball_descriptor.yml";
    break;

  default:
    return;
  }

  classifier_type_ = type;
  descriptor_loaded_ = loadDescriptor(descriptor_file_path);
}

bool HOGClassifier::loadDescriptor(std::string descriptor_file_path)
{
	if (!hog_descriptor_.load(descriptor_file_path))
  {
    printf("failed to load descriptor %s\n", descriptor_file_path.c_str());
    return false;
	}

  return true;
}

std::vector<cv::Rect> HOGClassifier::classify(cv::Mat input)
{
  std::vector<cv::Rect> rects;
  if (!descriptor_loaded_)
    return rects;

  cv::Mat gray = input.clone();
  cv::cvtColor(input, gray, cv::COLOR_BGR2GRAY);

  hog_descriptor_.detectMultiScale(gray, rects, 0, cv::Size(8,8), cv::Size(32,32), 1.05, 2);

  return rects;
}