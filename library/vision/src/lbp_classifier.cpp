#include "vision/lbp_classifier.h"
#include "algebra.h"
#include "linux.h"

#include <opencv2/imgproc.hpp>
#include <stdio.h>

LBPClassifier::LBPClassifier()
{
  classifier_loaded_ = false;
  classifier_type_ = -1;
}

LBPClassifier::LBPClassifier(int type)
{
  classifier_loaded_ = false;
  classifier_type_ = -1;

  setClassifierType(type);
}

void LBPClassifier::setClassifierType(int type)
{
  std::string classifier_file_path;
  switch (type)
  {
  case CLASSIFIER_TYPE_BALL:
    classifier_file_path = "../../data" + linux::getHostName() + "/lbp_classifier/ball_cascade.xml";
    break;

  default:
    return;
  }

  classifier_type_ = type;
  classifier_loaded_ = loadClassifier(classifier_file_path);
}

bool LBPClassifier::loadClassifier(std::string classifier_file_path)
{
	if (!cascade_classifier_.load(classifier_file_path))
  {
    printf("failed to load cascade %s\n", classifier_file_path.c_str());
    return false;
	}

  return true;
}

std::vector<cv::Rect> LBPClassifier::classify(cv::Mat input)
{
  std::vector<cv::Rect> rects;
  if (!classifier_loaded_)
    return rects;

  cv::Mat gray = input.clone();
  cv::cvtColor(input, gray, cv::COLOR_BGR2GRAY);

  cascade_classifier_.detectMultiScale(gray, rects, 1.1, 5, 8, cv::Size(5, 5));

  return rects;
}