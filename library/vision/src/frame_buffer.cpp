#include "vision/frame_buffer.h"
#include "vision/camera.h"

#include <opencv2/opencv.hpp>

FrameBuffer::FrameBuffer()
{
    Robot::Camera *camera = Robot::Camera::getInstance();
    FrameBuffer(camera->getWidth(), camera->getWidth());
}

FrameBuffer::FrameBuffer(int width, int height)
{
    m_YUVFrame = new Image(width, height, Image::YUV_PIXEL_SIZE);
    m_RGBFrame = new Image(width, height, Image::RGB_PIXEL_SIZE);
    m_RGBInput.create(height, width, CV_MAKETYPE(CV_8U, 3));
    m_RGBLapangan.create(height, width, CV_MAKETYPE(CV_8U, 3));
    m_HSVInput.create(height, width, CV_MAKETYPE(CV_8U, 3));
}

FrameBuffer::~FrameBuffer()
{
    delete m_YUVFrame;
    delete m_RGBFrame;
    m_RGBInput.release();
    m_RGBLapangan.release();
    m_HSVInput.release();
}

void FrameBuffer::rgb2hsv()
{
    cv::cvtColor(m_RGBInput, m_HSVInput, CV_RGB2HSV);
}

void FrameBuffer::rgb2gray()
{
    cv::cvtColor(m_RGBInput, m_GrayInput, CV_RGB2GRAY);
}

void FrameBuffer::yuv2rgb()
{
    unsigned char *yuyv, *rgb;
    int z = 0;

    yuyv = m_YUVFrame->m_ImageData;
    rgb = m_RGBFrame->m_ImageData;

    for(int height = 0; height < m_YUVFrame->m_Height; height++)
    {
        for(int width = 0; width < m_YUVFrame->m_Width; width++)
        {
            int r, g, b;
            int y, u, v;

            if(!z)
                y = yuyv[0] << 8;
            else
                y = yuyv[2] << 8;
            u = yuyv[1] - 128;
            v = yuyv[3] - 128;

            r = (y + (359 * v)) >> 8;
            g = (y - (88 * u) - (183 * v)) >> 8;
            b = (y + (454 * u)) >> 8;

            *(rgb++) = (r > 255) ? 255 : ((r < 0) ? 0 : r);
            *(rgb++) = (g > 255) ? 255 : ((g < 0) ? 0 : g);
            *(rgb++) = (b > 255) ? 255 : ((b < 0) ? 0 : b);

            if (z++)
            {
                z = 0;
                yuyv += 4;
            }
        }
    }

    memcpy(m_RGBInput.data, m_RGBFrame->m_ImageData, m_RGBFrame->m_ImageSize);
    cv::cvtColor(m_RGBInput, m_RGBInput, CV_BGR2RGB);
    m_RGBLapangan = m_RGBInput.clone();
}