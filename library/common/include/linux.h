#ifndef LINUX_H_
#define LINUX_H_

#include <string>

namespace linux
{
  std::string getHostName();
  std::string getEnv(std::string env);
  bool isRoot();

  bool isDirectoryExist(std::string path);
  bool createDirectory(std::string path);

  bool isFileExist(std::string path);
  bool createFile(std::string path);
}

#endif