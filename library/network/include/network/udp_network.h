#ifndef NETWORK_UDP_NETWORK
#define NETWORK_UDP_NETWORK

class UDPNetwork
{
private:

    int socket_;
    int receive_port_;
    int broadcast_port_;

    bool enable_receive_;
    bool enable_broadcast_;

public:

    UDPNetwork();
    ~UDPNetwork();

    bool openPort(int port);
    bool openPort(int receive_port, int broadcast_port);
    bool closePort();

    int receive(void *buffer, unsigned int size);
    bool broadcast(const void *data, unsigned int length);

    void setEnableReceive(bool enable) { enable_receive_ = enable; }
    bool getEnableReceive() { return enable_receive_; }

    void setEnableBroadcast(bool enable) { enable_broadcast_ = enable; }
    bool getEnableBroadcast() { return enable_broadcast_; }
};

#endif