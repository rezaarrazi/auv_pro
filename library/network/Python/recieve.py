import socket
from collections import namedtuple
from struct import *
import threading
import time

UDP_IP = "192.168.2.255" #127.0.0.1
UDP_PORT = 4003

RANGE_CHANNEL = 0x00000000

KEY_ROBOT_CHANNEL_PITCH             = RANGE_CHANNEL + 0
KEY_ROBOT_CHANNEL_ROLL              = RANGE_CHANNEL + 1
KEY_ROBOT_CHANNEL_THROTTLE          = RANGE_CHANNEL + 2
KEY_ROBOT_CHANNEL_YAW               = RANGE_CHANNEL + 3
KEY_ROBOT_CHANNEL_FORWARD           = RANGE_CHANNEL + 4
KEY_ROBOT_CHANNEL_LATERAL           = RANGE_CHANNEL + 5

CHANNEL_PITCH    = 1500
CHANNEL_ROLL     = 1500
CHANNEL_THROTTLE = 1500
CHANNEL_YAW      = 1500
CHANNEL_FORWARD  = 1500
CHANNEL_LATERAL  = 1500

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))

exitFlag = 0
class udpMavlink (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        while True:
            print("[CHANNEL_PITCH] %d, [CHANNEL_ROLL] %d, [CHANNEL_THROTTLE] %d"% (CHANNEL_PITCH, CHANNEL_ROLL, CHANNEL_THROTTLE))
            print("[CHANNEL_YAW] %d, [CHANNEL_FORWARD] %d, [CHANNEL_LATERAL] %d"% (CHANNEL_YAW, CHANNEL_FORWARD, CHANNEL_LATERAL))

            data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
            MessageData = namedtuple('MessageData', 'header version length flags')._make(unpack('LHHL', data[:12]))
            
            keyval_data = data[12:]
            keyval_data_size = 8
            for i in range(MessageData.length):
                KeyValData = namedtuple('KeyValData', 'key value')._make(unpack('Ll', keyval_data[keyval_data_size*(i):keyval_data_size*(i+1)]))
                if KeyValData.key == KEY_ROBOT_CHANNEL_PITCH:
                    CHANNEL_PITCH = KeyValData.value
                elif KeyValData.key == KEY_ROBOT_CHANNEL_ROLL :
                    CHANNEL_ROLL = KeyValData.value
                elif KeyValData.key == KEY_ROBOT_CHANNEL_THROTTLE:
                    CHANNEL_THROTTLE = KeyValData.value
                elif KeyValData.key == KEY_ROBOT_CHANNEL_YAW:
                    CHANNEL_YAW = KeyValData.value
                elif KeyValData.key == KEY_ROBOT_CHANNEL_FORWARD:
                    CHANNEL_FORWARD = KeyValData.value
                elif KeyValData.key == KEY_ROBOT_CHANNEL_LATERAL:
                    CHANNEL_LATERAL = KeyValData.value
            # print (MessageData)