/*******************************************************************************
#                                                                              #
#      MJPG-streamer allows to stream JPG frames from an input-plugin          #
#      to several output plugins                                               #
#                                                                              #
#      Copyright (C) 2007 Tom Stöveken                                         #
#                                                                              #
# This program is free software; you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation; version 2 of the License.                      #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program; if not, write to the Free Software                  #
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA    #
#                                                                              #
*******************************************************************************/

#ifndef HTTPD_H_
#define HTTPD_H_

#define IO_BUFFER 256
#define BUFFER_SIZE 1024

/* the boundary is used for the M-JPEG stream, it separates the multipart stream of pictures */
#define BOUNDARY "boundarydonotcross"

/*
 * this defines the buffer size for a JPG-frame
 * selecting to large values will allocate much wasted RAM for each buffer
 * selecting to small values will lead to crashes due to to small buffers
 */
#define MAX_FRAME_SIZE (256*1024)
#define TEN_K (10*1024)


#define ABS(a) (((a) < 0) ? -(a) : (a))
#define MIN_VAL(a, b) (((a) < (b)) ? (a) : (b))
#define MAX_VAL(a, b) (((a) > (b)) ? (a) : (b))
#define LENGTH_OF(x) (sizeof(x)/sizeof(x[0]))

#ifdef DEBUG
#define DBG(...) fprintf(stderr, " DBG(%s, %s(), %d): ", __FILE__, __FUNCTION__, __LINE__); fprintf(stderr, __VA_ARGS__)
#else
#define DBG(...)
#endif

#define LOG(...) { char _bf[1024] = {0}; snprintf(_bf, sizeof(_bf)-1, __VA_ARGS__); fprintf(stderr, "%s", _bf); syslog(LOG_INFO, "%s", _bf); }

#define OUTPUT_PLUGIN_PREFIX " o: "
#define OPRINT(...) { char _bf[1024] = {0}; snprintf(_bf, sizeof(_bf)-1, __VA_ARGS__); fprintf(stderr, "%s", OUTPUT_PLUGIN_PREFIX); fprintf(stderr, "%s", _bf); syslog(LOG_INFO, "%s", _bf); }

#include "vision.h"

#include <map>
#include <string>

/*
 * Standard header to be send along with other header information like mimetype.
 *
 * The parameters should ensure the browser does not cache our answer.
 * A browser should connect for each file and not serve files from his cache.
 * Using cached pictures would lead to showing old/outdated pictures
 * Many browser seem to ignore, or at least not always obey those headers
 * since i observed caching of files from time to time.
 */
#define STD_HEADER "Connection: close\r\n" \
                   "Server: MJPG-Streamer/0.2\r\n" \
                   "Cache-Control: no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0\r\n" \
                   "Pragma: no-cache\r\n" \
                   "Expires: Mon, 3 Jan 2000 12:34:56 GMT\r\n"


typedef enum{
    IN_CMD_UNKNOWN = 0,
    IN_CMD_HELLO,
    IN_CMD_RELOAD,
    IN_CMD_SAVE,
    IN_CMD_GAIN_PLUS,
    IN_CMD_GAIN_MINUS,
    IN_CMD_EXPOSURE_PLUS,
    IN_CMD_EXPOSURE_MINUS,
    IN_CMD_BRIGHTNESS_PLUS,
    IN_CMD_BRIGHTNESS_MINUS,
    IN_CMD_CONTRAST_PLUS,
    IN_CMD_CONTRAST_MINUS,
    IN_CMD_SATURATION_PLUS,
    IN_CMD_SATURATION_MINUS,
    IN_CMD_TEMPERATURE_PLUS,
    IN_CMD_TEMPERATURE_MINUS,
    IN_CMD_WHITE_MODE,
    IN_CMD_FOCUS_MODE,
    IN_CMD_GAME_MODE,
    IN_CMD_SELECTION_TOOL_MODE,
    IN_CMD_SELECTION_MODE,
    IN_CMD_PLAYER_ROLE,
    IN_CMD_RECORDER,
    IN_CMD_CAMERA_RELOAD,
    IN_CMD_CAMERA_SAVE,

    IN_CMD_HUE_SET,
    IN_CMD_HUE_PLUS,
    IN_CMD_HUE_MINUS,
    IN_CMD_TOLERANCE_SET,
    IN_CMD_TOLERANCE_PLUS,
    IN_CMD_TOLERANCE_MINUS,
    IN_CMD_MIN_SATURATION_SET,
    IN_CMD_MIN_SATURATION_PLUS,
    IN_CMD_MIN_SATURATION_MINUS,
    IN_CMD_MAX_SATURATION_SET,
    IN_CMD_MAX_SATURATION_PLUS,
    IN_CMD_MAX_SATURATION_MINUS,
    IN_CMD_MIN_VALUE_SET,
    IN_CMD_MIN_VALUE_PLUS,
    IN_CMD_MIN_VALUE_MINUS,
    IN_CMD_MAX_VALUE_SET,
    IN_CMD_MAX_VALUE_PLUS,
    IN_CMD_MAX_VALUE_MINUS,

    /* walk tuner */
    IN_CMD_WALK_MODE,
    IN_CMD_WALK_SAVE,
    IN_CMD_WALK_LOAD,
    IN_CMD_WALK_X_OFFSET,
    IN_CMD_WALK_Y_OFFSET,
    IN_CMD_WALK_Z_OFFSET,
    IN_CMD_WALK_ROLL_OFFSET,
    IN_CMD_WALK_PITCH_OFFSET,
    IN_CMD_WALK_YAW_OFFSET,
    IN_CMD_WALK_HIP_OFFSET,
    IN_CMD_WALK_AUTO_BALANCE,
    IN_CMD_WALK_PERIOD_TIME,
    IN_CMD_WALK_DSP_RATIO,
    IN_CMD_WALK_STEP_FB_RATIO,
    IN_CMD_WALK_STEP_FB,
    IN_CMD_WALK_STEP_RL,
    IN_CMD_WALK_STEP_DIR,
    IN_CMD_WALK_TURN_AIM,
    IN_CMD_WALK_FOOT_HEIGHT,
    IN_CMD_WALK_SWING_RL,
    IN_CMD_WALK_SWING_TD,
    IN_CMD_WALK_PELVIS_OFFSET,
    IN_CMD_WALK_ARM_SWING_GAIN,
    IN_CMD_WALK_B_KNEE_GAIN,
    IN_CMD_WALK_B_ANKLE_PITCH_GAIN,
    IN_CMD_WALK_B_HIP_ROLL_GAIN,
    IN_CMD_WALK_B_ANKLE_ROLL_GAIN,
    IN_CMD_WALK_P_GAIN,
    IN_CMD_WALK_I_GAIN,
    IN_CMD_WALK_D_GAIN,
    IN_CMD_WALK_BACKWARD_HIP_COMP_RATIO,
    IN_CMD_WALK_FORWARD_HIP_COMP_RATIO,
    IN_CMD_WALK_FOOT_COMP_RATIO,
    IN_CMD_WALK_DSP_COMP_RATIO,
    IN_CMD_WALK_MOVE_ACCEL_RATIO,
    IN_CMD_WALK_FOOT_ACCEL_RATIO,

    IN_CMD_INIT_R_HIP_YAW,
    IN_CMD_INIT_R_HIP_ROLL,
    IN_CMD_INIT_R_HIP_PITCH,
    IN_CMD_INIT_R_KNEE,
    IN_CMD_INIT_R_ANKLE_PITCH,
    IN_CMD_INIT_R_ANKLE_ROLL,
    IN_CMD_INIT_R_ARM_SWING,
    IN_CMD_INIT_R_SHOULDER_PITCH,
    IN_CMD_INIT_R_SHOULDER_ROLL,
    IN_CMD_INIT_R_ELBOW,
    IN_CMD_INIT_R_GRIPPER,

    IN_CMD_INIT_L_HIP_YAW,
    IN_CMD_INIT_L_HIP_ROLL,
    IN_CMD_INIT_L_HIP_PITCH,
    IN_CMD_INIT_L_KNEE,
    IN_CMD_INIT_L_ANKLE_PITCH,
    IN_CMD_INIT_L_ANKLE_ROLL,
    IN_CMD_INIT_L_ARM_SWING,
    IN_CMD_INIT_L_SHOULDER_PITCH,
    IN_CMD_INIT_L_SHOULDER_ROLL,
    IN_CMD_INIT_L_ELBOW,
    IN_CMD_INIT_L_GRIPPER,

    IN_CMD_ARM_POSITION,
    IN_CMD_PARAM_RELOAD,
    IN_CMD_PARAM_SAVE,
    IN_CMD_PARAM_SET,
    IN_CMD_PARAM_PLUS,
    IN_CMD_PARAM_MINUS,
    IN_CMD_STREAM_SHOW,
    IN_CMD_INIT,

    IN_CMD_START,

    IN_CMD_VALVE,
    IN_CMD_OBSTACLE,
    IN_CMD_UP_TERRAIN,
    IN_CMD_DOWN_TERRAIN,
    IN_CMD_SENANG,
    IN_CMD_DANGAK,
    IN_CMD_WALKREADY,
    IN_CMD_START_ROBOT,
    IN_CMD_STOP_ROBOT,
    IN_CMD_SALAM,

    IN_CMD_HANDX,
    IN_CMD_HANDY,
    IN_CMD_HANDZ,

    IN_CMD_HAND2X,
    IN_CMD_HAND2Y,
    IN_CMD_HAND2Z,

    IN_CMD_PAN,
    IN_CMD_TILT,
    IN_CMD_RESET_PAN_TILT,
    IN_CMD_CAPTURE,
    IN_CMD_RECORD,

    IN_CMD_IMAGE_LABEL,
    IN_CMD_INPUT_MODE,
    IN_CMD_VIDEO_MODE,
    IN_CMD_DAY_MODE,
    IN_CMD_IMAGE_CHANGE,
    IN_CMD_KEYBOARD_KEY,
    IN_CMD_KOMPAS_RESET,
    IN_CMD_MODE_RESET,

}in_cmd_type;

/* commands which can be send to the input plugin */
typedef enum {
  OUT_CMD_UNKNOWN = 0,
  OUT_CMD_HELLO
}out_cmd_type;


typedef struct _globals globals;
struct _globals {
    /* signal fresh frames */
    pthread_mutex_t db;
    pthread_cond_t  db_update;

    /* global JPG frame, this is more or less the "database" */
    unsigned char* buf;
    int size;
};

/*
 * Only the following fileypes are supported.
 *
 * Other filetypes are simply ignored!
 * This table is a 1:1 mapping of files extension to a certain mimetype.
 */
static const struct {
  const char *dot_extension;
  const char *mimetype;
} mimetypes[] = {
  { ".html", "text/html" },
  { ".htm",  "text/html" },
  { ".css",  "text/css" },
  { ".js",   "text/javascript" },
  { ".txt",  "text/plain" },
  { ".jpg",  "image/jpeg" },
  { ".jpeg", "image/jpeg" },
  { ".png",  "image/png"},
  { ".gif",  "image/gif" },
  { ".ico",  "image/x-icon" },
  { ".swf",  "application/x-shockwave-flash" },
  { ".cab",  "application/x-shockwave-flash" },
  { ".jar",  "application/java-archive" }
};

/*
 * mapping between command string and command type
 * it is used to find the command for a certain string
 */
static const struct {
  const char *string;
  const in_cmd_type cmd;
} in_cmd_mapping[] = {
  { "reload", IN_CMD_RELOAD },
  { "save", IN_CMD_SAVE },
  { "gain_plus", IN_CMD_GAIN_PLUS },
  { "gain_minus", IN_CMD_GAIN_MINUS },
  { "exposure_plus", IN_CMD_EXPOSURE_PLUS },
  { "exposure_minus", IN_CMD_EXPOSURE_MINUS },
  { "bright_plus", IN_CMD_BRIGHTNESS_PLUS },
  { "bright_minus", IN_CMD_BRIGHTNESS_MINUS },
  { "contrast_plus", IN_CMD_CONTRAST_PLUS },
  { "contrast_minus", IN_CMD_CONTRAST_MINUS },
  { "saturation_plus", IN_CMD_SATURATION_PLUS },
  { "saturation_minus", IN_CMD_SATURATION_MINUS },
  { "temperature_plus", IN_CMD_TEMPERATURE_PLUS },
  { "temperature_minus", IN_CMD_TEMPERATURE_MINUS },
  { "white_mode", IN_CMD_WHITE_MODE },
  { "focus_mode", IN_CMD_FOCUS_MODE },
  { "game_mode", IN_CMD_GAME_MODE },
  { "selection_tool_mode", IN_CMD_SELECTION_TOOL_MODE },
  { "selection_mode", IN_CMD_SELECTION_MODE },
  { "player_role", IN_CMD_PLAYER_ROLE },
  { "recorder", IN_CMD_RECORDER },
  { "camera_reload", IN_CMD_CAMERA_RELOAD },
  { "camera_save", IN_CMD_CAMERA_SAVE },

  { "hue_set", IN_CMD_HUE_SET },
  { "hue_plus", IN_CMD_HUE_PLUS },
  { "hue_minus", IN_CMD_HUE_MINUS },
  { "tolerance_set", IN_CMD_TOLERANCE_SET },
  { "tolerance_plus", IN_CMD_TOLERANCE_PLUS },
  { "tolerance_minus", IN_CMD_TOLERANCE_MINUS },
  { "min_saturation_set", IN_CMD_MIN_SATURATION_SET },
  { "min_saturation_plus", IN_CMD_MIN_SATURATION_PLUS },
  { "min_saturation_minus", IN_CMD_MIN_SATURATION_MINUS },
  { "max_saturation_set", IN_CMD_MAX_SATURATION_SET },
  { "max_saturation_plus", IN_CMD_MAX_SATURATION_PLUS },
  { "max_saturation_minus", IN_CMD_MAX_SATURATION_MINUS },
  { "min_value_set", IN_CMD_MIN_VALUE_SET },
  { "min_value_plus", IN_CMD_MIN_VALUE_PLUS },
  { "min_value_minus", IN_CMD_MIN_VALUE_MINUS },
  { "max_value_set", IN_CMD_MAX_VALUE_SET },
  { "max_value_plus", IN_CMD_MAX_VALUE_PLUS },
  { "max_value_minus", IN_CMD_MAX_VALUE_MINUS },

  { "walk_mode", IN_CMD_WALK_MODE },
  { "walk_save", IN_CMD_WALK_SAVE },
  { "walk_load", IN_CMD_WALK_LOAD },
  { "walk_x_offset", IN_CMD_WALK_X_OFFSET },
  { "walk_y_offset", IN_CMD_WALK_Y_OFFSET },
  { "walk_z_offset", IN_CMD_WALK_Z_OFFSET },
  { "walk_roll_offset", IN_CMD_WALK_ROLL_OFFSET },
  { "walk_pitch_offset", IN_CMD_WALK_PITCH_OFFSET },
  { "walk_yaw_offset", IN_CMD_WALK_YAW_OFFSET },
  { "walk_hip_offset", IN_CMD_WALK_HIP_OFFSET },
  { "walk_auto_balance", IN_CMD_WALK_AUTO_BALANCE },
  { "walk_period_time", IN_CMD_WALK_PERIOD_TIME },
  { "walk_dsp_ratio", IN_CMD_WALK_DSP_RATIO },
  { "walk_step_fb_ratio", IN_CMD_WALK_STEP_FB_RATIO },
  { "walk_step_fb", IN_CMD_WALK_STEP_FB },
  { "walk_step_rl", IN_CMD_WALK_STEP_RL },
  { "walk_step_dir", IN_CMD_WALK_STEP_DIR },
  { "walk_turn_aim", IN_CMD_WALK_TURN_AIM },
  { "walk_foot_height", IN_CMD_WALK_FOOT_HEIGHT },
  { "walk_swing_rl", IN_CMD_WALK_SWING_RL },
  { "walk_swing_td", IN_CMD_WALK_SWING_TD },
  { "walk_pelvis_offset", IN_CMD_WALK_PELVIS_OFFSET },
  { "walk_arm_swing_gain", IN_CMD_WALK_ARM_SWING_GAIN },
  { "walk_b_knee_gain", IN_CMD_WALK_B_KNEE_GAIN },
  { "walk_b_ankle_pitch_gain", IN_CMD_WALK_B_ANKLE_PITCH_GAIN },
  { "walk_b_hip_roll_gain", IN_CMD_WALK_B_HIP_ROLL_GAIN },
  { "walk_b_ankle_roll_gain", IN_CMD_WALK_B_ANKLE_ROLL_GAIN },
  { "walk_p_gain", IN_CMD_WALK_P_GAIN },
  { "walk_i_gain", IN_CMD_WALK_I_GAIN },
  { "walk_d_gain", IN_CMD_WALK_D_GAIN },
  { "walk_backward_hip_comp_ratio", IN_CMD_WALK_BACKWARD_HIP_COMP_RATIO },
  { "walk_forward_hip_comp_ratio", IN_CMD_WALK_FORWARD_HIP_COMP_RATIO },
  { "walk_foot_comp_ratio", IN_CMD_WALK_FOOT_COMP_RATIO },
  { "walk_dsp_comp_ratio", IN_CMD_WALK_DSP_COMP_RATIO },
  { "walk_move_accel_ratio", IN_CMD_WALK_MOVE_ACCEL_RATIO },
  { "walk_foot_accel_ratio", IN_CMD_WALK_FOOT_ACCEL_RATIO },

  { "walk_init_r_hip_yaw", IN_CMD_INIT_R_HIP_YAW },
  { "walk_init_r_hip_roll", IN_CMD_INIT_R_HIP_ROLL },
  { "walk_init_r_hip_pitch", IN_CMD_INIT_R_HIP_PITCH },
  { "walk_init_r_knee", IN_CMD_INIT_R_KNEE },
  { "walk_init_r_ankle_pitch", IN_CMD_INIT_R_ANKLE_PITCH },
  { "walk_init_r_ankle_roll", IN_CMD_INIT_R_ANKLE_ROLL },
  { "walk_init_r_arm_swing", IN_CMD_INIT_R_ARM_SWING },
  { "walk_init_r_shoulder_pitch", IN_CMD_INIT_R_SHOULDER_PITCH },
  { "walk_init_r_shoulder_roll", IN_CMD_INIT_R_SHOULDER_ROLL },
  { "walk_init_r_elbow", IN_CMD_INIT_R_ELBOW },
  { "walk_init_r_gripper", IN_CMD_INIT_R_GRIPPER },

  { "walk_init_l_hip_yaw", IN_CMD_INIT_L_HIP_YAW },
  { "walk_init_l_hip_roll", IN_CMD_INIT_L_HIP_ROLL },
  { "walk_init_l_hip_pitch", IN_CMD_INIT_L_HIP_PITCH },
  { "walk_init_l_knee", IN_CMD_INIT_L_KNEE },
  { "walk_init_l_ankle_pitch", IN_CMD_INIT_L_ANKLE_PITCH },
  { "walk_init_l_ankle_roll", IN_CMD_INIT_L_ANKLE_ROLL },
  { "walk_init_l_arm_swing", IN_CMD_INIT_L_ARM_SWING },
  { "walk_init_l_shoulder_pitch", IN_CMD_INIT_L_SHOULDER_PITCH },
  { "walk_init_l_shoulder_roll", IN_CMD_INIT_L_SHOULDER_ROLL },
  { "walk_init_l_elbow", IN_CMD_INIT_L_ELBOW },
  { "walk_init_l_gripper", IN_CMD_INIT_L_GRIPPER },

  { "walk_arm_position", IN_CMD_ARM_POSITION },

  { "cmd_start", IN_CMD_START },
  { "valve", IN_CMD_VALVE },
  { "obstacle", IN_CMD_OBSTACLE },
  { "naik_terrain", IN_CMD_UP_TERRAIN },
  { "turun_terrain", IN_CMD_DOWN_TERRAIN },
  { "senang", IN_CMD_SENANG },
  { "dangak", IN_CMD_DANGAK },
  { "walkready", IN_CMD_WALKREADY },
  { "start_robot", IN_CMD_START_ROBOT },
  { "stop_robot", IN_CMD_STOP_ROBOT },
  { "salam", IN_CMD_SALAM },

  { "hand_x", IN_CMD_HANDX },
  { "hand_y", IN_CMD_HANDY },
  { "hand_z", IN_CMD_HANDZ },

  { "hand2_x", IN_CMD_HAND2X },
  { "hand2_y", IN_CMD_HAND2Y },
  { "hand2_z", IN_CMD_HAND2Z },

  { "pan", IN_CMD_PAN },
  { "tilt", IN_CMD_TILT },
  { "reset_pan_tilt", IN_CMD_RESET_PAN_TILT},
  { "cmd_capture", IN_CMD_CAPTURE },
  { "cmd_record", IN_CMD_RECORD },

  { "image_label", IN_CMD_IMAGE_LABEL },
  { "input_mode", IN_CMD_INPUT_MODE },
  { "video_mode", IN_CMD_VIDEO_MODE },
  { "day_mode", IN_CMD_DAY_MODE },
  { "image_change", IN_CMD_IMAGE_CHANGE },
  { "keyboard_key", IN_CMD_KEYBOARD_KEY },

  { "kompas_reset", IN_CMD_KOMPAS_RESET },
  { "mode_reset", IN_CMD_MODE_RESET },
};


/* mapping between command string and command type */
static const struct {
  const char *string;
  const out_cmd_type cmd;
} out_cmd_mapping[] = {
  { "hello_output", OUT_CMD_HELLO }
};

/* the webserver determines between these values for an answer */
typedef enum { A_UNKNOWN, A_SNAPSHOT, A_STREAM, A_COMMAND, A_FILE } answer_t;

/*
 * the client sends information with each request
 * this structure is used to store the important parts
 */
typedef struct {
  answer_t type;
  char *parameter;
  char *client;
  char *credentials;
} request;

/* the iobuffer structure is used to read from the HTTP-client */
typedef struct {
  int level;              /* how full is the buffer */
  char buffer[IO_BUFFER]; /* the data */
} iobuffer;

/* store configuration for each server instance */
typedef struct {
  int port;
  char *credentials;
  char *www_folder;
  char nocommands;
} config;

/* context of each server thread */
typedef struct {
  int sd;
  globals *pglobal;
  pthread_t threadID;

  config conf;
} context;

/*
 * this struct is just defined to allow passing all necessary details to a worker thread
 * "cfd" is for connected/accepted filedescriptor
 */
typedef struct {
  context *pc;
  int fd;
} cfd;

using namespace Robot;

class httpd
{
private:
	  static httpd *m_UniqueInstance;
    static int keyboard_key;

	  httpd()
    {
        //m_value = 0;
        keyboard_key = -1;
        nilai = 0;
        init = false;
        show = false;
        app_start = false;
        capt = false;
        record = false;
        input_mode = false;
        day_mode = 1;
        ind_image = 0;
        _reset = false;
        _reset_mode = 0;
        game_mode = 0;
        robot_mode = 0;
        selection_tool_mode = 0;
        selection_mode = 0;
        player_role = -1;
        image_label = true;
        input_type = true;
    }

    static globals* pglobal;
    static context* server;

    static void init_iobuffer(iobuffer *iobuf);
    static void init_request(request *req);
    static void free_request(request *req);
    static int _read(int fd, iobuffer *iobuf, void *buffer, size_t len, int timeout);
    static int _readline(int fd, iobuffer *iobuf, void *buffer, size_t len, int timeout);
    static void decodeBase64(char *data);
    static void send_snapshot(int fd);
    static void send_stream(int fd);
    static void send_file(int fd, char *parameter);
    static void command(int fd, char *parameter);
    static void input_cmd(in_cmd_type cmd, float value, char* res_str);
    static void server_cleanup(void *arg);
    static void *client_thread( void *arg );

public:

  enum
  {
    KEYCODE_NONE            = 0,
    KEYCODE_BREAK           = 3,
    KEYCODE_BACKSPACE       = 8,
    KEYCODE_TAB             = 9,
    KEYCODE_CLEAR           = 12,
    KEYCODE_ENTER           = 13,
    KEYCODE_SHIFT           = 16,
    KEYCODE_CTRL            = 17,
    KEYCODE_ALT             = 18,
    KEYCODE_PAUSE           = 19,
    KEYCODE_CAPSLOCK        = 20,
    KEYCODE_ESCAPE          = 27,
    KEYCODE_SPACEBAR        = 32,
    KEYCODE_PAGE_UP         = 33,
    KEYCODE_PAGE_DOWN       = 34,
    KEYCODE_END             = 35,
    KEYCODE_HOME            = 36,
    KEYCODE_LEFT_ARROW      = 37,
    KEYCODE_UP_ARROW        = 38,
    KEYCODE_RIGHT_ARROW     = 39,
    KEYCODE_DOWN_ARROW      = 40,
    KEYCODE_SELECT          = 41,
    KEYCODE_PRINT           = 42,
    KEYCODE_EXECUTE         = 43,
    KEYCODE_PRINT_SCREEN    = 44,
    KEYCODE_INSERT          = 45,
    KEYCODE_DELETE          = 46,
    KEYCODE_HELP            = 47,
    KEYCODE_0               = 48,
    KEYCODE_1               = 49,
    KEYCODE_2               = 50,
    KEYCODE_3               = 51,
    KEYCODE_4               = 52,
    KEYCODE_5               = 53,
    KEYCODE_6               = 54,
    KEYCODE_7               = 55,
    KEYCODE_8               = 56,
    KEYCODE_9               = 57,
    KEYCODE_SEMICOLON       = 59,
    KEYCODE_EQUAL           = 61,
    KEYCODE_A               = 65,
    KEYCODE_B               = 66,
    KEYCODE_C               = 67,
    KEYCODE_D               = 68,
    KEYCODE_E               = 69,
    KEYCODE_F               = 70,
    KEYCODE_G               = 71,
    KEYCODE_H               = 72,
    KEYCODE_I               = 73,
    KEYCODE_J               = 74,
    KEYCODE_K               = 75,
    KEYCODE_L               = 76,
    KEYCODE_M               = 77,
    KEYCODE_N               = 78,
    KEYCODE_O               = 79,
    KEYCODE_P               = 80,
    KEYCODE_Q               = 81,
    KEYCODE_R               = 82,
    KEYCODE_S               = 83,
    KEYCODE_T               = 84,
    KEYCODE_U               = 85,
    KEYCODE_V               = 86,
    KEYCODE_W               = 87,
    KEYCODE_X               = 88,
    KEYCODE_Y               = 89,
    KEYCODE_Z               = 90,
    KEYCODE_MINUS           = 173,
    KEYCODE_COMMA           = 188,
    KEYCODE_PERIOD          = 190,
    KEYCODE_SLASH           = 191,
    KEYCODE_BRACKET_LEFT    = 219,
    KEYCODE_BRACKET_RIGHT   = 221,
    KEYCODE_BACKSLASH       = 220,
    KEYCODE_QUOTE           = 222
  };

  int nilai;
  bool init;
  bool show;
  bool app_start;
  bool capt;
  bool record;
  bool input_mode;
  int day_mode;
  int ind_image;
  bool _reset;
  int _reset_mode;
  int game_mode;
  int robot_mode;
  int selection_tool_mode;
  int selection_mode;
  int player_role;
  int recorder;
  bool image_label;
  bool input_type;

  int keyboard_clicked(){
    int return_val = httpd::keyboard_key;
    httpd::keyboard_key = -1;

    return return_val;
  }

  static int popKeyboardKey()
  {
    int key = httpd::keyboard_key;
    httpd::keyboard_key = -1;
    return key;
  }

  static void pushKeyboardKey(int key)
  {
    httpd::keyboard_key = key;
  }

  static httpd *GetInstance()
  {
    if (m_UniqueInstance == NULL)
      m_UniqueInstance = new httpd();

    return m_UniqueInstance;
  }

  static ColorClassifier *color_classifier_;

  static bool ClientRequest;

  static void *server_thread( void *arg );
  static void send_error(int fd, int which, char *message);
};


#endif /* HTTPD_H_ */