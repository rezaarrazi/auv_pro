#include "hurocup/weightlifting.h"

#include "cli.h"

Weightlifting::Weightlifting(Hurocup *hurocup) : Hurocup::Challenge(hurocup, Hurocup::Challenge::CHALLENGE_WEIGHTLIFTING)
{
  name_ = "weightlifting";
  current_mode_ = MODE_70_DISK;
  mode_count_ = 3;

  Config::Section *section = config_.addSection("Weightlifting");
  section->addParam<float>("barbell_x_position", 50.0)->setBindValue(&barbell_x_position_);
  section->addParam<float>("lift_min_tilt_", -70.0)->setBindValue(&lift_min_tilt_);
  section->addParam<float>("lift_max_tilt_", -30.0)->setBindValue(&lift_max_tilt_);

  section = config_.addSection("Vision");
  section->addParam<float>("white_top_boundary", 100.0)->setBindValue(&white_top_boundary_);
  section->addParam<float>("white_bottom_boundary", 220.0)->setBindValue(&white_bottom_boundary_);

  section = config_.addSection("DefaultSpeed");
  section->addParam<float>("default_min_fx_speed", 15.0)->setBindValue(&default_min_fx_speed_);
  section->addParam<float>("default_max_fx_speed", 25.0)->setBindValue(&default_max_fx_speed_);
  section->addParam<float>("default_max_a_speed", 10.0)->setBindValue(&default_max_a_speed_);

  section = config_.addSection("MiddleSpeed");
  section->addParam<float>("middle_min_fx_speed", 6.0)->setBindValue(&middle_min_fx_speed_);
  section->addParam<float>("middle_max_fx_speed", 10.0)->setBindValue(&middle_max_fx_speed_);
  section->addParam<float>("middle_max_ly_speed", 10.0)->setBindValue(&middle_max_ly_speed_);
  section->addParam<float>("middle_max_ry_speed", 10.0)->setBindValue(&middle_max_ry_speed_);
  section->addParam<float>("middle_max_a_speed", 5.0)->setBindValue(&middle_max_a_speed_);

  section = config_.addSection("TopSpeed");
  section->addParam<float>("top_min_fx_speed", 4.0)->setBindValue(&top_min_fx_speed_);
  section->addParam<float>("top_max_fx_speed", 8.0)->setBindValue(&top_max_fx_speed_);
  section->addParam<float>("top_max_ly_speed", 8.0)->setBindValue(&top_max_ly_speed_);
  section->addParam<float>("top_max_ry_speed", 8.0)->setBindValue(&top_max_ry_speed_);
  section->addParam<float>("top_max_a_speed", 0.0)->setBindValue(&top_max_a_speed_);

  config_.sync("hurocup/weightlifting.yaml");

  white_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_WHITE);

  white_bottom_ = 0.0;
  white_center_left_ = 0.0;
  white_center_right_ = 0.0;
  white_inside_boundary_ = false;

  state_lift_walk_stop_ = false;
  open_gripper_ = true;
}

void Weightlifting::start()
{
  Hurocup::Challenge::start();

  config_.load("hurocup/weightlifting.yaml");

  hurocup_->getWalking()->setConfigName("walking/barbell_none.yaml");
  hurocup_->getWalking()->loadConfig();
  hurocup_->getWalking()->jointEnable();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, lift_max_tilt_);

  nextState(STATE_POSITION_BARBELL);
}

void Weightlifting::stop()
{
  Hurocup::Challenge::stop();

  config_.load("hurocup/weightlifting.yaml");

  if (open_gripper_)
  {
    hurocup_->getWalking()->setConfigName("walking/barbell_none.yaml");
    hurocup_->getWalking()->loadConfig();

    hurocup_->getHead()->jointEnable();
    hurocup_->getHead()->moveByAngle(0.0, lift_max_tilt_);
  }
  else
  {
    open_gripper_ = true;
    switch (current_state_)
    {
    case STATE_LIFT_BARBELL_MIDDLE:
    case STATE_WALK_BARBELL_MIDDLE:
      hurocup_->getWalking()->setConfigName("walking/barbell_middle.yaml");
      break;

    case STATE_LIFT_BARBELL_TOP:
    case STATE_WALK_BARBELL_TOP:
      hurocup_->getWalking()->setConfigName("walking/barbell_top.yaml");
      break;
    }

    hurocup_->getWalking()->loadConfig();
    hurocup_->getWalking()->INIT_L_GRIPPER -= 90.0;
    hurocup_->getWalking()->INIT_L_GRIPPER += 90.0;

    hurocup_->getHead()->jointEnable();
    hurocup_->getHead()->moveByAngle(0.0, lift_min_tilt_);
  }
}

void Weightlifting::visionProcess()
{
  Hurocup::Challenge::visionProcess();

  Image *stream_image = hurocup_->getStreamImage();

  Robot::Camera *camera = Robot::Camera::getInstance();
	cv::Mat hsv = camera->getBuffer()->m_HSVInput.clone();
	cv::Size mat_size = hsv.size();

  // classify white image;
  cv::Mat white_bin = white_classifier_->classify(hsv);

  // find white contours
  Contours white_contours(white_bin);
  white_contours.filterLargerThen(300.0);
  white_bottom_ = white_contours.maxY();
  white_center_left_ = alg::minValue(white_contours.minX() - camera->getCenterX(), 0.0);
  white_center_right_ = alg::maxValue(white_contours.maxX() - camera->getCenterX(), 0.0);

  // bounding white and draw
  float width = camera->getWidth() - 4;
  float height = white_bottom_boundary_ - white_top_boundary_;
  Rects white_bounding(cv::Rect(2, white_top_boundary_, width, height));
  white_inside_boundary_ = (cv::countNonZero(white_bin) > 0);
  stream_image->filterMat(white_bounding.getBinaryMatLine(mat_size, 2), 128, 128, 0);

  // filter white bin and draw
  cv::bitwise_and(white_bounding.getBinaryMat(white_bin.size()), white_bin, white_bin);
  stream_image->filterMat(white_bin, 255, 255, 0);

  // draw white contours
  stream_image->filterMat(white_contours.getBinaryMatLine(mat_size, 2), 255, 255, 0);
}

void Weightlifting::process()
{
  Hurocup::Challenge::process();

  Robot::Walking *walking = hurocup_->getWalking();
  Robot::Head *head = hurocup_->getHead();
  Robot::Action *action = hurocup_->getAction();
  Robot::Camera *camera = hurocup_->getCamera();

  MPU *mpu = hurocup_->getMPU();
  float delta_sec = hurocup_->getDeltaSec();

  Locomotion *locomotion = hurocup_->getLocomotion();

  switch (current_state_)
  {
  case STATE_POSITION_BARBELL:
    {
      cli::printBlock("position barbell");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        walking->setConfigName("walking/barbell_none.yaml");
        walking->loadConfig();

        walking->POSITION_X = 0.0;
        walking->POSITION_Y = 0.0;
      }

      float delta_direction = alg::deltaAngle(0.0, mpu->getAngle());
      double a_speed = alg::mapValue(delta_direction, -10.0, 10.0, default_max_a_speed_, -default_max_a_speed_);

      double x_speed = 0.0;
      if (walking->POSITION_X < barbell_x_position_)
        x_speed = alg::mapValue(fabs(a_speed), 0.0, fabs(default_max_a_speed_), default_max_fx_speed_, default_min_fx_speed_);

      head->moveByAngle(0.0, lift_max_tilt_);

      walking->X_MOVE_AMPLITUDE = x_speed;
      walking->Y_MOVE_AMPLITUDE = 0.0;
      walking->A_MOVE_AMPLITUDE = a_speed;
      walking->A_MOVE_AIM_ON = false;
      walking->Start();

      if (fabs(delta_direction) < 5.0 && walking->POSITION_X >= barbell_x_position_)
      {
        nextState(STATE_LIFT_BARBELL_MIDDLE);
      }

      break;
    }

  case STATE_LIFT_BARBELL_MIDDLE:
    {
      cli::printBlock("grab barbell");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        state_lift_walk_stop_ = false;
      }

      if (!state_lift_walk_stop_)
      {
        cli::print("walk in position until stop");
        if (locomotion->walkInPositionUntilStop())
        {
          usleep(0.5 * 1000000);
          state_lift_walk_stop_ = true;

          cli::print("stopped, lift barbell middle");
          action->jointEnable();
          action->Start((char *)"[WEIGHTLIFTING_MIDDLE]");
        }
      }
      else
      {
        cli::print("motion playing");
        if (action->IsRunning() == false)
        {
          cli::print("done, walk");
          nextState(STATE_WALK_BARBELL_MIDDLE);

          walking->jointEnable();
        }
      }

      break;
    }

  case STATE_WALK_BARBELL_MIDDLE:
    {
      cli::printBlock("walk barbell middle");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        walking->setConfigName("walking/barbell_middle.yaml");
        walking->loadConfig();
      }

      double y_speed = 0.0;
      if (white_inside_boundary_)
      {
        float white_center = (white_center_left_ + white_center_right_) * 0.5;
        if (white_center > 0.0)
          alg::mapValue(white_center, 0.0, 20.0, 0.0, middle_max_ry_speed_);
        else
          alg::mapValue(white_center, -20.0, 0.0, middle_max_ly_speed_, 0.0);
      }

      float delta_direction = alg::deltaAngle(0.0, mpu->getAngle());
      double a_speed = alg::mapValue(delta_direction, -10.0, 10.0, middle_max_a_speed_, -middle_max_a_speed_);

      double x_speed = 0.0;
      if (walking->POSITION_X < barbell_x_position_)
        x_speed = alg::mapValue(fabs(a_speed), 0.0, fabs(middle_max_a_speed_), middle_max_fx_speed_, middle_min_fx_speed_);

      head->moveByAngle(0.0, lift_min_tilt_);

      walking->X_MOVE_AMPLITUDE = x_speed;
      walking->Y_MOVE_AMPLITUDE = y_speed;
      walking->A_MOVE_AMPLITUDE = a_speed;
      walking->A_MOVE_AIM_ON = false;
      walking->Start();

      if (white_bottom_ > white_bottom_boundary_)
      {
        nextState(STATE_LIFT_BARBELL_TOP);
      }

      break;
    }

  case STATE_LIFT_BARBELL_TOP:
    {
      cli::printBlock("lift barbell top");

      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        state_lift_walk_stop_ = false;
      }

      if (!state_lift_walk_stop_)
      {
        cli::print("walk in position until stop");
        if (locomotion->walkInPositionUntilStop())
        {
          usleep(0.5 * 1000000);
          state_lift_walk_stop_ = true;

          cli::print("stopped, lift barbell top");
          action->jointEnable();
          action->Start((char *)"[WEIGHTLIFTING_TOP]");
        }
      }
      else
      {
        cli::print("motion playing");
        if (action->IsRunning() == false)
        {
          cli::print("done, walk");
          nextState(STATE_WALK_BARBELL_TOP);
        }
      }

      break;
    }

  case STATE_WALK_BARBELL_TOP:
    {
      cli::printBlock("walk barbel top");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
        walking->setConfigName("walking/barbell_top.yaml");
        walking->loadConfig();
      }

      double y_speed = 0.0;
      if (white_inside_boundary_)
      {
        float white_center = (white_center_left_ + white_center_right_) * 0.5;
        if (white_center > 0.0)
          alg::mapValue(white_center, 0.0, 20.0, 0.0, top_max_ry_speed_);
        else
          alg::mapValue(white_center, -20.0, 0.0, top_max_ly_speed_, 0.0);
      }

      float delta_direction = alg::deltaAngle(0.0, mpu->getAngle());
      double a_speed = alg::mapValue(delta_direction, -10.0, 10.0, top_max_a_speed_, -top_max_a_speed_);

      double x_speed = alg::mapValue(fabs(a_speed), 0.0, fabs(top_max_a_speed_), top_max_fx_speed_, top_min_fx_speed_);

      head->moveByAngle(0.0, lift_min_tilt_);

      walking->X_MOVE_AMPLITUDE = x_speed;
      walking->Y_MOVE_AMPLITUDE = y_speed;
      walking->A_MOVE_AMPLITUDE = a_speed;
      walking->A_MOVE_AIM_ON = false;
      walking->Start();

      break;
    }
  }
}