#include "hurocup/penalty_kick.h"

#include "cli.h"

PenaltyKick::PenaltyKick(Hurocup *hurocup) : Hurocup::Challenge(hurocup, Hurocup::Challenge::CHALLENGE_PENALTY_KICK)
{
  name_ = "penalty_kick";
  current_mode_ = MODE_SHOOT_LEFT;
  mode_count_ = 2;
  fallen_getup_ = true;

  Config::Section *section = config_.addSection("Soccer");
  section->addParam<float>("follow_tilt", -30.0)->setBindValue(&follow_tilt_);
  section->addParam<float>("initial_position_x", -300.0)->setBindValue(&initial_position_x_);
  section->addParam<float>("initial_position_y", 0.0)->setBindValue(&initial_position_y_);
  section->addParam<float>("goal_position_x", 0.0)->setBindValue(&goal_position_x_);
  section->addParam<float>("goal_position_y", 0.0)->setBindValue(&goal_position_y_);
  section->addParam<bool>("left_kick", true)->setBindValue(&left_kick_);
  section->addParam<bool>("right_kick", true)->setBindValue(&right_kick_);

  section = config_.addSection("ShootLeft");
  section->addParam<float>("direction", 45.0)->setBindValue(&shoot_left_direction_);
  section->addParam<float>("target_x", -200.0)->setBindValue(&shoot_left_target_x_);
  section->addParam<float>("target_y", -100.0)->setBindValue(&shoot_left_target_y_);

  section = config_.addSection("ShootRight");
  section->addParam<float>("direction", -45.0)->setBindValue(&shoot_right_direction_);
  section->addParam<float>("target_x", -200.0)->setBindValue(&shoot_right_target_x_);
  section->addParam<float>("target_y", 100.0)->setBindValue(&shoot_right_target_y_);

  section = config_.addSection("ShootRight");
  section->addParam<int>("left_kick_id", 1)->setBindValue(&motion_left_kick_id_);
  section->addParam<int>("right_kick_id", 1)->setBindValue(&motion_right_kick_id_);

  config_.sync("hurocup/penalty_kick.yaml");

  field_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_FIELD);
  ball_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_BALL);

  detect_ball_ = false;
  detect_ball_sec_ = 0.0;
  lost_ball_sec_ = 0.0;

  ball_pos_x_ = -1.0;
  ball_pos_y_ = -1.0;

  ball_distance_x_ = 0.0;
  ball_distance_y_ = 0.0;

  state_position_pivot_finish_ = false;
  state_position_last_ball_x_ = 0.0;
  state_position_last_ball_y_ = 0.0;
  state_position_kick_direction_ = 0.0;

  state_kick_walk_stop_ = false;
}

void PenaltyKick::start()
{
  Hurocup::Challenge::start();

  config_.load("hurocup/penalty_kick.yaml");

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  hurocup_->getWalking()->loadConfig();
  hurocup_->getWalking()->jointEnable();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, -30.0);

  hurocup_->getWalking()->POSITION_X = 0.0;
  hurocup_->getWalking()->POSITION_Y = 0.0;
  kick_count_ = 0;

  nextState(STATE_FOLLOW_BALL);
}

void PenaltyKick::stop()
{
  Hurocup::Challenge::stop();

  config_.load("hurocup/penalty_kick.yaml");

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  hurocup_->getWalking()->loadConfig();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, -30.0);
}

void PenaltyKick::visionProcess()
{
  Hurocup::Challenge::visionProcess();

  Image *stream_image = hurocup_->getStreamImage();

  Robot::Camera *camera = Robot::Camera::getInstance();
	cv::Mat hsv = camera->getBuffer()->m_HSVInput.clone();
	cv::Size mat_size = hsv.size();

  Robot::Head *head = Robot::Head::getInstance();

  // classify field color and draw
  cv::Mat field_binary_mat = field_classifier_->classify(hsv);
	stream_image->filterMat(field_binary_mat, 0, 128, 255);

  // classify ball color and draw
  cv::Mat ball_binary_mat = ball_classifier_->classify(hsv);
	stream_image->filterMat(ball_binary_mat, 255, 128, 0);

    // get field contours
	Contours field_contours(field_binary_mat);
  field_contours.filterLargerThen(200.0);
  field_contours.joinAll();
  field_contours.convexHull();

  // draw field contours
  cv::Mat field_contours_mat = field_contours.getBinaryMatLine(mat_size, 4);
  stream_image->filterMat(field_contours_mat, 96, 96, 192);

  // filter field by tilt
	Contours filtered_field_contours = field_contours;

  float bottom_fill = camera->getHeight();
  bottom_fill -= alg::mapValue(head->getTiltAngle(), -45.0, -30.0, camera->getHeight(), 0.0);
  filtered_field_contours.fillRect(0.0, bottom_fill, camera->getWidth(), camera->getHeight());

  float top_erase = alg::mapValue(head->getTiltAngle(), -15.0, 30.0, 0.0, camera->getHeight());
  filtered_field_contours.removeRect(0.0, 0.0, camera->getWidth(), top_erase);

  filtered_field_contours.strecthUp(8.0);

  // draw filtered field contours
  cv::Mat filtered_field_contours_mat = filtered_field_contours.getBinaryMatLine(mat_size, 4);
  stream_image->filterMat(filtered_field_contours_mat, 128, 128, 255);

  // filter ball by filtered field
	cv::bitwise_and(filtered_field_contours.getBinaryMat(mat_size), ball_binary_mat, ball_binary_mat);

  // get ball contours
  Contours ball_contours(ball_binary_mat);
  ball_contours.filterLargest();
  ball_contours.filterLargerThen(5.0);

  // get ball circles
  Circles ball_circles(ball_contours.getContours());

  // draw ball circles
  cv::Mat ball_circles_mat = ball_circles.getBinaryMatLine(mat_size, 4);
  stream_image->filterMat(ball_circles_mat, 255, 255, 0);

  ball_pos_x_ = ball_circles.getFirstCenter().x;
  ball_pos_y_ = ball_circles.getFirstCenter().y;
}

void PenaltyKick::process()
{
  Hurocup::Challenge::process();

  if (fallen_)
    return;

  Robot::Walking *walking = hurocup_->getWalking();
  Robot::Action *action = hurocup_->getAction();;
  Robot::Head *head = hurocup_->getHead();
  Robot::Camera *camera = hurocup_->getCamera();
  Locomotion *locomotion = hurocup_->getLocomotion();
  MPU *mpu = hurocup_->getMPU();

  float delta_sec = hurocup_->getDeltaSec();

  detect_ball_ = (ball_pos_x_ > 0 && ball_pos_y_ > 0);
  if (detect_ball_)
  {
    detect_ball_sec_ += delta_sec;
    lost_ball_sec_ = 0.0;

    // calculate ball distance
    float distance = head->calculateDistanceFromPanTilt();
    float direction = mpu->getAngle() - head->getPanAngle();

    ball_distance_x_ = distance * cos(direction * alg::deg2Rad());
    ball_distance_y_ = distance * sin(direction * alg::deg2Rad());
  }
  else
  {
    ball_pos_x_ = camera->getWidth() / 2;
    ball_pos_y_ = camera->getHeight() / 2;

    lost_ball_sec_ += delta_sec;
    if (lost_ball_sec_ > 1.0)
    {
        detect_ball_sec_ = 0.0;
    }
  }

  if (lost_ball_sec_ < 1.0)
  {
    cli::print("track ball\n");
    head->moveTracking(-camera->calculatePanFromXPos(ball_pos_x_), -camera->calculateTiltFromYPos(ball_pos_y_));
  }
  else
  {
    cli::print("scan ball\n");
    head->moveScanBallDown();
  }

  switch (current_state_)
  {
  case STATE_FOLLOW_BALL:
    {
      cli::printBlock("state_follow_ball");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
      }

      if (detect_ball_sec_ > 0.5)
      {
        cli::print("follow ball");

        if (locomotion->moveFollowHead(follow_tilt_))
        {
          cli::print("done following, position ball");
          nextState(STATE_POSITION_BALL);
        }
      }
      else
      {
        cli::print("walk in position");
        locomotion->walkInPosition();

        if (lost_ball_sec_ > 1.0)
        {
          cli::print("lost ball");
          nextState(STATE_LOST_BALL);
        }
      }

      break;
    }

  case STATE_POSITION_BALL:
    {
      cli::printBlock("state_position_ball");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        state_position_pivot_finish_ = false;

        state_position_last_ball_x_ = 0.0;
        state_position_last_ball_y_ = 0.0;

        if (kick_count_ < 1)
        {
          switch (current_mode_)
          {
          case MODE_SHOOT_LEFT:
            cli::print("shoot left");
            state_position_kick_direction_ = shoot_left_direction_;
            break;

          case MODE_SHOOT_RIGHT:
            cli::print("shoot right");
            state_position_kick_direction_ = shoot_right_direction_;
            break;
          }
        }
        else
        {
          cli::print("kick to goal");
          float delta_x = goal_position_x_ - locomotion->getPositionX();
          float delta_y = goal_position_y_ - locomotion->getPositionY();

          float goal_orientation = alg::direction(delta_x, delta_y) * alg::rad2Deg();
          state_position_kick_direction_ = alg::clampValue(goal_orientation, -60.0, 60.0);
        }
      }

      if (state_position_last_ball_x_ != 0.0 || state_position_last_ball_y_ != 0.0)
      {
        cli::print("reset position according to ball");
        double pos_x = state_position_last_ball_x_ - ball_distance_x_;
        double pos_y = state_position_last_ball_y_ - ball_distance_y_;
        locomotion->setPosition(pos_x, pos_y);
      }
      else
      {
        if (detect_ball_ && detect_ball_sec_ > 0.5)
        {
          state_position_last_ball_x_ = locomotion->getPositionX() + ball_distance_x_;
          state_position_last_ball_y_ = locomotion->getPositionY() + ball_distance_y_;
        }
      }

      if (detect_ball_sec_ > 0.5)
      {
        if (head->getTiltAngle() > follow_tilt_ + 5.0)
        {
          cli::print("ball too far, follow");
          nextState(STATE_FOLLOW_BALL);
          break;
        }

        cli::printParameter("kick_direction", state_position_kick_direction_);
        cli::endLine();

        if (!state_position_pivot_finish_)
        {
          cli::print("pivoting");
          if (locomotion->pivot(state_position_kick_direction_))
            state_position_pivot_finish_ = true;
        }
        else
        {
          if ((head->getPanAngle() - camera->calculatePanFromXPos(ball_pos_x_) > 0.0) && left_kick_)
          {
            cli::print("left foot");
            if (locomotion->positionLeftKick(state_position_kick_direction_))
            {
              cli::print("done kick positioning");
              nextState(STATE_KICK_BALL);
            }
          }
          else if (right_kick_)
          {
            cli::print("right foot");
            if (locomotion->positionRightKick(state_position_kick_direction_))
            {
              cli::print("done kick positioning");
              nextState(STATE_KICK_BALL);
            }
          }
          else
          {
            cli::printError("cannot kick, stopped");
            stop();
          }
        }
      }
      else
      {
        cli::print("move backward");
        locomotion->moveBackward(mpu->getAngle());

        if (lost_ball_sec_ > 5.0)
        {
          cli::print("lost ball");
          nextState(STATE_LOST_BALL);
        }
      }

      break;
    }

  case STATE_KICK_BALL:
    {
      cli::printBlock("state_kick_ball");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        state_kick_walk_stop_ = false;
      }

      if (!state_kick_walk_stop_)
      {
        cli::print("walk in position until stop");
        if (locomotion->walkInPositionUntilStop())
        {
          usleep(0.5 * 1000000);
          state_kick_walk_stop_ = true;

          action->jointEnable();
          if ((head->getPanAngle() - camera->calculatePanFromXPos(ball_pos_x_) > 0.0) && left_kick_)
          {
            cli::print("left_kick");
            action->Start(motion_left_kick_id_);
          }
          else if (right_kick_)
          {
            cli::print("right_kick");
            action->Start(motion_right_kick_id_);
          }
          else
          {
            cli::printError("cannot kick, stopped");
            stop();
          }
        }
      }
      else
      {
        cli::print("action playing");
        if (action->IsRunning() == false)
        {
          walking->jointEnable();

          kick_count_++;
          if (kick_count_ == 1)
          {
            printf("done kicking, move to target\n");
            nextState(STATE_MOVE_TO_TARGET);
          }
          else
          {
            printf("done kicking, follow\n");
            nextState(STATE_FOLLOW_BALL);
          }
        }
      }

      break;
    }

  case STATE_MOVE_TO_TARGET:
    {
      cli::printBlock("move_to_target");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
      }

      float target_x = goal_position_x_;
      float target_y = goal_position_y_;
      switch (current_mode_)
      {
      case MODE_SHOOT_LEFT:
        target_x = shoot_left_target_x_;
        target_y = shoot_left_target_y_;
        break;

      case MODE_SHOOT_RIGHT:
        target_x = shoot_right_target_x_;
        target_y = shoot_right_target_y_;
        break;
      }

      if (locomotion->moveToTarget(target_x, target_y))
      {
        cli::print("reach target, follow");
        nextState(STATE_FOLLOW_BALL);
      }

      break;
    }

  case STATE_LOST_BALL:
    {
      cli::printBlock("state_lost_ball");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
      }

      cli::print("walk in position until stop");
      locomotion->walkInPositionUntilStop();

      if (detect_ball_sec_ > 1.0)
      {
        cli::print("found ball, following");
        nextState(STATE_FOLLOW_BALL);
      }

      break;
    }
  }
}