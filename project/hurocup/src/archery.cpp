#include "hurocup/archery.h"

#include "cli.h"

Archery::Archery(Hurocup *hurocup) : Hurocup::Challenge(hurocup, Hurocup::Challenge::CHALLENGE_ARCHERY)
{
  name_ = "archery";
  current_mode_ = MODE_ARCHERY;
  mode_count_ = 1;
  fallen_getup_ = true;

  Config::Section *section = config_.addSection("Archery");

  section = config_.addSection("Vision");
  section->addParam<float>("boundary_x", 80.0)->setBindValue(&boundary_x_);
  section->addParam<float>("boundary_y", 0.0)->setBindValue(&boundary_y_);
  section->addParam<float>("boundary_width", 160.0)->setBindValue(&boundary_width_);
  section->addParam<float>("boundary_height", 120.0)->setBindValue(&boundary_height_);

  config_.sync("hurocup/archery.yaml");

  red_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_RED);
  yellow_classifier_  = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_YELLOW);
  white_classifier_  = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_GOAL);
}

void Archery::start()
{
  Hurocup::Challenge::start();

  config_.load("hurocup/archery.yaml");

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  hurocup_->getWalking()->loadConfig();
  hurocup_->getWalking()->jointEnable();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, 0.0);

  target_locked = false;
  target_locked_counting = 0;
  scanning = 1;

  nextState(STATE_WALK_TO_POSITION);
}

void Archery::stop()
{
  Hurocup::Challenge::stop();

  config_.load("hurocup/archery.yaml");

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  // hurocup_->getWalking()->loadConfig();

  hurocup_->getWalking()->jointEnable();
  hurocup_->getWalking()->forceStop();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, 0.0);

  hurocup_->getAction()->jointEnable();
  hurocup_->getAction()->Start(159);
}

void Archery::visionProcess()
{
  Hurocup::Challenge::visionProcess();

  Image *stream_image = hurocup_->getStreamImage();

  Robot::Camera *camera = Robot::Camera::getInstance();
  cv::Mat hsv = camera->getBuffer()->m_HSVInput.clone();
  cv::Mat gray = camera->getBuffer()->m_GrayInput.clone();

  cv::Size mat_size = hsv.size();

  // bounding area
  Rects bounding(cv::Rect(boundary_x_, boundary_y_, boundary_width_, boundary_height_));
  stream_image->filterMat(bounding.getBinaryMatLine(mat_size, 2), 0, 128, 0);

  // classify white color and draw
  cv::Mat white_binary_mat = white_classifier_->classify(hsv);

  // get white contours
  // Contours white_contours(white_binary_mat);
  white_contours.find(white_binary_mat);
  white_contours.filterLargest();
  white_contours.filterLargerThen(200.0);
  stream_image->filterMat(white_contours.getBinaryMat(mat_size), 255, 255, 0);

  // classify yellow color and draw
  cv::Mat yellow_binary_mat = yellow_classifier_->classify(hsv);
  stream_image->filterMat(yellow_binary_mat, 255, 128, 0);

  // get yellow contours
  Contours yellow_contours(yellow_binary_mat);
  yellow_contours.filterLargest();
  yellow_contours.filterLargerThen(100.0);

  // get yellow circles
  Circles yellow_circles(yellow_contours.getContours());

  // draw yellow circles
  cv::Mat yellow_circles_mat = yellow_circles.getBinaryMatLine(mat_size, 4);
  stream_image->filterMat(yellow_circles_mat, 0, 128, 0);

  // classify red color and draw
  cv::Mat red_binary_mat = red_classifier_->classify(hsv);
  cv::bitwise_and(yellow_circles.getBinaryMat(mat_size), red_binary_mat, red_binary_mat);
  stream_image->filterMat(red_binary_mat, 0, 0, 128);

  // check if red is in bounding area
  cv::bitwise_and(bounding.getBinaryMat(mat_size), red_binary_mat, red_binary_mat);
  target_locked = (cv::countNonZero(red_binary_mat) > 90);

}

void Archery::process()
{
  Hurocup::Challenge::process();

  if (fallen_)
    return;

  Robot::Head *head = hurocup_->getHead();
  Robot::Walking *walking = hurocup_->getWalking();
  Robot::Action *action = hurocup_->getAction();
  Locomotion *locomotion = hurocup_->getLocomotion();
  MPU *mpu = hurocup_->getMPU();

  switch (current_state_)
  {
  case STATE_WALK_TO_POSITION:
    {
      cli::printBlock("state_walk_to_pos");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
        scanning = 1;
        scan_time = 0.0;
        in_position = false; //todo: move to state walk to position

         //todo: change load yaml
        walking->INIT_L_SHOULDER_ROLL = 10;
        walking->INIT_R_SHOULDER_ROLL = -10;
        walking->INIT_L_SHOULDER_PITCH = -63;
        walking->INIT_R_SHOULDER_PITCH = 63;
        walking->INIT_L_ELBOW = 27;
        walking->INIT_R_ELBOW = -27;

        walking->m_Joint.SetEnableLowerBody(true, true);
      }

      if(locomotion->rotateToTarget(-90.0))
      {
        cli::print("done rotate to left");
        if(locomotion->walkInPositionUntilStop()) nextState(STATE_WAITING);
        // walking->Stop();

      }

      // if(scanning)
      // {
      //   if(scanning == 1)
      //   {
      //     if(scan_time > 4)
      //     {
      //       left_y = white_contours.minY();
      //       scanning = 2;
      //     }
      //     cli::print("scan left\n");
      //     head->moveByAngle(-90.0, -50.0);
      //   }
      //   if(scanning == 2)
      //   {
      //     if(scan_time > 8)
      //     {
      //       right_y = white_contours.minY();
      //       scanning = 0;
      //     }
      //     cli::print("scan right\n");
      //     head->moveByAngle(90.0, -50.0);
      //   }
      // }
      // else //walk to position
      // {
      //   if((right_y - left_y) > 10.0)
      //   {
      //     cli::print("move to left");
      //     if(locomotion->moveToTarget(0.0, -20.0))
      //       nextState(STATE_WAITING);
      //   }
      //   else if((left_y - right_y) > 10.0)
      //   {
      //     cli::print("move to right");
      //     if(locomotion->moveToTarget(0.0, 20.0))
      //       nextState(STATE_WAITING);
      //   }
      //   else
      //   {
      //     cli::print("stay in position");
      //     nextState(STATE_WAITING);
      //   }
      // }
      // walking->POSITION_X = 0.0;
      // walking->POSITION_Y = 0.0;

      // hurocup_->getHead()->moveByAngle(0.0, 0.0);

      // walking->Start();

      break;
    }

  case STATE_WAITING:
    {
      cli::printBlock("state_waiting");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
        target_locked = false;
        target_locked_counting = 0;

        action->jointEnable();
        action->playUntilStop(159);
        head->jointEnable();
        head->moveByAngle(-90.0, -20.0);
        action->jointEnable();
        action->Start(160);
      }

      // if(in_position)
      // {
      //   action->jointEnable();
      //   action->playUntilStop(159);
        if(target_locked)
        {
          // locomotion->rotateToTarget(-(hurocup_->getHead()->getPanAngle()));
          if(target_locked_counting > 10)nextState(STATE_SHOOT);
          target_locked_counting++;
        }

      // }
      break;
    }

  case STATE_SHOOT:
    {
      cli::printBlock("state_shoot");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
      }

      action->jointEnable();
      action->playUntilStop(161);
      // hurocup_->getAction()->jointEnable();
      // hurocup_->getAction()->Start(161); //motion tembak

      nextState(STATE_WAITING);
      break;
    }
  }
}
