#include "hurocup/hurocup.h"
#include "hurocup/marathon.h"
#include "hurocup/obstacle_run.h"
#include "hurocup/penalty_kick.h"
#include "hurocup/sprint.h"
#include "hurocup/united_soccer.h"
#include "hurocup/weightlifting.h"
#include "hurocup/mini_drc.h"
#include "hurocup/basketball.h"
#include "hurocup/archery.h"
#include "hurocup/spartan.h"

#include "binary.h"
#include "cli.h"

#include <iostream>

Hurocup::Challenge::Challenge(Hurocup *hurocup, int challenge_type)
{
  hurocup_ = hurocup;
  type_ = challenge_type;

  name_ = "default";

  active_ = false;
  active_sec_ = 0.0;

  current_mode_ = -1;
  mode_count_ = -1;

  initial_ = false;

  current_state_ = -1;
  prev_state_ = -1;

  fallen_getup_ = false;
  fallen_ = false;
}

void Hurocup::Challenge::setMode(int mode)
{
  current_mode_ = mode % mode_count_;

  if (MotionStatus::LED_PANNEL != (current_mode_ + 1))
  {
    int led_pannel = Robot::MotionStatus::LED_PANNEL = (current_mode_ + 1);
    hurocup_->cm730_->WriteByte(Robot::CM730::P_LED_PANNEL, led_pannel, nullptr);
  }
}

void Hurocup::Challenge::nextState(int next_state)
{
  prev_state_ = current_state_;
  current_state_ = next_state;

  initial_ = true;
}

void Hurocup::Challenge::start()
{
  active_ = true;

  prev_state_ = -1;
  current_state_ = -1;

  // set random seed to current time
  std::srand(std::time(0));

  hurocup_->mpu_->reset();

  // load config
  hurocup_->camera_->loadConfig();
  hurocup_->head_->loadConfig();
  hurocup_->locomotion_->loadConfig();

  // force mpu calibrated
  hurocup_->initialized_ = true;

  loadConfig();
}

void Hurocup::Challenge::stop()
{
  active_ = false;
  active_sec_ = 0.0;

  hurocup_->mpu_->reset();
  hurocup_->cm730_->WriteByte(CM730::ID_BROADCAST, MX28::P_TORQUE_ENABLE, 1, nullptr);

  hurocup_->walking_->jointEnable();
  hurocup_->walking_->forceStop();
}

void Hurocup::Challenge::process()
{
  cli::clear();
  cli::printLine();

  cli::printParameterBlock("challenge", bin::uppercased(name_));
  cli::printParameterBlock("active", (active_sec_ += hurocup_->delta_sec_));
  cli::endLine();

  cli::printParameterBlock("position_x", (float)hurocup_->walking_->POSITION_X);
  cli::printParameterBlock("position_y", (float)hurocup_->walking_->POSITION_Y);
  cli::printParameterBlock("orientation", (float)hurocup_->mpu_->getAngle());
  cli::endLine();

  cli::printParameterBlock("x_move", (float)hurocup_->walking_->X_MOVE_AMPLITUDE);
  cli::printParameterBlock("y_move", (float)hurocup_->walking_->Y_MOVE_AMPLITUDE);
  cli::printParameterBlock("a_move", (float)hurocup_->walking_->A_MOVE_AMPLITUDE);
  cli::printParameterBlock("aim", (float)hurocup_->walking_->A_MOVE_AIM_ON);
  cli::endLine();

  cli::printParameterBlock("head_pan", (float)hurocup_->head_->getPanAngle());
  cli::printParameterBlock("head_tilt", (float)hurocup_->head_->getTiltAngle());
  cli::endLine();

  cli::printLine();
  cli::endLine();

  if (fallen_getup_)
  {
    if (!fallen_)
    {
      if (MotionStatus::FALLEN != MotionStatus::STANDUP)
      {
        cli::print("fallen");
        fallen_ = true;

        hurocup_->walking_->Stop();
        hurocup_->cm730_->WriteByte(CM730::ID_BROADCAST, MX28::P_TORQUE_ENABLE, 1, 0);

        hurocup_->action_->jointEnable();
        hurocup_->action_->Brake();

        switch (MotionStatus::FALLEN)
        {
        case MotionStatus::FORWARD: hurocup_->action_->Start(MotionManager::FORWARD_UP); break;
        case MotionStatus::BACKWARD:hurocup_->action_->Start(MotionManager::BACKWARD_UP); break;
        case MotionStatus::LEFT: hurocup_->action_->Start(MotionManager::L_UP); break;
        case MotionStatus::RIGHT: hurocup_->action_->Start(MotionManager::R_UP); break;
        }
      }
    }
    else
    {
      cli::print("stand up");
      if (hurocup_->action_->IsRunning() == false)
      {
        cli::print("done");
        fallen_ = false;

        hurocup_->walking_->jointEnable();
      }
    }
  }
}

Hurocup::Hurocup()
{
  active_ = false;

  current_challenge_ = nullptr;

  linux_cm730_ = nullptr;
  cm730_ = nullptr;

  motion_manager_ = nullptr;

  mpu_ = nullptr;

  action_ = nullptr;
  head_ = nullptr;
  walking_ = nullptr;

  motion_timer_ = nullptr;

  camera_ = nullptr;

  stream_image_ = nullptr;
  streamer_ = nullptr;

  locomotion_ = nullptr;
}

void Hurocup::loadArguments(int argc, char **argv)
{
  httpd::GetInstance()->game_mode = Challenge::CHALLENGE_UNITED_SOCCER;
  for (int i = 0; i < argc; i++)
  {
    for (const auto &keyval : challenges_)
    {
      if (keyval.second->getName() == argv[i])
      {
        current_challenge_ = keyval.second;
        httpd::GetInstance()->game_mode = current_challenge_->getType();
      }
    }
  }
}

void Hurocup::start()
{
  // cm730 initialize
	linux_cm730_ = new Robot::LinuxCM730("/dev/ttyUSB0");
	cm730_ = new Robot::CM730(linux_cm730_);

  // motion manager initialize
	motion_manager_ = Robot::MotionManager::GetInstance();

  // mpu initialize
	mpu_ = MPU::getInstance();
	mpu_->setCM730(cm730_);

  cli::printBlock("initialize");

  // TODO: change so cm730 and mpu could reconnect
	linux_cm730_->SetPortName("/dev/ttyUSB0");
	if (motion_manager_->Initialize(cm730_) == false)
	{
		if (mpu_->setPortName("/dev/ttyUSB0") == false)
		{
			cli::printError("fail to connect cm730!");
			cli::printError("fail to connect mpu!");
			return;
		}
		else
		{
			mpu_->start();
		}

		linux_cm730_->SetPortName("/dev/ttyUSB1");
		if (motion_manager_->Initialize(cm730_) == false)
		{
			cli::printError("fail to connect cm730!");
			return;
		}
	}
	else
	{
		if (mpu_->setPortName("/dev/ttyUSB1") == false)
		{
			cli::printError("Fail to connect cm730!");
			return;
		}
		else
		{
			mpu_->start();
		}
	}

	cli::print("action start..");
	action_ = Robot::Action::GetInstance();
  action_->LoadFile();
	motion_manager_->AddModule((Robot::MotionModule *)action_);

	cli::print("head start..");
	head_ = Robot::Head::getInstance();
	motion_manager_->AddModule((Robot::MotionModule *)head_);

	cli::print("walking start..");
	walking_ = Robot::Walking::GetInstance();
	motion_manager_->AddModule((Robot::MotionModule *)walking_);

	// motion timer initialize
	motion_timer_ = new Robot::LinuxMotionTimer(motion_manager_);
	motion_timer_->Start();

	cli::print("motion manager start..");
	motion_manager_->SetEnable(true);

	action_->jointDisable();
	head_->jointDisable();
	walking_->jointDisable();

	cli::print("torque on..");
	cm730_->WriteByte(Robot::CM730::ID_BROADCAST, Robot::MX28::P_TORQUE_ENABLE, 1, 0);

	// reset led panel
	cm730_->WriteByte(Robot::CM730::P_LED_PANNEL, (Robot::MotionStatus::LED_PANNEL = 0x04), NULL);
	cm730_->WriteWord(Robot::CM730::P_LED_EYE_L, (Robot::MotionStatus::LED_EYE = 0x00), NULL);
	cm730_->WriteWord(Robot::CM730::P_LED_HEAD_L, (Robot::MotionStatus::LED_HEAD = 0x00), NULL);

	cli::print("camera start..");
	camera_ = Robot::Camera::getInstance();
  camera_->openPort();
	camera_->captureFrame();

	cli::print("stream start..");
	stream_image_ = new Image(camera_->getWidth(), camera_->getHeight(), Image::RGB_PIXEL_SIZE);
	streamer_ = new mjpg_streamer(camera_->getWidth(), camera_->getHeight());

	cli::print("locomotion start..");
	locomotion_ = Locomotion::getInstance();

	cli::print("marathon start..");
  addChallenge(Challenge::CHALLENGE_MARATHON, (Challenge *)new Marathon(this));

	cli::print("obstacle run start..");
  addChallenge(Challenge::CHALLENGE_OBSTACLE_RUN, (Challenge *)new ObstacleRun(this));

  cli::print("penalty kick start..");
  addChallenge(Challenge::CHALLENGE_PENALTY_KICK, (Challenge *)new ObstacleRun(this));

  cli::print("sprint start..");
  addChallenge(Challenge::CHALLENGE_SPRINT, (Challenge *)new Sprint(this));

  cli::print("united soccer start..");
  addChallenge(Challenge::CHALLENGE_UNITED_SOCCER, (Challenge *)new UnitedSoccer(this));

  cli::print("weightlifting start..");
  addChallenge(Challenge::CHALLENGE_WEIGHTLIFTING, (Challenge *)new Weightlifting(this));

  cli::print("mini drc start..");
  addChallenge(Challenge::CHALLENGE_MINI_DRC, (Challenge *)new MiniDRC(this));

  cli::print("basketball start..");
  addChallenge(Challenge::CHALLENGE_BASKETBALL, (Challenge *)new Basketball(this));

  cli::print("archery start..");
  addChallenge(Challenge::CHALLENGE_ARCHERY, (Challenge *)new Archery(this));

  cli::print("spartan start..");
  addChallenge(Challenge::CHALLENGE_SPARTAN_RACE, (Challenge *)new Spartan(this));

  httpd::color_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_BALL);

	clock_gettime(CLOCK_MONOTONIC, &time_now_);

  delta_sec_ = 0.0;
  initialize_time_ = 0.0;
  initialized_ = false;

  selection_tool_box_x_ = camera_->getCenterX();
  selection_tool_box_y_ = camera_->getCenterY();
  selection_tool_box_size_ = 64;

  cli::print("done");

  active_ = true;
}

Hurocup::Challenge *Hurocup::addChallenge(int challenge_type, Hurocup::Challenge *challenge)
{
  if (challenges_.find(challenge_type) != challenges_.end())
    challenges_.erase(challenge_type);

  challenges_.emplace(challenge_type, challenge);
  return challenge;
}

Hurocup::Challenge *Hurocup::getChallenge(int challenge_type)
{
  if (challenges_.find(challenge_type) == challenges_.end())
    addChallenge(challenge_type, new Challenge(this, challenge_type));

  return challenges_[challenge_type];
}

void Hurocup::process()
{
  // update delta time
  time_prev_ = time_now_;
  clock_gettime(CLOCK_MONOTONIC, &time_now_);

  // update delta sec
  delta_sec_ = (time_now_.tv_sec - time_prev_.tv_sec);
  delta_sec_ += (float)(time_now_.tv_nsec - time_prev_.tv_nsec) / 1000000000;

  // if no challenge exist, default to 0
  if (current_challenge_ == nullptr)
    current_challenge_ = getChallenge(0);

  if (current_challenge_->getType() != httpd::GetInstance()->game_mode)
  {
    if (current_challenge_->isActive())
      current_challenge_->stop();

    current_challenge_ = getChallenge(httpd::GetInstance()->game_mode);
  }

  button_ = MotionStatus::BUTTON;
  if (button_ == BUTTON_NONE)
  {
    int key = httpd::popKeyboardKey();
    switch (key)
    {
    case httpd::KEYCODE_ESCAPE: button_ = BUTTON_STOP; break;
    case httpd::KEYCODE_ENTER: button_ = BUTTON_START; break;
    default: httpd::pushKeyboardKey(key); break;
    }
  }

  switch (button_)
  {
  case BUTTON_STOP:
    {
      cli::clear();
      cli::print("button stop");

      if (current_challenge_->isActive() == false)
        current_challenge_->nextMode();
      else
        current_challenge_->resetMode();

      current_challenge_->stop();

      cli::printParameter("mode", current_challenge_->getMode());

      while (MotionStatus::BUTTON == BUTTON_STOP) usleep(8000);

      break;
    }

  case BUTTON_START:
    {
      cli::clear();
      cli::print("button start");

      current_challenge_->start();

      while (MotionStatus::BUTTON == BUTTON_START) usleep(8000);
      break;
    }
  }

  // vision process
  if (camera_->isPortExist())
  {
    // vision capture
    camera_->captureFrame();
    stream_image_->fromMat(camera_->getBuffer()->m_RGBInput.clone());

    // box calibration
    if (httpd::GetInstance()->selection_tool_mode)
    {
      cv::Mat img = Camera::getInstance()->getBuffer()->m_RGBInput.clone();

      int x = selection_tool_box_x_ - selection_tool_box_size_;
      int y = selection_tool_box_y_ - selection_tool_box_size_;
      cv::Rect selection_tool_box(x, y, selection_tool_box_size_, selection_tool_box_size_);

      int key = httpd::popKeyboardKey();
      switch (key)
      {
        case httpd::KEYCODE_SHIFT:
          selection_tool_slide_ = !selection_tool_slide_;
          break;

        case httpd::KEYCODE_C:
        case httpd::KEYCODE_BRACKET_LEFT:
        case httpd::KEYCODE_BRACKET_RIGHT:
        {
          cv::Mat croppedImage = img(selection_tool_box);
          cvtColor(croppedImage, croppedImage, CV_RGB2HSV);

          int min_hue = 255;
          int max_hue = 0;

          int min_saturation = 255;
          int max_saturation = 0;

          int min_value = 255;
          int max_value = 0;

          ColorClassifier *classifier = httpd::color_classifier_;
          if (key != httpd::KEYCODE_C)
          {
            min_hue = ((classifier->getHue() - classifier->getHueTolerance()) * 255) / 360;
            if (min_hue < 0)
            {
              min_hue = 0;
            }
            max_hue = ((classifier->getHue() + classifier->getHueTolerance()) * 255) / 360;
            if (max_hue > 360)
            {
              max_hue = 0;
            }
            min_saturation = (classifier->getMinSaturation() * 255) / 100;
            max_saturation = (classifier->getMaxSaturation() * 255) / 100;

            min_value = (classifier->getMinValue() * 255) / 100;
            max_value = (classifier->getMaxValue() * 255) / 100;
          }

          for (int i = 1; i < croppedImage.rows; i++)
          {
            for (int j = 1; j < croppedImage.cols; j++)
            {
              cv::Vec3b hsv = croppedImage.at<cv::Vec3b>(i, j);

              if (key != httpd::KEYCODE_BRACKET_LEFT)
              {
                min_hue = alg::minValue(min_hue, hsv.val[0]);
                max_hue = alg::maxValue(max_hue, hsv.val[0]);

                min_saturation = alg::minValue(min_saturation, hsv.val[1]);
                max_saturation = alg::maxValue(max_saturation, hsv.val[1]);

                min_value = alg::minValue(min_value, hsv.val[2]);
                max_value = alg::maxValue(max_value, hsv.val[2]);
              }
              else
              {
                if(hsv.val[0] < ((min_hue + max_hue) / 2))
                {
                  min_hue = (hsv.val[0] >= min_hue) ? (hsv.val[0] + 1) : min_hue;
                }
                else
                {
                  max_hue = (hsv.val[0] <= max_hue) ? (hsv.val[0] - 1) : max_hue;
                }

                if(hsv.val[1] < ((min_saturation + max_saturation) / 2))
                {
                  min_saturation = (hsv.val[1] >= min_saturation) ? (hsv.val[1] + 1) : min_saturation;
                }
                else
                {
                  max_saturation = (hsv.val[1] <= max_saturation) ? (hsv.val[1] - 1) : max_saturation;
                }

                if(hsv.val[2] < ((min_value + max_value) / 2))
                {
                  min_value = (hsv.val[2] >= min_value) ? (hsv.val[2] + 1) : min_value;
                }
                else
                {
                  max_value = (hsv.val[2] <= max_value) ? (hsv.val[2] - 1) : max_value;
                }
              }
            }
          }

          float hue = (((min_hue + max_hue) / 2) * 360) / 255;
          float hue_tolerance = ((max_hue * 360) / 255) - hue;

          min_saturation = (min_saturation * 100) / 255;
          max_saturation = (max_saturation * 100) / 255;

          min_value = (min_value * 100) / 255;
          max_value = (max_value * 100) / 255;

          httpd::color_classifier_->setHue(hue);
          httpd::color_classifier_->setHueTolerance(hue_tolerance + 10);
          httpd::color_classifier_->setMinSaturation(min_saturation - 10);
          httpd::color_classifier_->setMaxSaturation(max_saturation + 10);
          httpd::color_classifier_->setMinValue(min_value - 10);
          httpd::color_classifier_->setMaxValue(max_value + 10);
          break;
        }

        case httpd::KEYCODE_COMMA:
          selection_tool_box_size_ = alg::maxValue(selection_tool_box_size_ - 8, 8);
          break;

        case httpd::KEYCODE_PERIOD:
          selection_tool_box_size_ += 16;
          if (selection_tool_box_size_ > selection_tool_box_x_)
            selection_tool_box_size_ = selection_tool_box_x_;
          if (selection_tool_box_size_ > (camera_->getWidth() - selection_tool_box_x_))
            selection_tool_box_size_ = camera_->getWidth() - selection_tool_box_x_;
          if (selection_tool_box_size_ > selection_tool_box_y_)
            selection_tool_box_size_ = selection_tool_box_y_;
          if (selection_tool_box_size_ > (camera_->getHeight() - selection_tool_box_y_))
            selection_tool_box_size_ = camera_->getHeight() - selection_tool_box_y_;
          break;

        case httpd::KEYCODE_W:
          selection_tool_box_y_ -= (selection_tool_slide_) ? 16 : 4;
          if (selection_tool_box_y_ < selection_tool_box_size_)
            selection_tool_box_y_ = selection_tool_box_size_;
          break;

        case httpd::KEYCODE_S:
          selection_tool_box_y_ += (selection_tool_slide_) ? 16 : 4;
          if (selection_tool_box_y_ > (camera_->getHeight() * 2) - selection_tool_box_size_)
            selection_tool_box_y_ = camera_->getHeight() - selection_tool_box_size_;
          break;

        case httpd::KEYCODE_A:
          selection_tool_box_x_ -= (selection_tool_slide_) ? 16 : 4;
          if (selection_tool_box_x_ < selection_tool_box_size_)
            selection_tool_box_x_ = selection_tool_box_size_;
          break;

        case httpd::KEYCODE_D:
          selection_tool_box_x_ += (selection_tool_slide_) ? 16 : 4;
          if (selection_tool_box_x_ > (camera_->getWidth() * 2) - selection_tool_box_size_)
            selection_tool_box_x_ = camera_->getWidth() - selection_tool_box_size_;
          break;

        case httpd::KEYCODE_R:
          selection_tool_box_x_ = camera_->getCenterX();
          selection_tool_box_y_ = camera_->getCenterX();
          selection_tool_box_size_ = 64;
          break;

        default:
        httpd::pushKeyboardKey(key);
        break;
      }

      current_challenge_->visionProcess();

      Rects selection_tool_rects(selection_tool_box);
      cv::rectangle(img, selection_tool_box, cv::Scalar( 0, 0, 255 ), 2, 1 );
      stream_image_->filterMat(selection_tool_rects.getBinaryMatLine(img.size(), 2), 255, 0, 0);
    }
    else
    {
      current_challenge_->visionProcess();
    }
  }
  else
  {
    // send empty frame
    cv::Mat empty_frame(camera_->getBuffer()->m_RGBInput.size(), CV_8UC3);
    empty_frame = cv::Scalar(0);

    stream_image_->fromMat(empty_frame);
  }

  // stream image
  streamer_->send_image(stream_image_);

  // robot process
  if (!initialized_)
  {
    cli::clear();

    cli::printParameter("initialize", (initialize_time_ += delta_sec_));

    head_->jointEnable();
    head_->moveByAngle(0.0, -85.0);

    if (MotionStatus::LED_PANNEL != 7)
      cm730_->WriteByte(CM730::P_LED_PANNEL, (MotionStatus::LED_PANNEL = 7), nullptr);

    if (mpu_->calibrated_)
    {
      cli::print("done");
      initialized_ = true;

      head_->jointEnable();
      head_->moveByAngle(0.0, 0.0);

      current_challenge_->resetMode();
    }
  }
  else if (current_challenge_->isActive())
  {
    current_challenge_->process();
  }
}