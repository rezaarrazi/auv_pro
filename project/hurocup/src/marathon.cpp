#include "hurocup/marathon.h"

#include "cli.h"

Marathon::Marathon(Hurocup *hurocup) : Hurocup::Challenge(hurocup, Hurocup::Challenge::CHALLENGE_MARATHON)
{
  name_ = "marathon";
  current_mode_ = MODE_MARATHON_COOL;
  mode_count_ = 2;
  fallen_getup_ = true;

  Config::Section *section = config_.addSection("Marathon");
  section->addParam<float>("follow_tilt", -60.0)->setBindValue(&follow_tilt_);
  section->addParam<float>("odometry_tilt", -30.0)->setBindValue(&odometry_tilt_);

  section = config_.addSection("Vision");
  section->addParam<float>("middle_top_boundary", 200.0)->setBindValue(&middle_top_boundary_);
  section->addParam<float>("middle_bottom_boundary", 220.0)->setBindValue(&middle_bottom_boundary_);
  section->addParam<float>("middle_left_boundary", 50.0)->setBindValue(&middle_left_boundary_);
  section->addParam<float>("middle_right_boundary", 270.0)->setBindValue(&middle_right_boundary_);
  section->addParam<float>("side_x_from_side", 200.0)->setBindValue(&side_x_from_side_);
  section->addParam<float>("side_y", 220.0)->setBindValue(&side_y_);
  section->addParam<float>("side_width", 50.0)->setBindValue(&side_width_);
  section->addParam<float>("side_height", 270.0)->setBindValue(&side_height_);


  section = config_.addSection("Speed");
  section->addParam<float>("min_fx_speed", 15.0)->setBindValue(&min_fx_speed_);
  section->addParam<float>("max_fx_speed", 30.0)->setBindValue(&max_fx_speed_);
  section->addParam<float>("max_a_speed", 15.0)->setBindValue(&max_a_speed_);

  config_.sync("hurocup/marathon.yaml");

  red_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_RED);
  black_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_BLACK);
  white_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_WHITE);

  middle_inside_boundary_ = false;
  middle_center_ = 0.0;
  middle_left_ = 0.0;
  middle_right_ = 0.0;
  marker_type_ = MARKER_NONE;
  marker_counting_ = 0;

  odometry_direction_ = 0.0;

  garis_habis = false;
}

void Marathon::start()
{
  Hurocup::Challenge::start();

  config_.load("hurocup/marathon.yaml");

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  hurocup_->getWalking()->loadConfig();
  hurocup_->getWalking()->jointEnable();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, follow_tilt_);

  nextState(STATE_FOLLOW_LINE);
}

void Marathon::stop()
{
  Hurocup::Challenge::stop();

  config_.load("hurocup/marathon.yaml");

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  hurocup_->getWalking()->loadConfig();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, follow_tilt_);
}

void Marathon::visionProcess()
{
  Hurocup::Challenge::visionProcess();

  Image *stream_image = hurocup_->getStreamImage();

  Robot::Camera *camera = Robot::Camera::getInstance();
	cv::Mat hsv = camera->getBuffer()->m_HSVInput.clone();
  cv::Mat gray = camera->getBuffer()->m_GrayInput.clone();
	cv::Size mat_size = hsv.size();
  cv::Size gray_size = gray.size();

  // bounding red and draw
  float width = camera->getWidth() - 4;
  float height = middle_bottom_boundary_ - middle_top_boundary_;
  Rects middle_bounding(cv::Rect(2, middle_top_boundary_, width, height));
  stream_image->filterMat(middle_bounding.getBinaryMatLine(mat_size, 2), 0, 0, 128);

  // middle classifycli
  cv::Mat middle_bin = red_classifier_->classify(hsv);
  cv::bitwise_and(middle_bounding.getBinaryMat(middle_bin.size()), middle_bin, middle_bin);
  middle_inside_boundary_ = (cv::countNonZero(middle_bin) > 0);
  stream_image->filterMat(middle_bin, 0, 0, 255);

  cv::Mat upper_bin = red_classifier_->classify(hsv);

  // find middle contours
  Contours middle_contours(middle_bin);
  middle_contours.filterLargerThen(100.0);

  middle_left_ = middle_contours.minX();
  middle_right_ = middle_contours.maxX();
  middle_center_ = middle_contours.centerX() - camera->getCenterX();

  Contours upper_con(upper_bin);
  upper_con.filterLargest();

  // //bounding side and draw
  // Rects left(cv::Rect(side_x_from_side_, side_y_, side_width_, side_height_));
  // Rects right(cv::Rect(camera->getWidth()-side_x_from_side_, side_y_, side_width_, side_height_));
  // stream_image->filterMat(left.getBinaryMatLine(mat_size, 2), 128, 0, 0);
  // stream_image->filterMat(right.getBinaryMatLine(mat_size, 2), 128, 0, 0);

  // //classify red to detect turn god i need a better comment my brain is not working
  // cv::Mat for_turn = red_classifier_->classify(hsv);
  // cv::Mat lefty;
  // cv::Mat righty;

  // cv::bitwise_and(left.getBinaryMat(mat_size), for_turn, lefty);
  // cv::bitwise_and(right.getBinaryMat(mat_size), for_turn, righty);

  // turn_left = (cv::countNonZero(lefty) > 90);
  // turn_right = (cv::countNonZero(righty) > 90);

  //detect if garisnya mau putus kali aja bisa dibicarain dulu baik baik
  garis_habis = ((upper_con.minY() > 170.0)
                  && (upper_con.minX() > middle_left_boundary_
                  && upper_con.maxX() < middle_right_boundary_));

  // black marker classify
  Rects middle_marker(cv::Rect(camera->getWidth() / 3.0, 0, camera->getWidth() / 3.0, camera->getHeight()));
  cv::Mat black_bin = black_classifier_->classifyGray(gray);
  cv::bitwise_and(middle_marker.getBinaryMat(mat_size), black_bin, black_bin);
  Contours black_contours(black_bin);
  black_contours.filterLargerThen(100.0);
  black_contours.filterLargest();
  black_contours.convexHull();
  stream_image->filterMat(black_contours.getBinaryMat(black_bin.size()), 0, 255, 0);

  // white marker classify
  cv::Mat white_bin = white_classifier_->classify(hsv);
  cv::bitwise_and(black_contours.getBinaryMat(white_bin.size()), white_bin, white_bin);
  stream_image->filterMat(white_bin, 255, 255, 0);

  Contours white_contours(white_bin);
  white_contours.filterLargest();
  white_contours.filterLargerThen(400.0);

  marker_type_ = MARKER_NONE;
  if (white_contours.getContours().size() > 0)
  {
    float bottom_x = 0.0;
    float bottom_y = 0.0;
    for (std::vector<cv::Point> &contour : white_contours.getContours())
    {
      for (cv::Point &point : contour)
      {
        if (point.y > bottom_y)
        {
          bottom_y = point.y;
          bottom_x = point.x;
        }
      }
    }

    float min_x = white_contours.minX();
    float max_x = white_contours.maxX();
    // printf("bot: %.2f min: %.2f max: %.2f\n", bottom_x, min_x, max_x);
    float rate = (bottom_x - min_x) / (max_x - min_x);
    if (rate > 0.6)
      marker_type_ = MARKER_LEFT;
    else if (rate < 0.4)
      marker_type_ = MARKER_RIGHT;
    else
      marker_type_ = MARKER_FORWARD;

    cli::printParameter("marker type", marker_type_);
    cli::printParameter("marker counting", marker_counting_);
  }
}

void Marathon::process()
{
  Hurocup::Challenge::process();

  if (fallen_)
    return;

  Robot::Walking *walking = hurocup_->getWalking();
  Robot::Head *head = hurocup_->getHead();

  MPU *mpu = hurocup_->getMPU();
  if(marker_type_!=MARKER_NONE) nextState(STATE_ODOMETRY);
  switch (current_state_)
  {
  case STATE_FOLLOW_LINE:
    {
      cli::printBlock("follow line");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
      }

      if (marker_type_ != MARKER_NONE)
      {
        nextState(STATE_ODOMETRY);
        break;
      }

      // if (!middle_inside_boundary_)
      // {
      //   nextState(STATE_LOST);
      //   break;
      // }

      cli::print("follow_line");

      float x_speed = 0.0;
      float a_speed = 0.0;
      if (middle_left_ < middle_left_boundary_)
      {
        x_speed = 0.0;
        a_speed = max_a_speed_;
      }
      else if (middle_right_ > middle_right_boundary_)
      {
        x_speed = 0.0;
        a_speed = -max_a_speed_;
      }
      else if(garis_habis)
      {
        puts("garis habis");
        x_speed = 0;
        a_speed = 0;
      }
      else
      {
        a_speed = alg::mapValue(middle_center_, -60.0, 60.0, max_a_speed_, -max_a_speed_);
        x_speed = alg::mapValue(fabs(a_speed), 0.0, max_a_speed_, min_fx_speed_, max_fx_speed_);
      }

      walking->X_MOVE_AMPLITUDE = x_speed;
      walking->Y_MOVE_AMPLITUDE = 0.0;
      walking->A_MOVE_AMPLITUDE = a_speed;
      walking->A_MOVE_AIM_ON = false;

      walking->Start();

      head->moveByAngle(0.0, follow_tilt_);

      break;
    }

  case STATE_ODOMETRY:
    {
      cli::printBlock("odometry move");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        mpu->reset();

        walking->POSITION_X = 0.0;
        walking->POSITION_Y = 0.0;

        odometry_direction_ = 0.0;
        switch (marker_type_)
        {
        case MARKER_FORWARD: odometry_direction_ = 0.0; break;
        case MARKER_LEFT: odometry_direction_ = -90.0; break;
        case MARKER_RIGHT: odometry_direction_ = 90.0; break;
        }
      }

      if(middle_inside_boundary_)
      {
        cli::print("found line");
        nextState(STATE_FOLLOW_LINE);
      }

      cli::printParameter("move to", odometry_direction_);
      float delta_direction = alg::deltaAngle(odometry_direction_, mpu->getAngle());

      double a_speed = alg::mapValue(delta_direction, -10.0, 10.0, max_a_speed_, -max_a_speed_);
      double x_speed = alg::mapValue(fabs(a_speed), 0.0, fabs(max_a_speed_), max_fx_speed_, 0.0);

      if (alg::distance(walking->POSITION_X, walking->POSITION_Y) > 30.0)
      {
        if (fabs(delta_direction) < 10.0)
        {
          cli::print("reach position");
          nextState(STATE_FOLLOW_LINE);
        }
      }

      walking->X_MOVE_AMPLITUDE = x_speed;
      walking->Y_MOVE_AMPLITUDE = 0.0;
      walking->A_MOVE_AMPLITUDE = a_speed;
      walking->A_MOVE_AIM_ON = false;

      walking->Start();

      head->moveByAngle(0.0, odometry_tilt_);

      break;
    }

  case STATE_LOST:
    {
      cli::printBlock("lost line");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
      }

      if (middle_inside_boundary_)
      {
        nextState(STATE_FOLLOW_LINE);
        break;
      }

      cli::print("move forward");

      walking->X_MOVE_AMPLITUDE = max_fx_speed_;
      walking->Y_MOVE_AMPLITUDE = 0.0;
      walking->A_MOVE_AMPLITUDE = 0.0;
      walking->A_MOVE_AIM_ON = false;

      walking->Start();

      head->moveByAngle(0.0, follow_tilt_);

      break;
    }
  }
}