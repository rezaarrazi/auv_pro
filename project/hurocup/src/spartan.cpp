#include "hurocup/spartan.h"

#include "cli.h"

Spartan::Spartan(Hurocup *hurocup) : Hurocup::Challenge(hurocup, Hurocup::Challenge::CHALLENGE_SPARTAN_RACE)
{
  name_ = "spartan";
  current_mode_ = MODE_SPARTAN_NAIK;
  mode_count_ = 2;
  fallen_getup_ = true;
  top_terrain = COLOR_YELLOW;
  current_color = COLOR_RED;
  // min_tilt_ = -70.0;
  Config::Section *section = config_.addSection("Spartan");
  section->addParam<float>("min_tilt", -70.0)->setBindValue(&min_tilt_);

  section = config_.addSection("Vision");
  section->addParam<float>("boundary_x", 80.0)->setBindValue(&boundary_x_);
  section->addParam<float>("boundary_y", 0.0)->setBindValue(&boundary_y_);
  section->addParam<float>("boundary_width", 160.0)->setBindValue(&boundary_width_);
  section->addParam<float>("boundary_height", 240.0)->setBindValue(&boundary_height_);

  config_.sync("hurocup/spartan.yaml");

  min_tilt_ = -85.0;
  red_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_RED);
  yellow_classifier_  = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_YELLOW);
  blue_classifier_  = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_BLUE);
  green_classifier_  = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_FIELD);

}

void Spartan::start()
{
  Hurocup::Challenge::start();

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  hurocup_->getWalking()->loadConfig();
  hurocup_->getWalking()->jointEnable();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, 0.0);

  current_color = COLOR_RED;
  deket_kiri = false;
  deket_kanan = false;
  counting_deket = 0;

  terrain_x = -1.0;
  terrain_y = -1.0;
  setMode(MODE_SPARTAN_NAIK);
  nextState(STATE_FOLLOW);
}

void Spartan::stop()
{
  Hurocup::Challenge::stop();

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  hurocup_->getWalking()->loadConfig();

  hurocup_->getWalking()->jointEnable();
  hurocup_->getWalking()->forceStop();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, 0.0);
}

void Spartan::visionProcess()
{
  Hurocup::Challenge::visionProcess();

  Image *stream_image = hurocup_->getStreamImage();

  Robot::Camera *camera = Robot::Camera::getInstance();
  cv::Mat hsv = camera->getBuffer()->m_HSVInput.clone();
  cv::Size mat_size = hsv.size();

  //rect bounding_left
  // printf("x: %.2f y: %.2f w: %.2f h: %.2f\n", boundary_x_, boundary_y_, boundary_width_, boundary_height_);
  Rects bounding_left(cv::Rect(20, 210, 100, 10));
  Rects bounding_right(cv::Rect(200, 210, 100, 10));

  stream_image->filterMat(bounding_left.getBinaryMatLine(mat_size, 2), 0, 128, 0);
  stream_image->filterMat(bounding_right.getBinaryMatLine(mat_size, 2), 0, 128, 0);

  switch(current_color)
  {
    case COLOR_RED:
    {
      cv::Mat red_binary_mat = red_classifier_->classify(hsv);
      cv::Mat left;
      cv::Mat right;

      cv::bitwise_and(bounding_left.getBinaryMat(mat_size), red_binary_mat, left);
      cv::bitwise_and(bounding_right.getBinaryMat(mat_size), red_binary_mat, right);

      deket_kiri = (cv::countNonZero(left) > 90);
      deket_kanan = (cv::countNonZero(right) > 90);

      stream_image->filterMat(left, 0, 128, 0);
      stream_image->filterMat(right, 0, 128, 0);

      break;
    }
    case COLOR_YELLOW:
    {
      cv::Mat yellow_binary_mat = yellow_classifier_->classify(hsv);
      cv::Mat left;
      cv::Mat right;

      cv::bitwise_and(bounding_left.getBinaryMat(mat_size), yellow_binary_mat, left);
      cv::bitwise_and(bounding_right.getBinaryMat(mat_size), yellow_binary_mat, right);

      deket_kiri = (cv::countNonZero(left) > 90);
      deket_kanan = (cv::countNonZero(right) > 90);

      stream_image->filterMat(left, 0, 128, 0);
      stream_image->filterMat(right, 0, 128, 0);

      break;
    }
    case COLOR_GREEN:
    {
      cv::Mat green_binary_mat = green_classifier_->classify(hsv);
      cv::Mat left;
      cv::Mat right;

      cv::bitwise_and(bounding_left.getBinaryMat(mat_size), green_binary_mat, left);
      cv::bitwise_and(bounding_right.getBinaryMat(mat_size), green_binary_mat, right);

      deket_kiri = (cv::countNonZero(left) > 90);
      deket_kanan = (cv::countNonZero(right) > 90);

      stream_image->filterMat(left, 0, 128, 0);
      stream_image->filterMat(right, 0, 128, 0);
      ;

      break;
    }
    case COLOR_BLUE:
    {
      cv::Mat blue_binary_mat = blue_classifier_->classify(hsv);
      cv::Mat left;
      cv::Mat right;

      cv::bitwise_and(bounding_left.getBinaryMat(mat_size), blue_binary_mat, left);
      cv::bitwise_and(bounding_right.getBinaryMat(mat_size), blue_binary_mat, right);

      deket_kiri = (cv::countNonZero(left) > 90);
      deket_kanan = (cv::countNonZero(right) > 90);

      stream_image->filterMat(left, 0, 128, 0);
      stream_image->filterMat(right, 0, 128, 0);

      break;
    }
  }
}

void Spartan::process()
{
  Hurocup::Challenge::process();

  if (fallen_)
    return;

  Robot::Head *head = hurocup_->getHead();
  Robot::Walking *walking = hurocup_->getWalking();
  Robot::Action *action = hurocup_->getAction();
  Robot::Camera *camera = hurocup_->getCamera();
  Locomotion *locomotion = hurocup_->getLocomotion();
  MPU *mpu = hurocup_->getMPU();

  if(terrain_x > 0)
  {
   head->moveTrackingTiltOnly(-camera->calculateTiltFromYPos(terrain_y));
  }

  if(current_mode_ == MODE_SPARTAN_NAIK)
    cli::printBlock("MODE NAIK");
  else
    cli::printBlock("MODE TURUN");

  switch (current_state_)
  {
  case STATE_FOLLOW:
    {
      cli::printBlock("state_follow_terrain");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
        counting_deket = 0;
        head->moveByAngle(0.0, -80.0);
      }

      if(deket_kiri || deket_kanan)
      {
        if((deket_kanan && deket_kiri) && counting_deket > 25)
        {
          walking->Stop();
          if(current_mode_ == MODE_SPARTAN_NAIK)
              nextState(STATE_NAIK);
          else
              nextState(STATE_TURUN);
        }
        else
        {
          float a;
          if(deket_kiri) a = -3.0;
          else if(deket_kanan) a = 3.0;
          cli::print("walk in position");
          walking->jointEnable();
          walking->A_MOVE_AIM_ON = false;
          walking->A_MOVE_AMPLITUDE = a;
          walking->Y_MOVE_AMPLITUDE = 0.0;
          walking->X_MOVE_AMPLITUDE = 3.0;
          walking->Z_MOVE_AMPLITUDE = 8.0;

          walking->Start();
        }

        counting_deket++;
      }
      else
      {

        float a = alg::mapValue(locomotion->getOrientation(), -20.0, 20.0, -25, 25);

        walking->jointEnable();
        walking->A_MOVE_AIM_ON = false;
        walking->A_MOVE_AMPLITUDE = a;
        walking->Y_MOVE_AMPLITUDE = 0.0;
        walking->X_MOVE_AMPLITUDE = 10.0;
        walking->Z_MOVE_AMPLITUDE = 10.0;
        walking->Start();
      }

      break;
    }

  case STATE_NAIK:
    {
      cli::printBlock("state_naik");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
      }

      walking->Stop();
      action->jointEnable();
      action->playUntilStop(196);

      if(current_color == top_terrain) setMode(MODE_SPARTAN_TURUN);
      nextState(STATE_FOLLOW);

      if((current_color % 3)) current_color++;
      else current_color--;
      break;
    }

  case STATE_TURUN:
    {
      cli::printBlock("state_turun");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
      }

      walking->Stop();
      action->jointEnable();
      action->playUntilStop(245);

      nextState(STATE_FOLLOW);

      current_color--;
      break;
    }
  }
}
