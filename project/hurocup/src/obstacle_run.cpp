#include "hurocup/obstacle_run.h"

#include "cli.h"

ObstacleRun::ObstacleRun(Hurocup *hurocup) : Hurocup::Challenge(hurocup, Hurocup::Challenge::CHALLENGE_OBSTACLE_RUN)
{
  name_ = "obstacle_run";
  current_mode_ = MODE_NO_PRIORITY;
  mode_count_ = 3;
  fallen_getup_ = true;

  Config::Section *section = config_.addSection("ObstacleRun");
  section->addParam<float>("left_border", -120.0)->setBindValue(&left_border_);
  section->addParam<float>("right_border", 120.0)->setBindValue(&right_border_);
  section->addParam<bool>("enable_crawl", true)->setBindValue(&enable_crawl_);
  section->addParam<int>("crawl_max_count", 0)->setBindValue(&state_crawl_max_count_);

  section = config_.addSection("Tilt");
  section->addParam<float>("forward_tilt", -30.0)->setBindValue(&state_forward_tilt_);
  section->addParam<float>("side_min_tilt", -45.0)->setBindValue(&state_side_min_tilt_);
  section->addParam<float>("enter_min_tilt", -45.0)->setBindValue(&state_enter_min_tilt_);

  section = config_.addSection("Vision");
  section->addParam<float>("red_top_boundary", 20.0)->setBindValue(&red_top_boundary_);
  section->addParam<float>("red_bottom_boundary", 40.0)->setBindValue(&red_bottom_boundary_);
  section->addParam<float>("blue_top_boundary", 170.0)->setBindValue(&blue_top_boundary_);
  section->addParam<float>("blue_bottom_boundary", 190.0)->setBindValue(&blue_bottom_boundary_);
  section->addParam<float>("blue_left_boundary", 90.0)->setBindValue(&blue_left_boundary_);
  section->addParam<float>("blue_right_boundary", 230.0)->setBindValue(&blue_right_boundary_);
  section->addParam<float>("yellow_top_boundary", 200.0)->setBindValue(&yellow_top_boundary_);
  section->addParam<float>("yellow_bottom_boundary", 220.0)->setBindValue(&yellow_bottom_boundary_);
  section->addParam<float>("yellow_left_boundary", 70.0)->setBindValue(&yellow_left_boundary_);
  section->addParam<float>("yellow_right_boundary", 250.0)->setBindValue(&yellow_right_boundary_);

  section = config_.addSection("Speed");
  section->addParam<float>("min_fx_speed", 15.0)->setBindValue(&min_fx_speed_);
  section->addParam<float>("max_fx_speed", 30.0)->setBindValue(&max_fx_speed_);
  section->addParam<float>("min_bx_speed", -15.0)->setBindValue(&min_bx_speed_);
  section->addParam<float>("max_bx_speed", -30.0)->setBindValue(&max_bx_speed_);
  section->addParam<float>("min_ly_speed", 15.0)->setBindValue(&min_ly_speed_);
  section->addParam<float>("max_ly_speed", 30.0)->setBindValue(&max_ly_speed_);
  section->addParam<float>("min_ry_speed", -15.0)->setBindValue(&min_ry_speed_);
  section->addParam<float>("max_ry_speed", -30.0)->setBindValue(&max_ry_speed_);
  section->addParam<float>("max_a_speed", 15.0)->setBindValue(&max_a_speed_);

  config_.sync("hurocup/obstacle_run.yaml");

  red_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_RED);
  blue_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_BLUE);
  yellow_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_YELLOW);

  state_forward_orientataion_ = 0.0;

  state_side_move_left_ = false;

  state_crawl_walk_stop_ = false;
  state_crawl_down_ = false;
  state_almost_crawl_up_ = false;
  state_crawl_up_ = false;
  state_crawl_count_ = 0;

  red_inside_boundary_ = false;
  blue_inside_boundary_ = false;
  yellow_inside_boundary_ = false;

  red_bottom_ = 0.0;
  red_bottom_left_ = 0.0;
  red_bottom_right_ = 0.0;
  red_center_left_ = 0.0;
  red_center_right_ = 0.0;

  blue_bottom_ = 0.0;
  blue_bottom_left_ = 0.0;
  blue_bottom_right_ = 0.0;
  blue_left_ = 0.0;
  blue_right_ = 0.0;
  blue_center_left_ = 0.0;
  blue_center_right_ = 0.0;

  yellow_bottom_ = 0.0;
  yellow_bottom_left_ = 0.0;
  yellow_bottom_right_ = 0.0;
  yellow_left_ = 0.0;
  yellow_right_ = 0.0;
  yellow_center_left_ = 0.0;
  yellow_center_right_ = 0.0;
}

void ObstacleRun::start()
{
  Hurocup::Challenge::start();

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  hurocup_->getWalking()->loadConfig();
  hurocup_->getWalking()->jointEnable();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, state_forward_tilt_);

  hurocup_->getWalking()->POSITION_X = 0.0;
  hurocup_->getWalking()->POSITION_Y = 0.0;

  nextState(STATE_FORWARD_MOVE);
}

void ObstacleRun::stop()
{
  Hurocup::Challenge::stop();

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  hurocup_->getWalking()->loadConfig();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, state_forward_tilt_);
}

void ObstacleRun::visionProcess()
{
  Hurocup::Challenge::visionProcess();

  Image *stream_image = hurocup_->getStreamImage();

  Robot::Camera *camera = Robot::Camera::getInstance();
	cv::Mat hsv = camera->getBuffer()->m_HSVInput.clone();
	cv::Size mat_size = hsv.size();

  // classify color
  cv::Mat red_bin = red_classifier_->classify(hsv);
  cv::Mat blue_bin = blue_classifier_->classify(hsv);
  cv::Mat yellow_bin = yellow_classifier_->classify(hsv);

  // find red contours
  Contours red_contours(red_bin);
  red_contours.filterLargerThen(400.0);
  red_bottom_ = red_contours.maxY();

  red_center_left_ = alg::minValue(red_contours.minX() - camera->getCenterX(), 0.0);
  red_center_right_ = alg::maxValue(red_contours.maxX() - camera->getCenterX(), 0.0);

  // split left right red contours
  float red_center_x = red_contours.centerX();
  Contours red_contours_bottom_left(red_contours.splitLeft(red_center_x));
  red_bottom_left_ = red_contours_bottom_left.maxY();
  Contours red_contours_bottom_right(red_contours.splitRight(red_center_x));
  red_bottom_right_ = red_contours_bottom_right.maxY();

  // find blue contours
  Contours blue_contours(blue_bin);
  blue_contours.filterLargerThen(200.0);
  blue_bottom_ = blue_contours.maxY();
  blue_left_ = blue_contours.minX();
  blue_right_ = blue_contours.maxX();

  blue_center_left_ = alg::minValue(blue_left_ - camera->getCenterX(), 0.0);
  blue_center_right_ = alg::maxValue(blue_right_ - camera->getCenterX(), 0.0);

  // split left right blue contours
  float blue_center_x = blue_contours.centerX();
  Contours blue_contours_bottom_left(blue_contours.splitLeft(blue_center_x));
  blue_bottom_left_ = blue_contours_bottom_left.maxY();
  Contours blue_contours_bottom_right(blue_contours.splitRight(blue_center_x));
  blue_bottom_right_ = blue_contours_bottom_right.maxY();

  Contours blue_contours_left(blue_contours.splitLeft(camera->getCenterX()));
  Contours blue_contours_right(blue_contours.splitRight(camera->getCenterX()));

  // find yellow contours and draw
  Contours yellow_contours(yellow_bin);
  yellow_contours.filterLargerThen(200.0);
  yellow_bottom_ = yellow_contours.maxY();
  yellow_left_ = yellow_contours.minX();
  yellow_right_ = yellow_contours.maxX();

  yellow_center_left_ = alg::minValue(yellow_left_ - camera->getCenterX(), 0.0);
  yellow_center_right_ = alg::maxValue(yellow_right_ - camera->getCenterX(), 0.0);

  // split left right yellow contours
  float yellow_center_x = yellow_contours.centerX();
  Contours yellow_contours_bottom_left(yellow_contours.splitLeft(yellow_center_x));
  yellow_bottom_left_ = yellow_contours_bottom_left.maxY();
  Contours yellow_contours_bottom_right(yellow_contours.splitRight(yellow_center_x));
  yellow_bottom_right_ = yellow_contours_bottom_right.maxY();

  // bounding red and draw
  float width = camera->getWidth() - 4;
  float height = red_bottom_boundary_ - red_top_boundary_;
  Rects red_bounding(cv::Rect(2, red_top_boundary_, width, height));
  stream_image->filterMat(red_bounding.getBinaryMatLine(mat_size, 2), 128, 0, 0);

  // bounding blue and draw
  width = blue_right_boundary_ - blue_left_boundary_;
  height = blue_bottom_boundary_ - blue_top_boundary_;
  Rects blue_bounding(cv::Rect(blue_left_boundary_, blue_top_boundary_, width, height));
  stream_image->filterMat(blue_bounding.getBinaryMatLine(mat_size, 2), 0, 0, 128);

  // bounding yellow and draw
  width = yellow_right_boundary_ - yellow_left_boundary_;
  height = yellow_bottom_boundary_ - yellow_top_boundary_;
  Rects yellow_bounding(cv::Rect(yellow_left_boundary_, yellow_top_boundary_, width, height));
  stream_image->filterMat(yellow_bounding.getBinaryMatLine(mat_size, 2), 128, 128, 0);

  // filter red bin and draw
  cv::bitwise_and(red_bounding.getBinaryMat(red_bin.size()), red_bin, red_bin);
  red_inside_boundary_ = (cv::countNonZero(red_bin) > 0);
  stream_image->filterMat(red_bin, 255, 0, 0);

  // filter blue bin and draw
  cv::bitwise_and(blue_bounding.getBinaryMat(blue_bin.size()), blue_bin, blue_bin);
  blue_inside_boundary_ = (cv::countNonZero(blue_bin) > 0);
  stream_image->filterMat(blue_bin, 0, 0, 255);

  // filter yellow bin and draw
  cv::bitwise_and(yellow_bounding.getBinaryMat(yellow_bin.size()), yellow_bin, yellow_bin);
  yellow_inside_boundary_ = (cv::countNonZero(yellow_bin) > 0);
  stream_image->filterMat(yellow_bin, 255, 255, 0);

  // draw contours
  stream_image->filterMat(red_contours.getBinaryMatLine(mat_size, 2), 255, 0, 0);
  stream_image->filterMat(blue_contours.getBinaryMatLine(mat_size, 2), 0, 0, 255);
  stream_image->filterMat(yellow_contours.getBinaryMatLine(mat_size, 2), 255, 255, 0);
}

void ObstacleRun::process()
{
  Hurocup::Challenge::process();

  if (fallen_)
    return;

  Robot::Head *head = hurocup_->getHead();
  Robot::Walking *walking = hurocup_->getWalking();
  Robot::Action *action = hurocup_->getAction();
  // Robot::Camera *camera = hurocup_->getCamera();
  Locomotion *locomotion = hurocup_->getLocomotion();
  MPU *mpu = hurocup_->getMPU();

  switch (current_state_)
  {
    case STATE_FORWARD_MOVE:
    {
      cli::printBlock("forward move");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        state_forward_orientataion_ = mpu->getAngle();
      }

      if (red_inside_boundary_)
      {
        if (enable_crawl_)
        {
          cli::print("gate exist, enter");
          nextState(STATE_ENTER_GATE);
        }
        else
        {
          cli::print("gate exist exist, side move");
          nextState(STATE_SIDE_MOVE);
        }
        break;
      }
      else if (yellow_inside_boundary_ || blue_inside_boundary_)
      {
        cli::print("obstacle or hole exist, side move");
        nextState(STATE_SIDE_MOVE);
        break;
      }

      state_forward_orientataion_ += ((state_forward_orientataion_ > 0.0) ? -0.2 : 0.2);
      cli::printParameter("forward orientation", state_forward_orientataion_);

      float delta_direction = alg::deltaAngle(state_forward_orientataion_, mpu->getAngle());

      double a_speed = alg::mapValue(delta_direction, -10.0, 10.0, max_a_speed_, -max_a_speed_);
      double x_speed = alg::mapValue(fabs(a_speed), 0.0, fabs(max_a_speed_), max_fx_speed_, min_fx_speed_);

      double y_speed = 0.0;
      if (yellow_bottom_ > yellow_top_boundary_)
      {
        if (yellow_left_ > yellow_right_boundary_)
          y_speed = min_ly_speed_;
        else if (yellow_right_ < yellow_left_boundary_)
          y_speed = min_ry_speed_;
      }
      else if (blue_bottom_ > blue_bottom_boundary_)
      {
        if (blue_left_ > blue_right_boundary_)
          y_speed = min_ly_speed_;
        else if (blue_right_ < blue_left_boundary_)
          y_speed = min_ry_speed_;
      }

      head->moveByAngle(0.0, state_forward_tilt_);

      walking->X_MOVE_AMPLITUDE = x_speed;
      walking->Y_MOVE_AMPLITUDE = y_speed;
      walking->A_MOVE_AMPLITUDE = a_speed;
      walking->A_MOVE_AIM_ON = false;
      walking->Start();

      break;
    }

    case STATE_SIDE_MOVE:
    {
      cli::printBlock("side move");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        if (current_mode_ == MODE_PRIORITY_LEFT && walking->POSITION_X < 200.0)
        {
          cli::print("priority left");
          state_side_move_left_ = true;
        }
        else if (current_mode_ == MODE_PRIORITY_RIGHT && walking->POSITION_X < 200.0)
        {
          cli::print("priority right");
          state_side_move_left_ = false;
        }
        else
        {
          if (yellow_center_left_ + yellow_center_right_ > 20.0)
            state_side_move_left_ = true;
          else if (yellow_center_left_ + yellow_center_right_ < -20.0)
            state_side_move_left_ = false;
          else if (blue_center_left_ + blue_center_right_ > 20.0)
            state_side_move_left_ = true;
          else if (blue_center_left_ + blue_center_right_ < -20.0)
            state_side_move_left_ = false;
          else
            state_side_move_left_ = (walking->POSITION_Y > alg::lerp(left_border_, right_border_, 0.5));
        }
      }

      if (enable_crawl_ && red_inside_boundary_)
      {
        cli::print("gate exist, enter");
        nextState(STATE_ENTER_GATE);
        break;
      }

      cli::printParameter("move ", (std::string)((state_side_move_left_) ? "left" : "right"));

      if (yellow_bottom_ > yellow_top_boundary_)
      {
        if (yellow_left_ > yellow_right_boundary_)
        {
          cli::print("near right border, move left");
          state_side_move_left_ = true;
        }
        else if (yellow_right_ < yellow_left_boundary_)
        {
          cli::print("near left border, move right");
          state_side_move_left_ = false;
        }
      }

      double x_speed = 0.0;
      if (head->getTiltAngle() < state_side_min_tilt_ + 5.0)
      {
        cli::print("move forward a bit");
        x_speed = min_fx_speed_;
      }

      bool no_obstacle = true;
      no_obstacle &= (!yellow_inside_boundary_ && !blue_inside_boundary_);
      if (!enable_crawl_)
        no_obstacle &= (!red_inside_boundary_);

      if (no_obstacle)
      {
        if (head->getTiltAngle() > state_forward_tilt_ - 5.0)
        {
          cli::print("no obstacle, move forward");
          nextState(STATE_FORWARD_MOVE);
        }
        else
        {
          cli::print("tilt up");
          float tilt = head->getTiltAngle() + 2.0;
          head->moveByAngle(0.0, alg::minValue(tilt, state_forward_tilt_));
        }
      }

      bool obstacle_too_near = false;
      obstacle_too_near |= (yellow_bottom_ > yellow_bottom_boundary_);
      obstacle_too_near |= (blue_bottom_ > blue_bottom_boundary_);
      obstacle_too_near |= (red_bottom_ > red_bottom_boundary_);
      if (obstacle_too_near)
      {
        if (head->getTiltAngle() < state_side_min_tilt_ + 5.0)
        {
          cli::print("move backward a bit");
          float delta = yellow_bottom_ - yellow_bottom_boundary_;
          delta = alg::maxValue(delta, blue_bottom_ - blue_bottom_boundary_);
          if (!enable_crawl_)
            delta = alg::maxValue(delta, red_bottom_ - red_bottom_boundary_);

          x_speed = alg::mapValue(delta, 0.0, 20.0, 0.0, min_bx_speed_);
        }
        else
        {
          cli::print("tilt down");
          float tilt = head->getTiltAngle() - 2.0;
          head->moveByAngle(0.0, alg::maxValue(tilt, state_side_min_tilt_));
        }
      }

      float delta_direction = 0.0;
      if (yellow_bottom_ > yellow_top_boundary_)
      {
        delta_direction = alg::mapValue((yellow_bottom_left_ - yellow_bottom_right_), -40.0, 40.0, 20.0, -20.0);
        if (state_side_move_left_ && (mpu->getAngle() > 0.0))
          delta_direction = alg::deltaAngle(0.0, mpu->getAngle());
        else if (!state_side_move_left_ && (mpu->getAngle() < 0.0))
          delta_direction = alg::deltaAngle(0.0, mpu->getAngle());
      }
      else if (blue_bottom_ > blue_top_boundary_)
      {
        delta_direction = alg::mapValue((blue_bottom_left_ - blue_bottom_right_), -40.0, 40.0, 20.0, -20.0);
      }
      else if (!enable_crawl_ && red_bottom_ > red_bottom_boundary_)
      {
        delta_direction = alg::mapValue((red_bottom_left_ - red_bottom_right_), -40.0, 40.0, 20.0, -20.0);
      }

      cli::printParameter("delta direction", delta_direction);
      double a_speed = alg::mapValue(delta_direction, -10.0, 10.0, max_a_speed_, -max_a_speed_);

      double y_speed = 0.0;
      if (state_side_move_left_)
        y_speed = alg::mapValue(fabs(a_speed), 0.0, max_a_speed_, max_ly_speed_, min_ly_speed_);
      else
        y_speed = alg::mapValue(fabs(a_speed), 0.0, max_a_speed_, max_ry_speed_, min_ry_speed_);

      walking->X_MOVE_AMPLITUDE = x_speed;
      walking->Y_MOVE_AMPLITUDE = y_speed;
      walking->A_MOVE_AMPLITUDE = a_speed;
      walking->A_MOVE_AIM_ON = false;
      walking->Start();

      break;
    }

    case STATE_ENTER_GATE:
    {
      cli::printBlock("enter gate");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
      }

      bool in_position = true;

      double x_speed = 0.0;
      if (head->getTiltAngle() < state_side_min_tilt_ + 5.0)
      {
        cli::print("move forward a bit");
        in_position = false;
        x_speed = min_fx_speed_;
      }

      if (!red_inside_boundary_)
      {
        in_position = false;
        if (head->getTiltAngle() < state_forward_tilt_ - 0.5)
        {
          float tilt = head->getTiltAngle() + 2.0;
          head->moveByAngle(0.0, alg::minValue(tilt, state_forward_tilt_));
        }
        else
        {
          cli::print("no gate exist, move forward");
          nextState(STATE_FORWARD_MOVE);
        }
      }

      if (red_bottom_ > red_bottom_boundary_)
      {
        in_position = false;
        if (head->getTiltAngle() < state_enter_min_tilt_ + 0.5)
        {
          cli::print("move backward a bit");
          x_speed = alg::mapValue(red_bottom_ - red_bottom_boundary_, 0.0, 20.0, 0.0, min_bx_speed_);
        }
        else
        {
          cli::print("tilt down");
          float tilt = head->getTiltAngle() - 2.0;
          head->moveByAngle(0.0, alg::maxValue(tilt, state_enter_min_tilt_));
        }
      }

      float delta_direction = 0.0;
      if (red_bottom_ < red_top_boundary_)
        delta_direction = alg::deltaAngle(0.0, mpu->getAngle());
      else
        delta_direction = alg::mapValue((red_bottom_left_ - red_bottom_right_), -20.0, 20.0, 20.0, -20.0);

      cli::printParameter("delta direction", delta_direction);
      double a_speed = alg::mapValue(delta_direction, -10.0, 10.0, max_a_speed_, -max_a_speed_);

      bool move_left = false;
      bool move_right = false;
      if (red_center_left_ + red_center_right_ > 20.0)
      {
        cli::print("gate in right");
        in_position = false;
        move_right = true;
      }
      else if (red_center_left_ + red_center_right_ < -20.0)
      {
        cli::print("gate in left");
        in_position = false;
        move_left = true;
      }
      else if (blue_inside_boundary_ || ((blue_left_ < blue_right_boundary_) && (blue_right_ > blue_left_boundary_)))
      {
        cli::print("blue inside");
        in_position = false;

        if (blue_center_left_ + blue_center_right_ > 0.0)
          move_left = true;
        else
          move_right = true;
      }

      double y_speed = 0.0;
      if (move_left)
        y_speed = alg::mapValue(fabs(a_speed), 0.0, max_a_speed_, max_ly_speed_, min_ly_speed_);
      if (move_right)
        y_speed = alg::mapValue(fabs(a_speed), 0.0, max_a_speed_, max_ry_speed_, min_ry_speed_);

      if (in_position)
      {
        cli::print("in position, crawl");
        nextState(STATE_CRAWL);
        break;
      }

      walking->X_MOVE_AMPLITUDE = x_speed;
      walking->Y_MOVE_AMPLITUDE = y_speed;
      walking->A_MOVE_AMPLITUDE = a_speed;
      walking->A_MOVE_AIM_ON = false;
      walking->Start();

      break;
    }

    case STATE_CRAWL:
    {
      cli::printBlock("crawl");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        state_crawl_walk_stop_ = false;
        state_crawl_down_ = false;
        state_almost_crawl_up_ = false;
        state_crawl_up_ = false;
        state_crawl_count_ = 0;
      }

      if (!state_crawl_walk_stop_)
      {
        cli::print("walk in position");
        if (locomotion->walkInPositionUntilStop())
        {
          usleep(0.5 * 1000000);
          state_crawl_walk_stop_ = true;
        }
      }
      else
      {
        if (action->IsRunning())
        {
          cli::print("playing motion");
        }
        else
        {
          if (!state_crawl_down_)
          {
            cli::print("crawl down");
            state_crawl_down_ = true;
            fallen_getup_ = false;

            action->jointEnable();
            action->Start(69);
          }
          else if (state_crawl_count_ < state_crawl_max_count_)
          {
            cli::printParameter("crawl left", (state_crawl_max_count_ - state_crawl_count_));
            state_crawl_count_++;

            action->jointEnable();
            action->Start(70);
          }
          else if (!state_almost_crawl_up_)
          {
            cli::print("almost crawl up");
            state_almost_crawl_up_ = true;

            action->jointEnable();
            action->Start(71);
          }
          else if (!state_crawl_up_)
          {
            cli::print("crawl up");
            state_crawl_up_ = true;

            action->jointEnable();
            action->Start(4);
          }
          else
          {
            cli::print("walk");
            usleep(0.5 * 1000000);
            nextState(STATE_FORWARD_MOVE);

            fallen_getup_ = true;
            walking->jointEnable();
          }
        }
      }

      break;
    }
  }
}