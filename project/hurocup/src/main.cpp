#include "hurocup/hurocup.h"
#include "config.h"
#include "linux.h"
#include "cli.h"

#include <unistd.h>
#include <libgen.h>

int handleError( int status, const char* func_name, const char* err_msg,
                 const char* file_name, int line, void* userdata )
{
  return 0;
}

int main(int argc, char **argv)
{
	// prevent run from non root user
	if (linux::isRoot())
	{
		cli::printError("shouldn't run as root");
		return -1;
	}

	// change directory to execution directory
	char exepath[1024] = {0};
	if (readlink("/proc/self/exe", exepath, sizeof(exepath)) != -1)
	{
		if (chdir(dirname(exepath)) != 0)
		{
			cli::printError("change directory error!!");
      return -1;
		}
	}

	// redirect opencv error
	cv::redirectError(handleError);

	// reset cm740, arduino, and camera driver
	// system("modprobe -r ftdi_sio");
	// system("modprobe -r ch341");
	// system("modprobe -r uvcvideo");
	// usleep(1500000);
	// system("modprobe ch341");
	// system("modprobe ftdi_sio");
	// system("modprobe uvcvideo");

  Hurocup *hurocup = new Hurocup();
  hurocup->start();
  hurocup->loadArguments(argc, argv);

  usleep(2 * 1000000);

  while (hurocup->isActive())
  {
    hurocup->process();
  }

	return 0;
}
