#include "hurocup/united_soccer.h"

#include "cli.h"

UnitedSoccer::UnitedSoccer(Hurocup *hurocup) : Hurocup::Challenge(hurocup, Hurocup::Challenge::CHALLENGE_UNITED_SOCCER)
{
  name_ = "united_soccer";
  current_mode_ = MODE_KICKOFF;
  mode_count_ = 4;
  fallen_getup_ = true;

  Config::Section *section = config_.addSection("Soccer");
  section->addParam<float>("follow_tilt", -30.0)->setBindValue(&follow_tilt_);
  section->addParam<float>("dynamic_kick", 0.0)->setBindValue(&dynamic_kick_);
  section->addParam<bool>("always_dribble", false)->setBindValue(&always_dribble_);

  section = config_.addSection("Field");
  section->addParam<float>("field_width", 800.0)->setBindValue(&field_width_);
  section->addParam<float>("field_height", 600.0)->setBindValue(&field_height_);
  section->addParam<float>("field_circle_diameter", 100.0)->setBindValue(&field_circle_diameter_);

  section = config_.addSection("Player");
  section->addParam<float>("state_kickoff_x_position", 355.0)->setBindValue(&state_kickoff_x_position_);
  section->addParam<float>("state_kickoff_y_position", 300.0)->setBindValue(&state_kickoff_y_position_);

  section = config_.addSection("Motion");
  section->addParam<int>("right_kick", 6)->setBindValue(&right_kick_);
  section->addParam<int>("left_kick", 7)->setBindValue(&left_kick_);

  config_.sync("hurocup/united_soccer.yaml");

  field_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_FIELD);
  ball_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_BALL);

  ball_pos_x_ = -1.0;
  ball_pos_y_ = -1.0;

  detect_ball_ = false;
  detect_ball_sec_ = 0.0;
  lost_ball_sec_ = 0.0;

  ball_distance_x_ = 0.0;
  ball_distance_y_ = 0.0;

  state_kickoff_time_ = 0.0;

  state_release_time_ = 0.0;
  state_release_left_ = false;

  state_patrol_position_ = 0;
  state_patrol_in_position_time_ = 0.0;

  state_lost_search_front_time_ = 0.0;
  state_lost_search_last_ball_time_ = 0.0;

  state_lost_last_orientation_ = 0.0;
  state_lost_last_ball_x_ = 0.0;
  state_lost_last_ball_y_ = 0.0;
  state_lost_search_rotate_left_ = 0.0;

  state_dribble_pivot_finish_ = false;
  state_dribble_direction_ = 0.0;

  state_position_pivot_finish_ = false;
  state_position_last_ball_x_ = 0.0;
  state_position_last_ball_y_ = 0.0;
  state_position_kick_direction_ = 0.0;

  state_kick_walk_stop_ = false;
}

void UnitedSoccer::start()
{
  Hurocup::Challenge::start();

  config_.load("hurocup/united_soccer.yaml");

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  hurocup_->getWalking()->loadConfig();
  hurocup_->getWalking()->jointEnable();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, -30.0);

  switch (current_mode_)
  {
  case MODE_RELEASE_LEFT:
  case MODE_RELEASE_RIGHT:
    nextState(STATE_RELEASE);
    state_release_left_ = (current_mode_ == MODE_RELEASE_LEFT);
    break;

  case MODE_KICKOFF:
  case MODE_KICKOFF_OPPONENT:
    nextState(STATE_KICKOFF);
    break;
  }
}

void UnitedSoccer::stop()
{
  Hurocup::Challenge::stop();

  config_.load("hurocup/united_soccer.yaml");

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  hurocup_->getWalking()->loadConfig();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, -30.0);
}

void UnitedSoccer::visionProcess()
{
  Hurocup::Challenge::visionProcess();

  Image *stream_image = hurocup_->getStreamImage();

  Robot::Camera *camera = Robot::Camera::getInstance();
	cv::Mat hsv = camera->getBuffer()->m_HSVInput.clone();
	cv::Size mat_size = hsv.size();

  Robot::Head *head = Robot::Head::getInstance();

  // classify field color and draw
  cv::Mat field_binary_mat = field_classifier_->classify(hsv);
	stream_image->filterMat(field_binary_mat, 0, 128, 255);

  // classify ball color and draw
  cv::Mat ball_binary_mat = ball_classifier_->classify(hsv);
	stream_image->filterMat(ball_binary_mat, 255, 128, 0);

    // get field contours
	Contours field_contours(field_binary_mat);
  field_contours.filterLargerThen(200.0);
  field_contours.joinAll();
  field_contours.convexHull();

  // draw field contours
  cv::Mat field_contours_mat = field_contours.getBinaryMatLine(mat_size, 4);
  stream_image->filterMat(field_contours_mat, 96, 96, 192);

  // filter field by tilt
	Contours filtered_field_contours = field_contours;

  float bottom_fill = camera->getHeight();
  bottom_fill -= alg::mapValue(head->getTiltAngle(), -45.0, -30.0, camera->getHeight(), 0.0);
  filtered_field_contours.fillRect(0.0, bottom_fill, camera->getWidth(), camera->getHeight());

  float top_erase = alg::mapValue(head->getTiltAngle(), -15.0, 30.0, 0.0, camera->getHeight());
  filtered_field_contours.removeRect(0.0, 0.0, camera->getWidth(), top_erase);

  filtered_field_contours.strecthUp(8.0);

  // draw filtered field contours
  cv::Mat filtered_field_contours_mat = filtered_field_contours.getBinaryMatLine(mat_size, 4);
  stream_image->filterMat(filtered_field_contours_mat, 128, 128, 255);

  // filter ball by filtered field
	cv::bitwise_and(filtered_field_contours.getBinaryMat(mat_size), ball_binary_mat, ball_binary_mat);

  // get ball contours
  Contours ball_contours(ball_binary_mat);
  ball_contours.filterLargest();
  ball_contours.filterLargerThen(5.0);

  // get ball circles
  Circles ball_circles(ball_contours.getContours());

  // draw ball circles
  cv::Mat ball_circles_mat = ball_circles.getBinaryMatLine(mat_size, 4);
  stream_image->filterMat(ball_circles_mat, 255, 255, 0);

  ball_pos_x_ = ball_circles.getFirstCenter().x;
  ball_pos_y_ = ball_circles.getFirstCenter().y;
}

void UnitedSoccer::process()
{
  Hurocup::Challenge::process();

  if (fallen_)
    return;

  Robot::Walking *walking = hurocup_->getWalking();
  Robot::Action *action = hurocup_->getAction();;
  Robot::Head *head = hurocup_->getHead();
  Robot::Camera *camera = hurocup_->getCamera();
  Locomotion *locomotion = hurocup_->getLocomotion();
  MPU *mpu = hurocup_->getMPU();

  float delta_sec = hurocup_->getDeltaSec();

  detect_ball_ = (ball_pos_x_ > 0 && ball_pos_y_ > 0);
  if (detect_ball_)
  {
    detect_ball_sec_ += delta_sec;
    lost_ball_sec_ = 0.0;

    // calculate ball distance
    float distance = head->calculateDistanceFromPanTilt();
    float direction = mpu->getAngle() - head->getPanAngle();

    ball_distance_x_ = distance * cos(direction * alg::deg2Rad());
    ball_distance_y_ = distance * sin(direction * alg::deg2Rad());
  }
  else
  {
    ball_pos_x_ = camera->getWidth() / 2;
    ball_pos_y_ = camera->getHeight() / 2;

    lost_ball_sec_ += delta_sec;
    if (lost_ball_sec_ > 1.0)
    {
        detect_ball_sec_ = 0.0;
    }
  }

  switch (current_state_)
  {
  case STATE_KICKOFF:
    {
      cli::printBlock("state kickoff");

      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        state_kickoff_time_ = 0.0;
        locomotion->setPosition(state_kickoff_x_position_, state_kickoff_y_position_);
      }

      if (locomotion->rotateToTarget(mpu->getAngle() - head->getPanAngle()))
      {
        locomotion->walkInPositionUntilStop();
      }

      state_kickoff_time_ += delta_sec;
      if (state_kickoff_time_ > ((current_mode_ ==  MODE_KICKOFF_OPPONENT) ? 13.0 : 3.0))
      {
        cli::print("kickoff end");
        nextState(STATE_PLAY);
      }

      break;
    }

  case STATE_RELEASE:
    {
      cli::printBlock("state release");

      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        state_release_time_ = 0.0;

        if (state_release_left_)
        {
          cli::print("release left");
          locomotion->setOrientation(90.0);
          locomotion->setPosition(field_width_ * 0.5 - field_circle_diameter_ * 0.5, -20.0);
        }
        else
        {
          cli::print("release right");
          locomotion->setOrientation(-90.0);
          locomotion->setPosition(field_width_ * 0.5 - field_circle_diameter_ * 0.5, field_height_ + 20.0);
        }
      }

      state_release_time_ += delta_sec;
      cli::print("waiting");

      if (state_release_time_ > 3.0)
      {
        cli::print("done waiting");
        nextState(STATE_PLAY);
      }

      break;
    }

  case STATE_PLAY:
    {
      cli::printBlock("state play");

      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
      }

      nextState(STATE_PATROL_POSITION);

      break;
    }

  case STATE_PATROL_POSITION:
    {
      cli::printBlock("state patrol");

      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        state_patrol_position_ = (locomotion->getPositionX() < field_width_ * 0.45) ? 0 : 1;
				state_patrol_in_position_time_ = 0.0;
      }

      if (detect_ball_sec_ > 0.5)
      {
        cli::print("found ball, follow ball");
        nextState(STATE_FOLLOW_BALL);

        break;
      }

      float target_x = field_width_ * 0.5;
      float target_y = field_height_ * 0.5;

      switch (state_patrol_position_)
      {
      case 0: target_x = field_width_ * 0.5; break;
      case 1: target_x = field_width_ * 0.85; break;
      case 2: target_x = field_width_ * 0.5; break;
      case 3: target_x = field_width_ * 0.25; break;
      }

      // move inside field
      if (alg::valueOutside(locomotion->getPositionY(), field_height_ * 0.1, field_height_ * 0.9))
      {
        cli::print("near edge, move inside");
        target_x = locomotion->getPositionX();
      }

      cli::printParameter("target x", target_x);
      cli::printParameter("target y", target_y);

      if (locomotion->moveToTarget(target_x, target_y))
      {
        cli::print("reach target position, stop");
        locomotion->walkInPositionUntilStop();

        state_patrol_in_position_time_ += delta_sec;
        if (state_patrol_in_position_time_ > 5.0)
        {
          state_patrol_position_ = (state_patrol_position_ + 1) % 4;
          state_patrol_in_position_time_ = 0.0;
        }
      }

      break;
    }

  case STATE_FOLLOW_BALL:
    {
      cli::printBlock("state follow ball");

      if (initial_)
      {
          cli::print("initial");
          initial_ = false;
      }

      if (detect_ball_sec_ > 0.5)
      {
				cli::print("follow ball");

        if (locomotion->moveFollowHead(follow_tilt_))
        {
          cli::print("done following");

					if (always_dribble_) //if (alg::valueInside(ball_x, 375, 525) || always_dribble_)
          {
              cli::print("dribble ball");
              nextState(STATE_DRIBBLE_BALL);
          }
          else
          {
            cli::print("positioning ball");
            nextState(STATE_POSITION_BALL);
          }
        }
      }
      else
      {
        cli::print("walk in position");
        locomotion->walkInPosition();

        if (lost_ball_sec_ > 1.0)
        {
          nextState(STATE_LOST_BALL);
          break;
        }
      }

      break;
    }

  case STATE_LOST_BALL:
    {
      cli::printBlock("state lost ball");

      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        state_lost_search_front_time_ = 0.0;
        state_lost_search_last_ball_time_ = 0.0;

        state_lost_last_orientation_ = mpu->getAngle();
        state_lost_last_ball_x_ = locomotion->getPositionX() + ball_distance_x_;
        state_lost_last_ball_y_ = locomotion->getPositionY() + ball_distance_y_;

        float direction = alg::direction(state_lost_last_ball_x_, state_lost_last_ball_y_) * alg::rad2Deg();
        state_lost_search_rotate_left_ = (alg::deltaAngle(state_lost_last_orientation_, direction) > 0.0);
      }

      if (detect_ball_sec_ > 1.0)
      {
        cli::print("found ball, following");
        nextState(STATE_FOLLOW_BALL);
      }
      else
      {
        if (state_lost_search_front_time_ < 3.0)
        {
          cli::print("walk in position until stop");
          if (locomotion->walkInPositionUntilStop())
          {
            state_lost_search_front_time_ += delta_sec;
          }
        }
        else
        {
          if (state_lost_search_last_ball_time_ < 3.0)
          {
            cli::print("rotate to last ball position");

            float delta_x = state_lost_last_ball_x_ - locomotion->getPositionX();
            float delta_y = state_lost_last_ball_y_ - locomotion->getPositionY();

            float direction = alg::direction(delta_x, delta_y) * alg::rad2Deg();
            if (locomotion->rotateToTarget(direction))
            {
              cli::print("walk in position until stop");
              if (locomotion->walkInPositionUntilStop())
              {
                state_lost_search_last_ball_time_ += delta_sec;
              }
            }
          }
          else
          {
            if (state_lost_search_rotate_left_)
            {
              cli::print("rotate left");
              if (alg::deltaAngle(mpu->getAngle(), state_lost_last_orientation_) > 0)
              {
                if (locomotion->rotateToTarget(state_lost_last_orientation_))
                {
                  cli::print("lost ball, state play");
                  nextState(STATE_PLAY);
                }
              }
              else
              {
                locomotion->rotateToTarget(mpu->getAngle() - 30.0);
              }
            }
            else
            {
              cli::print("rotate right");
              if (alg::deltaAngle(mpu->getAngle(), state_lost_last_orientation_) < 0)
              {
                if (locomotion->rotateToTarget(state_lost_last_orientation_))
                {
                  cli::print("lost ball, state play");
                  nextState(STATE_PLAY);
                }
              }
              else
              {
                locomotion->rotateToTarget(mpu->getAngle() + 30.0);
              }
            }
          }
        }
      }

      break;
    }

  case STATE_DRIBBLE_BALL:
    {
      cli::printBlock("state dribble ball");

      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        state_dribble_pivot_finish_ = false;
        state_dribble_direction_ = 0.0;
      }

      if (detect_ball_sec_ > 0.5)
      {
        if ((head->getTiltAngle() > follow_tilt_ + 5.0) || (fabs(head->getPanAngle()) > 45.0))
        {
          cli::print("ball too far, follow");
          nextState(STATE_FOLLOW_BALL);
          break;
        }

        if (!state_dribble_pivot_finish_)
        {
          if (locomotion->pivot(state_dribble_direction_))
          {
            state_dribble_pivot_finish_ = true;
          }
        }
        else if (alg::valueInside(locomotion->getPositionX(), 350, 550) || always_dribble_)
        {
          cli::print("dribble");
          locomotion->dribble(state_dribble_direction_);
        }
        else
        {
          cli::print("done dribble, follow ball");
          nextState(STATE_FOLLOW_BALL);
        }
      }
      else
      {
        cli::print("walk in position");
        locomotion->walkInPosition();

        if (lost_ball_sec_ > 1.0)
        {
          cli::print("lost ball");
          nextState(STATE_LOST_BALL);
          break;
        }
      }

      break;
    }

  case STATE_POSITION_BALL:
    {
      cli::printBlock("state position ball");

      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        state_position_pivot_finish_ = false;

        state_position_last_ball_x_ = 0.0;
        state_position_last_ball_y_ = 0.0;

        bool kick_straight = false;
        kick_straight |= (locomotion->getPositionX() < 200.0);
        kick_straight |= (locomotion->getPositionX() > 800.0 && alg::valueInside(locomotion->getPositionY(), 200.0, 400.0));

        if (kick_straight)
        {
          cli::print("kick straight");
          state_position_kick_direction_ = 0.0;
        }
        else
        {
          cli::print("kick to goal");

          float delta_x = field_width_ - locomotion->getPositionX();
          float delta_y = (field_height_ * 0.5) - locomotion->getPositionY();

          float goal_orientation = alg::direction(delta_x, delta_y) * alg::rad2Deg();
          state_position_kick_direction_ = alg::clampValue(goal_orientation, -60.0, 60.0);
        }
      }

      if (state_position_last_ball_x_ != 0.0 || state_position_last_ball_y_ != 0.0)
      {
        cli::print("reset position according to ball");
        double pos_x = state_position_last_ball_x_ - ball_distance_x_;
        double pos_y = state_position_last_ball_y_ - ball_distance_y_;

        locomotion->setPosition(pos_x, pos_y);
      }
      else
      {
        if (detect_ball_ && detect_ball_sec_ > 0.5)
        {
          state_position_last_ball_x_ = locomotion->getPositionX() + ball_distance_x_;
          state_position_last_ball_y_ = locomotion->getPositionY() + ball_distance_y_;
        }
      }

      if (detect_ball_sec_ > 0.5)
      {
        if (head->getTiltAngle() - fabs(head->getPanAngle() * 0.5) > follow_tilt_ + 5.0)
        {
          cli::print("ball too far, follow");
          nextState(STATE_FOLLOW_BALL);
          break;
        }

        cli::printParameter("kick direction", state_position_kick_direction_);
        if (!state_position_pivot_finish_)
        {
          cli::print("pivoting");
          if (locomotion->pivot(state_position_kick_direction_))
          {
              state_position_pivot_finish_ = true;
          }
        }
        else
        {
          if (head->getPanAngle() - camera->calculatePanFromXPos(ball_pos_x_) > 0.0)
          {
            cli::print("left foot");
            if (locomotion->positionLeftKick(state_position_kick_direction_))
            {
              cli::print("done kick positioning");
              nextState(STATE_KICK_BALL);
            }
          }
          else
          {
            cli::print("right foot");
            if (locomotion->positionRightKick(state_position_kick_direction_))
            {
              cli::print("done kick positioning");
              nextState(STATE_KICK_BALL);
            }
          }
        }
      }
      else
      {
        cli::print("move backward");
        locomotion->moveBackward(mpu->getAngle());

        if (lost_ball_sec_ > 5.0)
        {
          cli::print("lost ball");
          nextState(STATE_LOST_BALL);
        }
      }

      break;
    }

  case STATE_KICK_BALL:
    {
      cli::print("state kick ball");

      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        state_kick_walk_stop_ = false;
      }

      if (!state_kick_walk_stop_)
      {
        if (dynamic_kick_ > 5.0)
        {
          state_kick_walk_stop_ = true;
          if (head->getPanAngle() - camera->calculatePanFromXPos(ball_pos_x_) > 0.0)
          {
            walking->setDynamicLeftKick(dynamic_kick_);
          }
          else
          {
            walking->setDynamicRightKick(dynamic_kick_);
          }
        }
        else
        {
          if (locomotion->walkInPositionUntilStop())
          {
            if (action->IsRunning() == false)
            {
              usleep(0.5 * 1000000);
              state_kick_walk_stop_ = true;

              action->jointEnable();
              if (head->getPanAngle() - camera->calculatePanFromXPos(ball_pos_x_) > 0.0)
              {
                cli::print("left kick");
                action->Start(left_kick_);
              }
              else
              {
                cli::print("right kick");
                action->Start(right_kick_);
              }
            }
          }
        }
      }
      else
      {
        if (dynamic_kick_ > 5.0)
        {
          cli::printParameter("dynamic kick", (float)walking->getDynamicKick());
          if (walking->getDynamicKick() < 10.0)
          {
            cli::print("done dynamic kicking, follow");
            nextState(STATE_FOLLOW_BALL);

            walking->setDynamicLeftKick(0.0);
            walking->setDynamicRightKick(0.0);
          }
        }
        else
        {
          if (action->IsRunning() == false)
          {
            walking->jointEnable();
            if (detect_ball_sec_ > 0.5)
            {
              cli::print("done kicking, follow");
              nextState(STATE_FOLLOW_BALL);
            }
            else
            {
              cli::print("lost ball after kicking");
              nextState(STATE_LOST_BALL);
            }
          }
        }
      }

      break;
    }
  }

  bool default_head = true;

  switch (current_state_)
  {
  case STATE_KICKOFF:
    if (state_kickoff_time_ < 1.0)
    {
      cli::print("head down");
      head->lookToPosition(field_width_ / 2, field_height_ / 2);
      default_head = false;
    }
    break;

  case STATE_LOST_BALL:
    if (prev_state_ == STATE_KICK_BALL)
    {
      if (lost_ball_sec_ < 1.0)
      {
        cli::print("look forward");
        head->moveScanVertical();
        default_head = false;
      }
    }
    break;
  }

  if (default_head)
  {
    if (lost_ball_sec_ < 1.0)
    {
      cli::print("track ball");
      head->moveTracking(-camera->calculatePanFromXPos(ball_pos_x_), -camera->calculateTiltFromYPos(ball_pos_y_));
    }
    else
    {
      cli::print("scan ball");
      head->moveScanBallDown();
    }
  }
}