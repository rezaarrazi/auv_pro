#include "hurocup/basketball.h"

#include "cli.h"

Basketball::Basketball(Hurocup *hurocup) : Hurocup::Challenge(hurocup, Hurocup::Challenge::CHALLENGE_BASKETBALL)
{
  name_ = "basketball";
  current_mode_ = MODE_BASKETBALL;
  mode_count_ = 1;
  fallen_getup_ = true;

  Config::Section *section = config_.addSection("Basketball");
  section->addParam<float>("target_pan", 30.0)->setBindValue(&target_pan_);
  section->addParam<float>("target_tilt", 40.0)->setBindValue(&target_tilt_);

  config_.sync("hurocup/basketball.yaml");

  basketball_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_BASKETBALL);
  basket_classifier_  = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_RED);
}

void Basketball::start()
{
  Hurocup::Challenge::start();

  config_.load("hurocup/basketball.yaml");

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  hurocup_->getWalking()->loadConfig();
  hurocup_->getWalking()->jointEnable();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, 0.0);

  nextState(STATE_LOOK_FOR_BALL);
}

void Basketball::stop()
{
  Hurocup::Challenge::stop();

  config_.load("hurocup/basketball.yaml");

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  hurocup_->getWalking()->loadConfig();

  hurocup_->getWalking()->jointEnable();
  hurocup_->getWalking()->forceStop();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, 0.0);
}

void Basketball::visionProcess()
{
  Hurocup::Challenge::visionProcess();

  Image *stream_image = hurocup_->getStreamImage();

  Robot::Camera *camera = Robot::Camera::getInstance();
	cv::Mat hsv = camera->getBuffer()->m_HSVInput.clone();
	cv::Size mat_size = hsv.size();

  // classify basketball color and draw
  cv::Mat basketball_binary_mat = basketball_classifier_->classify(hsv);
	stream_image->filterMat(basketball_binary_mat, 255, 128, 0);

  // get basketball contours
  Contours basketball_contours(basketball_binary_mat);
  basketball_contours.filterLargest();
  basketball_contours.filterLargerThen(5.0);

  // get basketball circles
  Circles basketball_circles(basketball_contours.getContours());

  // draw basketball circles
  cv::Mat basketball_circles_mat = basketball_circles.getBinaryMatLine(mat_size, 4);
  stream_image->filterMat(basketball_circles_mat, 255, 255, 0);

  basketball_pos_x_ = basketball_circles.getFirstCenter().x;
  basketball_pos_y_ = basketball_circles.getFirstCenter().y;

  // classify red color and draw
  cv::Mat basket_binary_mat = basket_classifier_->classify(hsv);
	stream_image->filterMat(basket_binary_mat, 255, 128, 0);

  // get red contours
  Contours basket_contours(basket_binary_mat);
  basket_contours.filterLargest();
  basket_contours.filterLargerThen(5.0);

  // get red circles
  Circles basket_circles(basket_contours.getContours());

  // draw red circles
  cv::Mat basket_circles_mat = basket_circles.getBinaryMatLine(mat_size, 4);
  stream_image->filterMat(basket_circles_mat, 255, 255, 0);

  basket_pos_x_ = basket_circles.getFirstCenter().x;
  basket_pos_y_ = basket_circles.getFirstCenter().y;

}

void Basketball::process()
{
  Hurocup::Challenge::process();

  if (fallen_)
    return;

  Robot::Head *head = hurocup_->getHead();
  Robot::Walking *walking = hurocup_->getWalking();
  Robot::Action *action = hurocup_->getAction();
  Locomotion *locomotion = hurocup_->getLocomotion();
  MPU *mpu = hurocup_->getMPU();

  switch (current_state_)
  {
    case STATE_LOOK_FOR_BALL:
    {
      cli::printBlock("state_look_for_ball");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
      }

      walking->POSITION_X = 0.0;
      walking->POSITION_Y = 0.0;

      hurocup_->getHead()->moveByAngle(0.0, 0.0);

      walking->Start();

      if (locomotion->moveFollowHead(target_tilt_))
      {
        cli::print("found the ball, get it");

        action->jointEnable();
        action->Start(69); //motion ambil bola
        nextState(STATE_AIM_BASKET);
      }
      break;
    }

    case STATE_AIM_BASKET:
    {
      cli::printBlock("state_aim_basket");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
        hurocup_->getHead()->jointEnable();
        hurocup_->getHead()->moveScanHorizontal();
        hurocup_->getHead()->moveByAngle(0.0, 0.0);
      }

      if(basket_pos_x_ > 0)
      {
        head->stop();
        locomotion->rotateToTarget(-(hurocup_->getHead()->getPanAngle()));
        nextState(STATE_SHOOT);
      }
      break;
    }

    case STATE_ARM_POSITIONING:
    {
      cli::printBlock("state_shoot");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
      }

      action->jointEnable();
      action->Start(69); //motion hampir lempar

      break;
    }

    case STATE_SHOOT:
    {
      cli::printBlock("state_shoot");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;
      }

      action->jointEnable();
      action->wUnitTimeCountAddition  = 12;
      action->Start(69); //motion lempar
      action->wUnitTimeCountAddition  = 1;

      break;
    }
  }
}
