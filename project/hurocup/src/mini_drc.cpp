#include "hurocup/mini_drc.h"

#include "cli.h"

MiniDRC::MiniDRC(Hurocup *hurocup) : Hurocup::Challenge(hurocup, Hurocup::Challenge::CHALLENGE_MINI_DRC)
{
  name_ = "mini_drc";
  current_mode_ = MODE_MINI_DRC;
  mode_count_ = 1;
  fallen_getup_ = true;

  state_side_angle_ = 0.0;
  state_motion_walk_stop_ = false;
}

void MiniDRC::start()
{
  Hurocup::Challenge::start();

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  hurocup_->getWalking()->loadConfig();
  hurocup_->getWalking()->jointEnable();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, 0.0);

  nextState(STATE_IDLE);
}

void MiniDRC::stop()
{
  Hurocup::Challenge::stop();

  hurocup_->getWalking()->setConfigName("walking/default.yaml");
  hurocup_->getWalking()->loadConfig();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, 0.0);
}

void MiniDRC::visionProcess()
{
  Hurocup::Challenge::visionProcess();
}

void MiniDRC::process()
{
  Hurocup::Challenge::process();

  Robot::Head *head = hurocup_->getHead();
  Robot::Action* action = hurocup_->getAction();
  Robot::Walking *walking = hurocup_->getWalking();

  Locomotion *locomotion = hurocup_->getLocomotion();

  MPU *mpu = hurocup_->getMPU();

  if (!fallen_)
  {
    // walking
    switch (current_state_)
    {
    case STATE_IDLE:
      {
        cli::printBlock("idle");
        if (initial_)
        {
          cli::print("initial");
          initial_ = false;

          walking->jointEnable();
          walking->loadConfig();
        }

        int key = httpd::popKeyboardKey();
        switch (key)
        {
        case httpd::KEYCODE_W:
        case httpd::KEYCODE_S:
        case httpd::KEYCODE_A:
        case httpd::KEYCODE_D:
        case httpd::KEYCODE_Q:
        case httpd::KEYCODE_E:
          nextState(STATE_WALK_IN_POSITION);
          break;

        default:
          httpd::pushKeyboardKey(key);
          break;
        }

        locomotion->walkInPositionUntilStop();

        break;
      }

    case STATE_WALK_IN_POSITION:
      {
        cli::printBlock("walk in position");
        if (initial_)
        {
          cli::print("initial");
          initial_ = false;

          walking->A_MOVE_AMPLITUDE = 0.0;
        }

        double a_speed = walking->A_MOVE_AMPLITUDE;

        int key = httpd::popKeyboardKey();
        switch (key)
        {
        case httpd::KEYCODE_Q:
          if (a_speed < -1.0)
            a_speed = 0.0;
          else
            a_speed = alg::minValue(a_speed + 5.0, 15.0);
          break;

        case httpd::KEYCODE_E:
          if (a_speed > 1.0)
            a_speed = 0.0;
          else
            a_speed = alg::maxValue(a_speed - 5.0, -15.0);
          break;

        case httpd::KEYCODE_W:
        case httpd::KEYCODE_S:
          nextState(STATE_FORWARD_MOVE);
          break;

        case httpd::KEYCODE_A:
        case httpd::KEYCODE_D:
          nextState(STATE_SIDE_MOVE);
          break;

        case httpd::KEYCODE_SHIFT:
          nextState(STATE_IDLE);
          break;

        default:
          httpd::pushKeyboardKey(key);
          break;
        }

        walking->X_MOVE_AMPLITUDE = 0.0;
        walking->Y_MOVE_AMPLITUDE = 0.0;
        walking->A_MOVE_AMPLITUDE = a_speed;
        walking->A_MOVE_AIM_ON = false;
        walking->Start();

        break;
      }

    case STATE_FORWARD_MOVE:
      {
        cli::printBlock("forward move");
        if (initial_)
        {
          cli::print("initial");
          initial_ = false;

          walking->X_MOVE_AMPLITUDE = 0.0;
          walking->Y_MOVE_AMPLITUDE = 0.0;
          walking->A_MOVE_AMPLITUDE = 0.0;
        }

        double x_speed = walking->X_MOVE_AMPLITUDE;
        double y_speed = walking->Y_MOVE_AMPLITUDE;
        double a_speed = walking->A_MOVE_AMPLITUDE;

        int key = httpd::popKeyboardKey();
        switch (key)
        {
        case httpd::KEYCODE_W:
          if (x_speed < -5.0)
            x_speed = 0.0;
          else
            x_speed = alg::minValue(x_speed + 10.0, 50.0);
          y_speed = 0.0;
          a_speed = 0.0;
          break;

        case httpd::KEYCODE_S:
          if (x_speed > 5.0)
            x_speed = 0.0;
          else
            x_speed = alg::maxValue(x_speed - 10.0, -50.0);
          y_speed = 0.0;
          a_speed = 0.0;
          break;

        case httpd::KEYCODE_Q:
          if (a_speed < -1.0)
            a_speed = 0.0;
          else
            a_speed = alg::minValue(a_speed + 5.0, 15.0);
          break;

        case httpd::KEYCODE_E:
          if (a_speed > 1.0)
            a_speed = 0.0;
          else
            a_speed = alg::maxValue(a_speed - 5.0, -15.0);
          break;

        case httpd::KEYCODE_A:
          if (y_speed < -1.0)
            y_speed = 0.0;
          else
            y_speed = alg::minValue(y_speed + 5.0, 20.0);
          break;

        case httpd::KEYCODE_D:
          if (y_speed > 1.0)
            y_speed = 0.0;
          else
            y_speed = alg::maxValue(y_speed - 5.0, -20.0);
          break;

        case httpd::KEYCODE_SHIFT:
          nextState(STATE_WALK_IN_POSITION);
          break;

        default:
          httpd::pushKeyboardKey(key);
          break;
        }

        walking->X_MOVE_AMPLITUDE = x_speed;
        walking->Y_MOVE_AMPLITUDE = y_speed;
        walking->A_MOVE_AMPLITUDE = a_speed;
        walking->A_MOVE_AIM_ON = false;
        walking->Start();

        break;
      }

    case STATE_SIDE_MOVE:
      {
        cli::printBlock("side move");
        if (initial_)
        {
          cli::print("initial");
          initial_ = false;

          walking->Y_MOVE_AMPLITUDE = 0.0;
          walking->X_MOVE_AMPLITUDE = 0.0;
          state_side_angle_ = mpu->getAngle();
        }

        double x_speed = walking->X_MOVE_AMPLITUDE;
        double y_speed = walking->Y_MOVE_AMPLITUDE;

        int key = httpd::popKeyboardKey();
        switch (key)
        {
        case httpd::KEYCODE_A:
          if (y_speed < -1.0)
            y_speed = 0.0;
          else
            y_speed = alg::minValue(y_speed + 10.0, 50.0);
          x_speed = 0.0;
          break;

        case httpd::KEYCODE_D:
          if (y_speed > 1.0)
            y_speed = 0.0;
          else
            y_speed = alg::maxValue(y_speed - 10.0, -50.0);
          x_speed = 0.0;
          break;

        case httpd::KEYCODE_W:
          if (x_speed < -1.0)
            x_speed = 0.0;
          else
            x_speed = alg::maxValue(x_speed + 5.0, 20.0);
          break;

        case httpd::KEYCODE_S:
          if (x_speed > 1.0)
            x_speed = 0.0;
          else
            x_speed = alg::maxValue(x_speed - 5.0, -20.0);
          break;

        case httpd::KEYCODE_SHIFT:
          nextState(STATE_WALK_IN_POSITION);
          break;

        default:
          httpd::pushKeyboardKey(key);
          break;
        }

        float delta_direction = alg::deltaAngle(state_side_angle_, mpu->getAngle());
        double a_speed = alg::mapValue(delta_direction, -10.0, 10.0, 15.0, -15.0);

        walking->X_MOVE_AMPLITUDE = x_speed;
        walking->Y_MOVE_AMPLITUDE = y_speed;
        walking->A_MOVE_AMPLITUDE = a_speed;
        walking->A_MOVE_AIM_ON = false;
        walking->Start();

        break;
      }
    }

    // motion
    switch (current_state_)
    {
    case STATE_MOTION_VALVE:
      {
        cli::printBlock("motion valve");
        if (initial_)
        {
          cli::print("initial");
          initial_ = false;

          state_motion_walk_stop_ = false;
        }

        if (!state_motion_walk_stop_)
        {
          if (locomotion->walkInPositionUntilStop())
          {
            cli::print("walk stop");
            state_motion_walk_stop_ = true;

            action->jointEnable();
            action->Start(57);
          }
        }
        else
        {
          cli::print("playing motion");
          if (action->IsRunning() == false)
          {
            cli::print("done, idle");
            nextState(STATE_IDLE);

            walking->jointEnable();
          }
        }

        break;
      }

    default:
      {
        int key = httpd::popKeyboardKey();
        switch (key)
        {
        case httpd::KEYCODE_0:
          nextState(STATE_MOTION_VALVE);
          break;

        default:
          httpd::pushKeyboardKey(key);
          break;
        }

        break;
      }
    }
  }

  int key = httpd::popKeyboardKey();
  switch (key)
  {
  case httpd::KEYCODE_Z:
    walking->INIT_L_SHOULDER_PITCH += 10.0;
    break;

  case httpd::KEYCODE_X:
    walking->INIT_L_SHOULDER_PITCH -= 10.0;
    break;

  case httpd::KEYCODE_C:
    walking->INIT_L_SHOULDER_ROLL += 10.0;
    break;

  case httpd::KEYCODE_V:
    walking->INIT_L_SHOULDER_ROLL -= 10.0;
    break;

  case httpd::KEYCODE_B:
    walking->INIT_L_ELBOW += 10.0;
    break;

  case httpd::KEYCODE_N:
    walking->INIT_L_ELBOW -= 10.0;
    break;

  case httpd::KEYCODE_U:
    cli::print("reset pan and tilt");
    head->moveByAngle(0.0, 0.0);
    break;

  case httpd::KEYCODE_O:
    cli::print("reset pan");
    head->moveByAngle(0.0, head->getTiltAngle());
    break;

  case httpd::KEYCODE_J:
    cli::print("pan right");
    head->moveByAngle(head->getPanAngle() + 15.0, head->getTiltAngle());
    break;

  case httpd::KEYCODE_L:
    cli::print("pan left");
    head->moveByAngle(head->getPanAngle() - 15.0, head->getTiltAngle());
    break;

  case httpd::KEYCODE_I:
    cli::print("pan top");
    head->moveByAngle(head->getPanAngle(), head->getTiltAngle() + 15.0);
    break;

  case httpd::KEYCODE_K:
    cli::print("pan down");
    head->moveByAngle(head->getPanAngle(), head->getTiltAngle() - 15.0);
    break;

  default:
    httpd::pushKeyboardKey(key);
    break;
  }
}