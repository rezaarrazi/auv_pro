#include "hurocup/sprint.h"

#include "cli.h"

Sprint::Sprint(Hurocup *hurocup) : Hurocup::Challenge(hurocup, Hurocup::Challenge::CHALLENGE_SPRINT)
{
  name_ = "sprint";
  current_mode_ = MODE_SPRINT;
  mode_count_ = 1;
  fallen_getup_ = true;

  Config::Section *section = config_.addSection("Sprint");
  section->addParam<float>("sprint_distance", 310.0)->setBindValue(&sprint_distance_);
  section->addParam<float>("deceleration_distance", 290.0)->setBindValue(&deceleration_distance_);
  section->addParam<float>("min_deceleration", 0.2)->setBindValue(&min_deceleration_);
  section->addParam<float>("forward_angle_compensation", 0.0)->setBindValue(&forward_angle_compensation_);
  section->addParam<float>("backward_angle_compensation", 0.0)->setBindValue(&backward_angle_compensation_);

  section = config_.addSection("Speed");
  section->addParam<float>("min_fx_speed", 30.0)->setBindValue(&min_fx_speed_);
  section->addParam<float>("max_fx_speed", 40.0)->setBindValue(&max_fx_speed_);
  section->addParam<float>("min_bx_speed", -30.0)->setBindValue(&min_bx_speed_);
  section->addParam<float>("max_bx_speed", -40.0)->setBindValue(&max_bx_speed_);
  section->addParam<float>("max_a_speed", 15.0)->setBindValue(&max_a_speed_);

  config_.sync("hurocup/sprint.yaml");
}

void Sprint::start()
{
  Hurocup::Challenge::start();

  config_.load("hurocup/sprint.yaml");

  hurocup_->getWalking()->setConfigName("walking/sprint.yaml");
  hurocup_->getWalking()->loadConfig();
  hurocup_->getWalking()->jointEnable();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, 0.0);

  nextState(STATE_SPRINT_FORWARD);

  walk_in_position_counting_ = 0;
}

void Sprint::stop()
{
  Hurocup::Challenge::stop();

  config_.load("hurocup/sprint.yaml");

  hurocup_->getWalking()->setConfigName("walking/sprint.yaml");
  hurocup_->getWalking()->loadConfig();

  hurocup_->getWalking()->jointEnable();
  hurocup_->getWalking()->forceStop();

  hurocup_->getHead()->jointEnable();
  hurocup_->getHead()->moveByAngle(0.0, 0.0);
}

void Sprint::visionProcess()
{
  Hurocup::Challenge::visionProcess();
}

void Sprint::process()
{
  Hurocup::Challenge::process();

  printf("%d\n", walk_in_position_counting_);

  if (fallen_)
    return;

  Robot::Walking *walking = hurocup_->getWalking();
  Robot::Head *head = hurocup_->getHead();
  MPU *mpu = hurocup_->getMPU();

  switch (current_state_)
  {
  case STATE_SPRINT_FORWARD:
    {
      cli::printBlock("state_sprint_forward");
      if (initial_)
      {
        cli::print("initial");
        initial_ = false;

        walking->setConfigName("walking/sprint.yaml");
        walking->loadConfig();

        walking->POSITION_X = 0.0;
        walking->POSITION_Y = 0.0;
      }

      float delta_direction = alg::deltaAngle(forward_angle_compensation_, mpu->getAngle());

      double a_speed = alg::mapValue(delta_direction, -10.0, 10.0, max_a_speed_, -max_a_speed_);

      double deceleration = alg::mapValue(walking->POSITION_X, deceleration_distance_, sprint_distance_, 1.0, min_deceleration_);
      double x_speed = alg::mapValue(fabs(a_speed), 0.0, fabs(max_a_speed_), max_fx_speed_ * deceleration, min_fx_speed_ * deceleration);

      head->moveByAngle(0.0, 0.0);

      walking->X_MOVE_AMPLITUDE = x_speed;
      walking->Y_MOVE_AMPLITUDE = 0.0;
      walking->A_MOVE_AMPLITUDE = a_speed;
      walking->A_MOVE_AIM_ON = false;
      walking->Start();

      if (walking->POSITION_X > sprint_distance_)
      {
        cli::print("reach position, move backward");

        nextState(STATE_SPRINT_BACKWARD);
      }

      break;
    }

  case STATE_SPRINT_BACKWARD:
    {
      cli::printBlock("state_sprint_backward");
      if (initial_)
      {
        cli::print("initial");

        walking->setConfigName("walking/backward.yaml");

        if(walk_in_position_counting_<12)
        {
          hurocup_->getLocomotion()->walkInPosition();
          walking->HIP_PITCH_OFFSET = 11;
          walk_in_position_counting_++;

        }
        else
        {
          initial_ = false;
           walking->loadConfig();
        }

        break;
      }

      float delta_direction = alg::deltaAngle(backward_angle_compensation_, mpu->getAngle());

      double a_speed = alg::mapValue(delta_direction, -10.0, 10.0, max_a_speed_, -max_a_speed_);
      double x_speed = alg::mapValue(fabs(a_speed), 0.0, fabs(max_a_speed_), max_bx_speed_, min_bx_speed_);
      head->moveByAngle(0.0, 0.0);

      walking->X_MOVE_AMPLITUDE = x_speed;
      walking->Y_MOVE_AMPLITUDE = 0.0;
      walking->A_MOVE_AMPLITUDE = a_speed;
      walking->A_MOVE_AIM_ON = false;
      walking->Start();

      break;
    }
  }
}