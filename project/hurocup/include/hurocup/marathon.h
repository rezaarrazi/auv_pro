#ifndef HUROCUP_MARATHON_H_
#define HUROCUP_MARATHON_H_

#include "hurocup/hurocup.h"
#include "vision/color_classifier.h"

class Marathon : public Hurocup::Challenge
{
public:

  enum
  {
    MODE_MARATHON_COOL    = 0,
    MODE_MARATHON_WARM    = 1,
  };

  enum
  {
    STATE_FOLLOW_LINE     = 0,
    STATE_ODOMETRY        = 1,
    STATE_LOST            = 3
  };

  enum
  {
    MARKER_NONE       = 0,
    MARKER_FORWARD    = 1,
    MARKER_LEFT       = 2,
    MARKER_RIGHT      = 3
  };

  Marathon(Hurocup *hurocup);
  ~Marathon() { }

  void loadConfig() { config_.load("hurocup/marathon.yaml"); }
  void saveConfig() { config_.save("hurocup/marathon.yaml"); }

  void start();
  void stop();

  void visionProcess();
  void process();

private:

  ColorClassifier *red_classifier_;
  ColorClassifier *black_classifier_;
  ColorClassifier *white_classifier_;

  float follow_tilt_;
  float odometry_tilt_;

  float middle_top_boundary_;
  float middle_bottom_boundary_;
  float middle_left_boundary_;
  float middle_right_boundary_;

  float side_x_from_side_;
  float side_y_;
  float side_width_;
  float side_height_;

  float min_fx_speed_;
  float max_fx_speed_;
  float max_a_speed_;

  bool middle_inside_boundary_;
  float middle_center_;
  float middle_left_;
  float middle_right_;
  int marker_type_;
  int marker_counting_;

  float odometry_direction_;

  bool garis_habis;
  bool turn_left;
  bool turn_right;

};

#endif