#ifndef HUROCUP_BASKETBALL_H
#define HUROCUP_BASKETBALL_H

#include "hurocup/hurocup.h"

class Basketball : public Hurocup::Challenge
{
public:

  enum
  {
    MODE_BASKETBALL
  };

  enum
  {
    STATE_LOOK_FOR_BALL,
    STATE_AIM_BASKET,
    STATE_ARM_POSITIONING,
    STATE_SHOOT
  };

  Basketball(Hurocup *hurocup);
  ~Basketball() { }

  void loadConfig() { config_.load("hurocup/basketball.yaml"); }
  void saveConfig() { config_.save("hurocup/basketball.yaml"); }

  void start();
  void stop();

  void visionProcess();
  void process();

private:

  ColorClassifier *basketball_classifier_;
  ColorClassifier *basket_classifier_;  

  float basketball_pos_x_;
  float basketball_pos_y_;

  float basket_pos_x_;
  float basket_pos_y_;

  float min_fx_speed_;
  float max_fx_speed_;
  float min_bx_speed_;
  float max_bx_speed_;
  float max_a_speed_;

  float target_pan_;
  float target_tilt_;

};


#endif