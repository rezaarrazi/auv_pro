#ifndef HUROCUP_SPARTAN_H
#define HUROCUP_SPARTAN_H

#include "hurocup/hurocup.h"

class Spartan : public Hurocup::Challenge
{
public:
  enum
  {
    MODE_SPARTAN_NAIK = 0,
    MODE_SPARTAN_TURUN = 1
  };

  enum
  {
    STATE_FOLLOW    = 0,
    STATE_NAIK      = 1,
    STATE_TURUN     = 2
  };

  enum
  {
    COLOR_RED       = 1,
    COLOR_BLUE      = 2,
    COLOR_YELLOW    = 3,
    COLOR_GREEN     = 0,
  };

  Spartan(Hurocup *hurocup);
  ~Spartan() { }

  void loadConfig() { config_.load("hurocup/archery.yaml"); }
  void saveConfig() { config_.save("hurocup/archery.yaml"); }

  void start();
  void stop();

  void visionProcess();
  void process();

private:

  ColorClassifier *red_classifier_;
  ColorClassifier *yellow_classifier_;  
  ColorClassifier *blue_classifier_;  
  ColorClassifier *green_classifier_;  

  float boundary_x_;
  float boundary_y_;
  float boundary_width_;
  float boundary_height_;

  float terrain_x;
  float terrain_y;

  float min_tilt_;

  int top_terrain;
  int current_color;

  bool deket_kiri;
  bool deket_kanan;

  int counting_deket;
};


#endif