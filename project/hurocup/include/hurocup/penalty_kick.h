#ifndef HUROCUP_PENALTY_KICK_H_
#define HUROCUP_PENALTY_KICK_H_

#include "hurocup/hurocup.h"

class PenaltyKick : public Hurocup::Challenge
{
public:

  enum
  {
    MODE_SHOOT_LEFT     = 0,
    MODE_SHOOT_RIGHT    = 1
  };

  enum
  {
    STATE_FOLLOW_BALL       = 0,
    STATE_POSITION_BALL     = 1,
    STATE_KICK_BALL         = 2,
    STATE_MOVE_TO_TARGET    = 3,
    STATE_LOST_BALL         = 4
  };

  PenaltyKick(Hurocup *hurocup);
  ~PenaltyKick() { }

  void loadConfig() { config_.load("hurocup/penalty_kick.yaml"); }
  void saveConfig() { config_.save("hurocup/penalty_kick.yaml"); }

  void start();
  void stop();

  void visionProcess();
  void process();

private:

  ColorClassifier *field_classifier_;
  ColorClassifier *ball_classifier_;

  float follow_tilt_;
  float initial_position_x_;
  float initial_position_y_;
  float goal_position_x_;
  float goal_position_y_;
  bool left_kick_;
  bool right_kick_;
  int kick_count_;

  float shoot_left_direction_;
  float shoot_left_target_x_;
  float shoot_left_target_y_;

  float shoot_right_direction_;
  float shoot_right_target_x_;
  float shoot_right_target_y_;

  int motion_left_kick_id_;
  int motion_right_kick_id_;

  bool detect_ball_;
  float detect_ball_sec_;
  float lost_ball_sec_;

  float ball_pos_x_;
  float ball_pos_y_;

  float ball_distance_x_;
  float ball_distance_y_;

  bool state_position_pivot_finish_;
  float state_position_last_ball_x_;
  float state_position_last_ball_y_;
  float state_position_kick_direction_;

  bool state_kick_walk_stop_;
};

#endif