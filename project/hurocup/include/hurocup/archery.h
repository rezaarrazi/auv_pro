#ifndef HUROCUP_ARCHERY_H
#define HUROCUP_ARCHERY_H

#include "hurocup/hurocup.h"

class Archery : public Hurocup::Challenge
{
public:

  enum
  {
    MODE_ARCHERY   = 0
  };

  enum
  {
    STATE_WALK_TO_POSITION  = 0,
    STATE_WAITING  = 1,
    STATE_SHOOT  = 2
  };

  enum
  {
    LEFT  = 0,
    RIGHT = 1,
    STAY  = 2
  };

  Archery(Hurocup *hurocup);
  ~Archery() { }

  void loadConfig() { config_.load("hurocup/archery.yaml"); }
  void saveConfig() { config_.save("hurocup/archery.yaml"); }

  void start();
  void stop();

  void visionProcess();
  void process();

private:

  ColorClassifier *red_classifier_;
  ColorClassifier *yellow_classifier_;  
  ColorClassifier *white_classifier_;  

  Contours white_contours;

  float red_pos_x_;
  float red_pos_y_;

  float yellow_pos_x_;
  float yellow_pos_y_;

  float left_y;
  float right_y;

  float boundary_x_;
  float boundary_y_;
  float boundary_width_;
  float boundary_height_;

  bool target_locked;
  int target_locked_counting;

  int scanning;
  float scan_time;
  bool in_position;
};


#endif