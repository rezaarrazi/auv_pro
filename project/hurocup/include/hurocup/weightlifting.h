#ifndef HUROCUP_WEIGHTLIFTING_H_
#define HUROCUP_WEIGHTLIFTING_H_

#include "hurocup/hurocup.h"

class Weightlifting : public Hurocup::Challenge
{
public:

  enum
  {
    MODE_70_DISK      = 0,
    MODE_90_DISK      = 1,
    MODE_100_DISK     = 2
  };

  enum
  {
    STATE_POSITION_BARBELL      = 0,
    STATE_LIFT_BARBELL_MIDDLE   = 2,
    STATE_WALK_BARBELL_MIDDLE   = 3,
    STATE_LIFT_BARBELL_TOP      = 4,
    STATE_WALK_BARBELL_TOP      = 5
  };

  Weightlifting(Hurocup *hurocup);
  ~Weightlifting() { }

  void loadConfig() { config_.load("hurocup/weightlifting.yaml"); }
  void saveConfig() { config_.save("hurocup/weightlifting.yaml"); }

  void start();
  void stop();

  void visionProcess();
  void process();

private:

  ColorClassifier *white_classifier_;

  float barbell_x_position_;
  float lift_min_tilt_;
  float lift_max_tilt_;

  float white_top_boundary_;
  float white_bottom_boundary_;

  float white_bottom_;
  float white_center_left_;
  float white_center_right_;
  bool white_inside_boundary_;

  float default_min_fx_speed_;
  float default_max_fx_speed_;
  float default_max_a_speed_;

  float middle_min_fx_speed_;
  float middle_max_fx_speed_;
  float middle_max_ly_speed_;
  float middle_max_ry_speed_;
  float middle_max_a_speed_;

  float top_min_fx_speed_;
  float top_max_fx_speed_;
  float top_max_ly_speed_;
  float top_max_ry_speed_;
  float top_max_a_speed_;

  bool state_lift_walk_stop_;
  bool open_gripper_;
};

#endif