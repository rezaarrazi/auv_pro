#ifndef HUROCUP_OBSTACLE_RUN_H
#define HUROCUP_OBSTACLE_RUN_H

#include "hurocup/hurocup.h"
#include "vision/color_classifier.h"

class ObstacleRun : public Hurocup::Challenge
{
public:

  enum
  {
    MODE_PRIORITY_LEFT    = 0,
    MODE_PRIORITY_RIGHT   = 1,
    MODE_NO_PRIORITY      = 2
  };

  enum
  {
    STATE_FORWARD_MOVE    = 0,
    STATE_SIDE_MOVE       = 1,
    STATE_ENTER_GATE      = 2,
    STATE_CRAWL           = 3
  };

  ObstacleRun(Hurocup *hurocup);
  ~ObstacleRun() { }

  void loadConfig() { config_.load("hurocup/obstacle_run.yaml"); }
  void saveConfig() { config_.save("hurocup/obstacle_run.yaml"); }

  void resetMode() { setMode(MODE_NO_PRIORITY); }

  void start();
  void stop();

  void visionProcess();
  void process();

private:

  ColorClassifier *red_classifier_;
  ColorClassifier *blue_classifier_;
  ColorClassifier *yellow_classifier_;

  float left_border_;
  float right_border_;

  float state_forward_tilt_;
  float state_forward_orientataion_;

  float state_side_min_tilt_;
  bool state_side_move_left_;

  float state_enter_min_tilt_;

  bool enable_crawl_;
  bool state_crawl_walk_stop_;
  bool state_crawl_down_;
  bool state_almost_crawl_up_;
  bool state_crawl_up_;
  int state_crawl_count_;
  int state_crawl_max_count_;

  float red_top_boundary_;
  float red_bottom_boundary_;

  float blue_top_boundary_;
  float blue_bottom_boundary_;
  float blue_left_boundary_;
  float blue_right_boundary_;

  bool red_inside_boundary_;
  bool blue_inside_boundary_;
  bool yellow_inside_boundary_;

  float yellow_top_boundary_;
  float yellow_bottom_boundary_;
  float yellow_left_boundary_;
  float yellow_right_boundary_;

  float red_bottom_;
  float red_bottom_left_;
  float red_bottom_right_;
  float red_center_left_;
  float red_center_right_;

  float blue_bottom_;
  float blue_bottom_left_;
  float blue_bottom_right_;
  float blue_left_;
  float blue_right_;
  float blue_center_left_;
  float blue_center_right_;

  float yellow_bottom_;
  float yellow_bottom_left_;
  float yellow_bottom_right_;
  float yellow_left_;
  float yellow_right_;
  float yellow_center_left_;
  float yellow_center_right_;

  float min_fx_speed_;
  float max_fx_speed_;
  float min_bx_speed_;
  float max_bx_speed_;
  float min_ly_speed_;
  float max_ly_speed_;
  float min_ry_speed_;
  float max_ry_speed_;
  float max_a_speed_;
};

#endif