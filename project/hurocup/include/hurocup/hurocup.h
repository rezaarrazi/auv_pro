#ifndef HUROCUP_HUROCUP_H_
#define HUROCUP_HUROCUP_H_

#include "controller.h"
#include "motion.h"
#include "vision.h"
#include "httpd.h"
#include "config.h"
#include <map>

class Hurocup
{
public:

  class Challenge
  {
  public:

    enum
    {
      CHALLENGE_ARCHERY           = 0,
      CHALLENGE_BASKETBALL        = 1,
      CHALLENGE_TRIPLE_JUMP       = 2,
      CHALLENGE_MARATHON          = 3,
      CHALLENGE_OBSTACLE_RUN      = 4,
      CHALLENGE_PENALTY_KICK      = 5,
      CHALLENGE_SPARTAN_RACE      = 6,
      CHALLENGE_SPRINT            = 7,
      CHALLENGE_UNITED_SOCCER     = 8,
      CHALLENGE_WEIGHTLIFTING     = 9,
      CHALLENGE_MINI_DRC          = 10
    };

    Challenge(Hurocup *hurocup, int challenge_type);
    virtual ~Challenge() { }

    std::string getName() { return name_; }
    int getType() { return type_; }

    virtual void loadConfig() { }
    virtual void saveConfig() { }

    bool isActive() { return active_; }

    int getMode() { return current_mode_; }
    void setMode(int mode);
    void nextMode() { setMode(current_mode_ + 1); }
    virtual void resetMode() { setMode(0); }

    void nextState(int next_state);

    virtual void start();
    virtual void stop();

    virtual void visionProcess() { }
    virtual void process();

  protected:

    Hurocup *hurocup_;

    std::string name_;
    int type_;

    Config config_;

    bool active_;
    float active_sec_;

    int current_mode_;
    int mode_count_;

    bool initial_;

    int current_state_;
    int prev_state_;

    bool fallen_getup_;
    bool fallen_;

  };

  enum
  {
    BUTTON_NONE       = 0,
    BUTTON_STOP       = 1,
    BUTTON_START      = 2
  };

  Hurocup();
  ~Hurocup() { }

  Challenge *addChallenge(int challenge_type, Challenge *challenge);
  Challenge *getChallenge(int challenge_type);

  bool isActive() { return active_; }

  void loadArguments(int argc, char **argv);

  void start();
  void stop();

  void process();

  void checkFallen();

  float getDeltaSec() { return delta_sec_; }
  Image *getStreamImage() { return stream_image_; }

  MPU *getMPU() { return mpu_; }

  Robot::Action *getAction() { return action_; }
  Robot::Head *getHead() { return  head_; }
  Robot::Walking *getWalking() { return walking_; }

  Robot::Camera *getCamera() { return camera_; }
  Locomotion *getLocomotion() { return locomotion_; }
  bool open_gripper;

private:

  float selection_tool_box_x_;
  float selection_tool_box_y_;
  float selection_tool_box_size_;
	bool selection_tool_slide_;

  void updateTime();
  void buttonProcess();
  void visionProcess();
  void robotProcess();

  void modeStop();
  void modeInitialize();
  void modeFallen();

  bool active_;

  Challenge *current_challenge_;
  std::map<int, Challenge *> challenges_;

  Robot::LinuxCM730 *linux_cm730_;
  Robot::CM730 *cm730_;

  Robot::MotionManager *motion_manager_;

  MPU *mpu_;

  Robot::Action *action_;
  Robot::Head *head_;
  Robot::Walking *walking_;

  Robot::LinuxMotionTimer *motion_timer_;

  Robot::Camera *camera_;

  Image *stream_image_;
  mjpg_streamer *streamer_;

  Locomotion *locomotion_;

  struct timespec time_prev_;
  struct timespec time_now_;

  float delta_sec_;
  float initialize_time_;
  bool initialized_;

  int button_;

};

#endif