#ifndef HUROCUP_SPRINT_H_
#define HUROCUP_SPRINT_H_

#include "hurocup/hurocup.h"

class Sprint : public Hurocup::Challenge
{
public:

  enum
  {
    MODE_SPRINT             = 0
  };

  enum
  {
    STATE_SPRINT_FORWARD    = 0,
    STATE_SPRINT_BACKWARD   = 1
  };

  Sprint(Hurocup *hurocup);
  ~Sprint() { }

  void loadConfig() { config_.load("hurocup/sprint.yaml"); }
  void saveConfig() { config_.save("hurocup/sprint.yaml"); }

  void start();
  void stop();

  void visionProcess();
  void process();

private:

  float sprint_distance_;
  float deceleration_distance_;
  float min_deceleration_;

  float min_fx_speed_;
  float max_fx_speed_;
  float min_bx_speed_;
  float max_bx_speed_;
  float max_a_speed_;

  float forward_angle_compensation_;
  float backward_angle_compensation_;
  
  int walk_in_position_counting_;
};

#endif