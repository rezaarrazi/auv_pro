#ifndef HUROCUP_UNITED_SOCCER_H_
#define HUROCUP_UNITED_SOCCER_H_

#include "hurocup/hurocup.h"
#include "vision/color_classifier.h"
#include "vision/image.h"

class UnitedSoccer : public Hurocup::Challenge
{
public:

  enum MODE
  {
    MODE_RELEASE_LEFT       = 0,
    MODE_RELEASE_RIGHT      = 1,
    MODE_KICKOFF            = 2,
    MODE_KICKOFF_OPPONENT   = 3
  };

  enum
  {
    STATE_KICKOFF           = 1,
    STATE_RELEASE           = 2,
    STATE_PLAY              = 3,
    STATE_PATROL_POSITION   = 5,
    STATE_FOLLOW_BALL       = 6,
    STATE_LOST_BALL         = 7,
    STATE_DRIBBLE_BALL      = 8,
    STATE_POSITION_BALL     = 9,
    STATE_KICK_BALL         = 10
  };

  UnitedSoccer(Hurocup *hurocup);
  ~UnitedSoccer() { }

  void loadConfig() { config_.load("hurocup/united_soccer.yaml"); }
  void saveConfig() { config_.save("hurocup/united_soccer.yaml"); }

  void start();
  void stop();

  void visionProcess();
  void process();

private:

  ColorClassifier *field_classifier_;
  ColorClassifier *ball_classifier_;

  float field_width_;
  float field_height_;
  float field_circle_diameter_;

  float follow_tilt_;
  float dynamic_kick_;
  bool always_dribble_;

  bool detect_ball_;
  float detect_ball_sec_;
  float lost_ball_sec_;

  float ball_pos_x_;
  float ball_pos_y_;

  float ball_distance_x_;
  float ball_distance_y_;

  float state_kickoff_time_;

  float state_release_time_;
  bool state_release_left_;

  int state_patrol_position_;
  float state_patrol_in_position_time_;

  float state_lost_search_front_time_;
  float state_lost_search_last_ball_time_;

  float state_lost_last_orientation_;
  float state_lost_last_ball_x_;
  float state_lost_last_ball_y_;
  float state_lost_search_rotate_left_;

  bool state_dribble_pivot_finish_;
  float state_dribble_direction_;

  bool state_position_pivot_finish_;
  float state_position_last_ball_x_;
  float state_position_last_ball_y_;
  float state_position_kick_direction_;

  float state_kickoff_x_position_;
  float state_kickoff_y_position_;

  bool state_kick_walk_stop_;

  int right_kick_;
  int left_kick_;
};

#endif