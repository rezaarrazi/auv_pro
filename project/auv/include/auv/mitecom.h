#ifndef SOCCER_MITECOM_H
#define SOCCER_MITECOM_H

#include "network/udp_network.h"
#include "auv/mavlink.h"
#include "auv/auv.h"

#include <stdint.h>
#include <pthread.h>

class Mitecom : public UDPNetwork
{
public:

    static const uint32_t RANGE_CHANNEL         = 0x00000000;
    static const uint32_t RANGE_COGNITION       = 0x00010000;
    static const uint32_t RANGE_CAPABILITIES    = 0x00020000;
    static const uint32_t RANGE_STRATEGIES      = 0x00030000;
    static const uint32_t RANGE_USERDEFINED     = 0x10000000;

    enum
    {
        ROLE_IDLING     = 0,
        ROLE_OTHER      = 1,
        ROLE_STRIKER    = 2,
        ROLE_SUPORTER   = 3,
        ROLE_DEFENDER   = 4,
        ROLE_GOALIE     = 5
    };

    enum
    {
        ACTION_UNDEFINED        = 0,
        ACTION_POSITIONING      = 1,
        ACTION_GOING_TO_BALL    = 2,
        ACTION_TRYING_TO_SCORE  = 3,
        ACTION_WAITING          = 4
    };

    enum
    {
        STATE_INACTIVE  = 0,
        STATE_ACTIVE    = 1,
        STATE_PENALIZED = 2
    };

    enum
    {
        SIDE_UNSPECIFIED    = 0,
        SIDE_LEFT           = 1,
        SIDE_MIDDLE         = 2,
        SIDE_RIGHT          = 3
    };

    enum
    {
        KEY_ROBOT_CHANNEL_PITCH             = RANGE_CHANNEL + 0,
        KEY_ROBOT_CHANNEL_ROLL              = RANGE_CHANNEL + 1,
        KEY_ROBOT_CHANNEL_THROTTLE          = RANGE_CHANNEL + 2,
        KEY_ROBOT_CHANNEL_YAW               = RANGE_CHANNEL + 3,
        KEY_ROBOT_CHANNEL_FORWARD           = RANGE_CHANNEL + 4,
        KEY_ROBOT_CHANNEL_LATERAL           = RANGE_CHANNEL + 5,
    };

    struct KeyValData
    {
        uint32_t key;
        int32_t value;
    };

    struct MessageData
    {
        uint32_t header;
        uint16_t version;
        uint16_t length;
        uint32_t flags;
        KeyValData values[6];

        void set(int id, uint32_t key, int value)
        {
            values[id].key = key;
            values[id].value = (int32_t)value;
        }
    };

private:

    pthread_t thread_;
    bool thread_handler_;

    Auv *auv_;

    static void *threadMethod(void *object);

public:

    Mitecom(Auv *auv);
    ~Mitecom();

    bool start();
    bool stop() { return (thread_handler_ = false); }
    bool isActive() { return thread_handler_; }
    void process();
};

#endif