#ifndef HUROCUP_MANUAL_H_
#define HUROCUP_MANUAL_H_

#include "auv/auv.h"

class Manual : public Auv::Challenge
{
public:

  enum
  {
    MODE_MINI_DRC   = 0
  };

  enum
  {
    STATE_IDLE              = 0,
    STATE_WALK_IN_POSITION  = 1,
    STATE_FORWARD_MOVE      = 2,
    STATE_SIDE_MOVE         = 3,
    STATE_MOTION_VALVE      = 4
  };

  Manual(Auv *auv);
  ~Manual() { }

  void loadConfig() { }
  void saveConfig() { }

  void start();
  void stop();

  void visionProcess();
  void process();

private:

  float state_side_angle_;
  bool state_motion_walk_stop_;
};


#endif