#ifndef ROBOT_H
#define ROBOT_H

#include "algebra.h"

#include <stdint.h>
#include <time.h>
#include <string>

class Mavlink
{
public:
    Mavlink();
    ~Mavlink() { }
    
    static Mavlink* GetInstance();

    int CHANNEL_PITCH;
    int CHANNEL_ROLL;
    int CHANNEL_THROTTLE;
    int CHANNEL_YAW;
    int CHANNEL_FORWARD;
    int CHANNEL_LATERAL;

    static const int CHANNEL_SIGNAL_MAX = 1700;
    static const int CHANNEL_SIGNAL_NETRAL = 1500;
    static const int CHANNEL_SIGNAL_MIN = 1300;

    static const int MANUAL_ACCELERATION = 25;
    static const int MANUAL_DECCELERATION = -50;

private:
    static Mavlink* m_UniqueInstance;
};

#endif