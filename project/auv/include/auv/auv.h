#ifndef AUV_AUV_H_
#define AUV_AUV_H_

#include "vision.h"
#include "httpd.h"
#include "config.h"
#include "auv/mavlink.h"
#include <map>

class Auv
{
public:

  class Challenge
  {
  public:

    enum
    {
      CHALLENGE_QUALIFICATION     = 0,
      CHALLENGE_MISSION           = 1,
      CHALLENGE_MANUAL            = 2
    };

    Challenge(Auv *auv, int challenge_type);
    virtual ~Challenge() { }

    std::string getName() { return name_; }
    int getType() { return type_; }

    virtual void loadConfig() { }
    virtual void saveConfig() { }

    bool isActive() { return active_; }

    int getMode() { return current_mode_; }
    void setMode(int mode);
    void nextMode() { setMode(current_mode_ + 1); }
    virtual void resetMode() { setMode(0); }

    void nextState(int next_state);

    virtual void start();
    virtual void stop();

    virtual void visionProcess() { }
    virtual void process();

  protected:

    Auv *auv_;

    std::string name_;
    int type_;

    Config config_;

    bool active_;
    float active_sec_;

    int current_mode_;
    int mode_count_;

    bool initial_;

    int current_state_;
    int prev_state_;

    bool fallen_getup_;
    bool fallen_;

  };

  enum
  {
    BUTTON_NONE       = 0,
    BUTTON_STOP       = 1,
    BUTTON_START      = 2
  };

  Auv();
  ~Auv() { }

  Challenge *addChallenge(int challenge_type, Challenge *challenge);
  Challenge *getChallenge(int challenge_type);

  bool isActive() { return active_; }

  void loadArguments(int argc, char **argv);

  void start();
  void stop();

  void process();

  void checkFallen();

  float getDeltaSec() { return delta_sec_; }
  Image *getStreamImage() { return stream_image_; }

  Mavlink *getMavlink() { return mavlink_; }

  Robot::Camera *getCamera() { return camera_; }
  bool open_gripper;

private:

  float selection_tool_box_x_;
  float selection_tool_box_y_;
  float selection_tool_box_size_;
	bool selection_tool_slide_;

  void updateTime();
  void buttonProcess();
  void visionProcess();
  void robotProcess();

  void modeStop();
  void modeInitialize();
  void modeFallen();

  bool active_;

  Challenge *current_challenge_;
  std::map<int, Challenge *> challenges_;

  Mavlink *mavlink_;

  Robot::Camera *camera_;

  Image *stream_image_;
  mjpg_streamer *streamer_;

  struct timespec time_prev_;
  struct timespec time_now_;

  float delta_sec_;
  float initialize_time_;
  bool initialized_;

  int button_;

};

#endif