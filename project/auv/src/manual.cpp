#include "auv/manual.h"

#include "cli.h"

Manual::Manual(Auv *auv) : Auv::Challenge(auv, Auv::Challenge::CHALLENGE_MANUAL)
{
  name_ = "manual";
  current_mode_ = MODE_MINI_DRC;
  mode_count_ = 1;
  fallen_getup_ = true;

  state_side_angle_ = 0.0;
  state_motion_walk_stop_ = false;
}

void Manual::start()
{
  Auv::Challenge::start();

  nextState(STATE_IDLE);
}

void Manual::stop()
{
  Auv::Challenge::stop();
}

void Manual::visionProcess()
{
  Auv::Challenge::visionProcess();
}

void Manual::process()
{
  Auv::Challenge::process();

  int key = httpd::popKeyboardKey();
  switch (key)
  {
  case httpd::KEYCODE_W:
    if(auv_->getMavlink()->CHANNEL_FORWARD < auv_->getMavlink()->CHANNEL_SIGNAL_MAX)
      auv_->getMavlink()->CHANNEL_FORWARD += auv_->getMavlink()->MANUAL_ACCELERATION;
    break;
  case httpd::KEYCODE_S:
    if(auv_->getMavlink()->CHANNEL_FORWARD > auv_->getMavlink()->CHANNEL_SIGNAL_MIN)
      auv_->getMavlink()->CHANNEL_FORWARD -= auv_->getMavlink()->MANUAL_ACCELERATION;
    break;
  case httpd::KEYCODE_D:
    if(auv_->getMavlink()->CHANNEL_LATERAL < auv_->getMavlink()->CHANNEL_SIGNAL_MAX)
      auv_->getMavlink()->CHANNEL_LATERAL += auv_->getMavlink()->MANUAL_ACCELERATION;
    break;
  case httpd::KEYCODE_A:
    if(auv_->getMavlink()->CHANNEL_LATERAL > auv_->getMavlink()->CHANNEL_SIGNAL_MIN)
      auv_->getMavlink()->CHANNEL_LATERAL -= auv_->getMavlink()->MANUAL_ACCELERATION;
    break;
  case httpd::KEYCODE_UP_ARROW:
    if(auv_->getMavlink()->CHANNEL_THROTTLE < auv_->getMavlink()->CHANNEL_SIGNAL_MAX)
      auv_->getMavlink()->CHANNEL_THROTTLE += auv_->getMavlink()->MANUAL_ACCELERATION;
    break;
  case httpd::KEYCODE_DOWN_ARROW:
    if(auv_->getMavlink()->CHANNEL_THROTTLE > auv_->getMavlink()->CHANNEL_SIGNAL_MIN)
      auv_->getMavlink()->CHANNEL_THROTTLE -= auv_->getMavlink()->MANUAL_ACCELERATION;
    break;
  case httpd::KEYCODE_RIGHT_ARROW:
    if(auv_->getMavlink()->CHANNEL_YAW < auv_->getMavlink()->CHANNEL_SIGNAL_MAX)
      auv_->getMavlink()->CHANNEL_YAW += auv_->getMavlink()->MANUAL_ACCELERATION;
    break;
  case httpd::KEYCODE_LEFT_ARROW:
    if(auv_->getMavlink()->CHANNEL_YAW > auv_->getMavlink()->CHANNEL_SIGNAL_MIN)
      auv_->getMavlink()->CHANNEL_YAW -= auv_->getMavlink()->MANUAL_ACCELERATION;
    break;
  
  default:
    if(auv_->getMavlink()->CHANNEL_FORWARD > auv_->getMavlink()->CHANNEL_SIGNAL_NETRAL)
      auv_->getMavlink()->CHANNEL_FORWARD += auv_->getMavlink()->MANUAL_DECCELERATION;
    if(auv_->getMavlink()->CHANNEL_FORWARD < auv_->getMavlink()->CHANNEL_SIGNAL_NETRAL)
      auv_->getMavlink()->CHANNEL_FORWARD -= auv_->getMavlink()->MANUAL_DECCELERATION;
    
    if(auv_->getMavlink()->CHANNEL_LATERAL > auv_->getMavlink()->CHANNEL_SIGNAL_NETRAL)
      auv_->getMavlink()->CHANNEL_LATERAL += auv_->getMavlink()->MANUAL_DECCELERATION;
    if(auv_->getMavlink()->CHANNEL_LATERAL < auv_->getMavlink()->CHANNEL_SIGNAL_NETRAL)
      auv_->getMavlink()->CHANNEL_LATERAL -= auv_->getMavlink()->MANUAL_DECCELERATION;

    if(auv_->getMavlink()->CHANNEL_THROTTLE > auv_->getMavlink()->CHANNEL_SIGNAL_NETRAL)
      auv_->getMavlink()->CHANNEL_THROTTLE += auv_->getMavlink()->MANUAL_DECCELERATION;
    if(auv_->getMavlink()->CHANNEL_THROTTLE < auv_->getMavlink()->CHANNEL_SIGNAL_NETRAL)
      auv_->getMavlink()->CHANNEL_THROTTLE -= auv_->getMavlink()->MANUAL_DECCELERATION;

    if(auv_->getMavlink()->CHANNEL_YAW > auv_->getMavlink()->CHANNEL_SIGNAL_NETRAL)
      auv_->getMavlink()->CHANNEL_YAW += auv_->getMavlink()->MANUAL_DECCELERATION;
    if(auv_->getMavlink()->CHANNEL_YAW < auv_->getMavlink()->CHANNEL_SIGNAL_NETRAL)
      auv_->getMavlink()->CHANNEL_YAW -= auv_->getMavlink()->MANUAL_DECCELERATION;

    httpd::pushKeyboardKey(key);
    break;
  }
}