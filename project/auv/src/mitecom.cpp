#include "auv/mitecom.h"
#include "binary.h"

#include <stdio.h>
#include <unistd.h>

#define MITECOM_BROADCAST_PORT 	4003
#define MITECOM_RECIEVE_PORT 	4005

Mitecom::Mitecom(Auv *auv) : UDPNetwork()
{
	thread_handler_ = false;
	auv_ = auv;

	openPort(4005, MITECOM_BROADCAST_PORT);
	// openPort(MITECOM_BROADCAST_PORT);
}

Mitecom::~Mitecom()
{
	thread_handler_ = false;
}

bool Mitecom::start()
{
	if (thread_handler_ == true)
		return false;

	thread_handler_ = true;
	return pthread_create(&thread_, NULL, &threadMethod, this);
}

void Mitecom::process()
{
	MessageData message_data;
	MessageData return_data;

	while (thread_handler_)
	{
		for (int j = 0; j < 5; j++)
		{
			if (receive(&message_data, sizeof(message_data)) > 0)
			{
				if (getEnableReceive() && message_data.header == 0x4354584D)
				{
					// printf("recieved\n");
				}
			}
		}
		
		if (getEnableBroadcast())
		{
			// printf("broadcast\n");
			return_data.header = 0x4354584D;
			return_data.version = 1;

			int i = 0;

			return_data.set(i++, KEY_ROBOT_CHANNEL_PITCH, 		auv_->getMavlink()->CHANNEL_PITCH);
			return_data.set(i++, KEY_ROBOT_CHANNEL_ROLL, 		auv_->getMavlink()->CHANNEL_ROLL);
			return_data.set(i++, KEY_ROBOT_CHANNEL_THROTTLE, 	auv_->getMavlink()->CHANNEL_THROTTLE);
			return_data.set(i++, KEY_ROBOT_CHANNEL_YAW, 		auv_->getMavlink()->CHANNEL_YAW);
			return_data.set(i++, KEY_ROBOT_CHANNEL_FORWARD, 	auv_->getMavlink()->CHANNEL_FORWARD);
			return_data.set(i++, KEY_ROBOT_CHANNEL_LATERAL, 	auv_->getMavlink()->CHANNEL_LATERAL);

			return_data.length = i;

			broadcast(&return_data, sizeof(return_data));
		}

		usleep(100 * 1000);
		// Robot::MotionStatus::LED_EYE = Robot::CM730::MakeColor(0, 255, 255);
		// cm730_->WriteWord(Robot::CM730::P_LED_EYE_L, Robot::MotionStatus::LED_EYE, NULL);
	}
}

void *Mitecom::threadMethod(void *object)
{
    reinterpret_cast<Mitecom *>(object)->process();
    return 0;
}
