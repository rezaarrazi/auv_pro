#include "auv/mavlink.h"

#include <math.h>

Mavlink *Mavlink::m_UniqueInstance = nullptr;

Mavlink::Mavlink()
{
    CHANNEL_PITCH       = CHANNEL_SIGNAL_NETRAL;
    CHANNEL_ROLL        = CHANNEL_SIGNAL_NETRAL;
    CHANNEL_THROTTLE    = CHANNEL_SIGNAL_NETRAL;
    CHANNEL_YAW         = CHANNEL_SIGNAL_NETRAL;
    CHANNEL_FORWARD     = CHANNEL_SIGNAL_NETRAL;
    CHANNEL_LATERAL     = CHANNEL_SIGNAL_NETRAL;
}

Mavlink *Mavlink::GetInstance()
{
  if (m_UniqueInstance != nullptr)
    return m_UniqueInstance;

  return (m_UniqueInstance = new Mavlink());
}