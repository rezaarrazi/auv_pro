#include "auv/auv.h"
#include "auv/qualification.h"
#include "auv/manual.h"
#include "auv/mitecom.h"

#include "binary.h"
#include "cli.h"

#include <iostream>

Auv::Challenge::Challenge(Auv *auv, int challenge_type)
{
  auv_ = auv;
  type_ = challenge_type;

  name_ = "default";

  active_ = false;
  active_sec_ = 0.0;

  current_mode_ = -1;
  mode_count_ = -1;

  initial_ = false;

  current_state_ = -1;
  prev_state_ = -1;
}

void Auv::Challenge::setMode(int mode)
{
  current_mode_ = mode % mode_count_;
}

void Auv::Challenge::nextState(int next_state)
{
  prev_state_ = current_state_;
  current_state_ = next_state;

  initial_ = true;
}

void Auv::Challenge::start()
{
  cli::print("challenge start");
  active_ = true;

  prev_state_ = -1;
  current_state_ = -1;

  // set random seed to current time
  std::srand(std::time(0));

  // load config
  auv_->camera_->loadConfig();

  // force mpu calibrated
  auv_->initialized_ = true;

  loadConfig();
}

void Auv::Challenge::stop()
{
  active_ = false;
  active_sec_ = 0.0;
}

void Auv::Challenge::process()
{
  cli::clear();
  cli::printLine();

  cli::printParameterBlock("challenge", bin::uppercased(name_));
  cli::printParameterBlock("active", (active_sec_ += auv_->delta_sec_));
  cli::endLine();

  cli::printParameterBlock("CHANNEL_FORWARD", (float)auv_->mavlink_->CHANNEL_FORWARD);
  cli::printParameterBlock("CHANNEL_LATERAL", (float)auv_->mavlink_->CHANNEL_LATERAL);
  cli::printParameterBlock("CHANNEL_THROTTLE", (float)auv_->mavlink_->CHANNEL_THROTTLE);
  cli::printParameterBlock("CHANNEL_YAW", (float)auv_->mavlink_->CHANNEL_YAW);
  cli::endLine();

  cli::printLine();
  cli::endLine();
}

Auv::Auv()
{
  active_ = false;

  current_challenge_ = nullptr;

  mavlink_ = nullptr;

  camera_ = nullptr;

  stream_image_ = nullptr;
  streamer_ = nullptr;
}

void Auv::loadArguments(int argc, char **argv)
{
  // httpd::GetInstance()->game_mode = Challenge::CHALLENGE_UNITED_SOCCER;
  // for (int i = 0; i < argc; i++)
  // {
  //   for (const auto &keyval : challenges_)
  //   {
  //     if (keyval.second->getName() == argv[i])
  //     {
  //       current_challenge_ = keyval.second;
  //       httpd::GetInstance()->game_mode = current_challenge_->getType();
  //     }
  //   }
  // }
}

void Auv::start()
{
  cli::printBlock("initialize");

  cli::print("mavlink start..");
  mavlink_ = Mavlink::GetInstance();

  cli::print("camera start..");
	camera_ = Robot::Camera::getInstance();
  camera_->openPort();
	camera_->captureFrame();

	cli::print("stream start..");
	stream_image_ = new Image(camera_->getWidth(), camera_->getHeight(), Image::RGB_PIXEL_SIZE);
	streamer_ = new mjpg_streamer(camera_->getWidth(), camera_->getHeight());

	cli::print("qualification start..");
  addChallenge(Challenge::CHALLENGE_QUALIFICATION, (Challenge *)new Qualification(this));

  cli::print("manual start..");
  addChallenge(Challenge::CHALLENGE_MANUAL, (Challenge *)new Manual(this));

  cli::print("mitecom start..");
	Mitecom *mitecom_ = new Mitecom(this);
	mitecom_->start();

  httpd::color_classifier_ = ColorClassifier::getInstance(ColorClassifier::CLASSIFIER_TYPE_BALL);

	clock_gettime(CLOCK_MONOTONIC, &time_now_);

  delta_sec_ = 0.0;
  initialize_time_ = 0.0;
  initialized_ = false;

  selection_tool_box_x_ = camera_->getCenterX();
  selection_tool_box_y_ = camera_->getCenterY();
  selection_tool_box_size_ = 64;

  cli::print("done");

  active_ = true;
}

Auv::Challenge *Auv::addChallenge(int challenge_type, Auv::Challenge *challenge)
{
  if (challenges_.find(challenge_type) != challenges_.end())
    challenges_.erase(challenge_type);

  challenges_.emplace(challenge_type, challenge);
  return challenge;
}

Auv::Challenge *Auv::getChallenge(int challenge_type)
{
  if (challenges_.find(challenge_type) == challenges_.end())
    addChallenge(challenge_type, new Challenge(this, challenge_type));

  return challenges_[challenge_type];
}

void Auv::process()
{
  // update delta time
  time_prev_ = time_now_;
  clock_gettime(CLOCK_MONOTONIC, &time_now_);

  // update delta sec
  delta_sec_ = (time_now_.tv_sec - time_prev_.tv_sec);
  delta_sec_ += (float)(time_now_.tv_nsec - time_prev_.tv_nsec) / 1000000000;

  // if no challenge exist, default to 0
  if (current_challenge_ == nullptr)
    current_challenge_ = getChallenge(0);

  // switch challeng/mode
  if (current_challenge_->getType() != httpd::GetInstance()->game_mode)
  {
    if (current_challenge_->isActive())
      current_challenge_->stop();

    current_challenge_ = getChallenge(httpd::GetInstance()->game_mode);
  }

  // button_ = MotionStatus::BUTTON;
  button_ = BUTTON_NONE;
  if (button_ == BUTTON_NONE)
  {
    int key = httpd::popKeyboardKey();
    // cli::printParameter("key", key);
    switch (key)
    {
    case httpd::KEYCODE_ESCAPE: button_ = BUTTON_STOP; break;
    case httpd::KEYCODE_ENTER: button_ = BUTTON_START; break;
    default: httpd::pushKeyboardKey(key); break;
    }
  }

  switch (button_)
  {
  case BUTTON_STOP:
    {
      cli::clear();
      cli::print("button stop");

      if (current_challenge_->isActive() == false)
        current_challenge_->nextMode();
      else
        current_challenge_->resetMode();

      current_challenge_->stop();

      cli::printParameter("mode", current_challenge_->getMode());

      break;
    }

  case BUTTON_START:
    {
      cli::clear();
      cli::print("button start");

      current_challenge_->start();

      break;
    }
  }

  // vision process
  if (camera_->isPortExist())
  {
    // vision capture
    camera_->captureFrame();
    stream_image_->fromMat(camera_->getBuffer()->m_RGBInput.clone());

    // box calibration
    if (httpd::GetInstance()->selection_tool_mode)
    {
      cv::Mat img = Camera::getInstance()->getBuffer()->m_RGBInput.clone();

      int x = selection_tool_box_x_ - selection_tool_box_size_;
      int y = selection_tool_box_y_ - selection_tool_box_size_;
      cv::Rect selection_tool_box(x, y, selection_tool_box_size_, selection_tool_box_size_);

      int key = httpd::popKeyboardKey();
      switch (key)
      {
        case httpd::KEYCODE_SHIFT:
          selection_tool_slide_ = !selection_tool_slide_;
          break;

        case httpd::KEYCODE_C:
        case httpd::KEYCODE_BRACKET_LEFT:
        case httpd::KEYCODE_BRACKET_RIGHT:
        {
          cv::Mat croppedImage = img(selection_tool_box);
          cvtColor(croppedImage, croppedImage, CV_RGB2HSV);

          int min_hue = 255;
          int max_hue = 0;

          int min_saturation = 255;
          int max_saturation = 0;

          int min_value = 255;
          int max_value = 0;

          ColorClassifier *classifier = httpd::color_classifier_;
          if (key != httpd::KEYCODE_C)
          {
            min_hue = ((classifier->getHue() - classifier->getHueTolerance()) * 255) / 360;
            if (min_hue < 0)
            {
              min_hue = 0;
            }
            max_hue = ((classifier->getHue() + classifier->getHueTolerance()) * 255) / 360;
            if (max_hue > 360)
            {
              max_hue = 0;
            }
            min_saturation = (classifier->getMinSaturation() * 255) / 100;
            max_saturation = (classifier->getMaxSaturation() * 255) / 100;

            min_value = (classifier->getMinValue() * 255) / 100;
            max_value = (classifier->getMaxValue() * 255) / 100;
          }

          for (int i = 1; i < croppedImage.rows; i++)
          {
            for (int j = 1; j < croppedImage.cols; j++)
            {
              cv::Vec3b hsv = croppedImage.at<cv::Vec3b>(i, j);

              if (key != httpd::KEYCODE_BRACKET_LEFT)
              {
                min_hue = alg::minValue(min_hue, hsv.val[0]);
                max_hue = alg::maxValue(max_hue, hsv.val[0]);

                min_saturation = alg::minValue(min_saturation, hsv.val[1]);
                max_saturation = alg::maxValue(max_saturation, hsv.val[1]);

                min_value = alg::minValue(min_value, hsv.val[2]);
                max_value = alg::maxValue(max_value, hsv.val[2]);
              }
              else
              {
                if(hsv.val[0] < ((min_hue + max_hue) / 2))
                {
                  min_hue = (hsv.val[0] >= min_hue) ? (hsv.val[0] + 1) : min_hue;
                }
                else
                {
                  max_hue = (hsv.val[0] <= max_hue) ? (hsv.val[0] - 1) : max_hue;
                }

                if(hsv.val[1] < ((min_saturation + max_saturation) / 2))
                {
                  min_saturation = (hsv.val[1] >= min_saturation) ? (hsv.val[1] + 1) : min_saturation;
                }
                else
                {
                  max_saturation = (hsv.val[1] <= max_saturation) ? (hsv.val[1] - 1) : max_saturation;
                }

                if(hsv.val[2] < ((min_value + max_value) / 2))
                {
                  min_value = (hsv.val[2] >= min_value) ? (hsv.val[2] + 1) : min_value;
                }
                else
                {
                  max_value = (hsv.val[2] <= max_value) ? (hsv.val[2] - 1) : max_value;
                }
              }
            }
          }

          float hue = (((min_hue + max_hue) / 2) * 360) / 255;
          float hue_tolerance = ((max_hue * 360) / 255) - hue;

          min_saturation = (min_saturation * 100) / 255;
          max_saturation = (max_saturation * 100) / 255;

          min_value = (min_value * 100) / 255;
          max_value = (max_value * 100) / 255;

          httpd::color_classifier_->setHue(hue);
          httpd::color_classifier_->setHueTolerance(hue_tolerance + 10);
          httpd::color_classifier_->setMinSaturation(min_saturation - 10);
          httpd::color_classifier_->setMaxSaturation(max_saturation + 10);
          httpd::color_classifier_->setMinValue(min_value - 10);
          httpd::color_classifier_->setMaxValue(max_value + 10);
          break;
        }

        case httpd::KEYCODE_COMMA:
          selection_tool_box_size_ = alg::maxValue(selection_tool_box_size_ - 8, 8);
          break;

        case httpd::KEYCODE_PERIOD:
          selection_tool_box_size_ += 16;
          if (selection_tool_box_size_ > selection_tool_box_x_)
            selection_tool_box_size_ = selection_tool_box_x_;
          if (selection_tool_box_size_ > (camera_->getWidth() - selection_tool_box_x_))
            selection_tool_box_size_ = camera_->getWidth() - selection_tool_box_x_;
          if (selection_tool_box_size_ > selection_tool_box_y_)
            selection_tool_box_size_ = selection_tool_box_y_;
          if (selection_tool_box_size_ > (camera_->getHeight() - selection_tool_box_y_))
            selection_tool_box_size_ = camera_->getHeight() - selection_tool_box_y_;
          break;

        case httpd::KEYCODE_W:
          selection_tool_box_y_ -= (selection_tool_slide_) ? 16 : 4;
          if (selection_tool_box_y_ < selection_tool_box_size_)
            selection_tool_box_y_ = selection_tool_box_size_;
          break;

        case httpd::KEYCODE_S:
          selection_tool_box_y_ += (selection_tool_slide_) ? 16 : 4;
          if (selection_tool_box_y_ > (camera_->getHeight() * 2) - selection_tool_box_size_)
            selection_tool_box_y_ = camera_->getHeight() - selection_tool_box_size_;
          break;

        case httpd::KEYCODE_A:
          selection_tool_box_x_ -= (selection_tool_slide_) ? 16 : 4;
          if (selection_tool_box_x_ < selection_tool_box_size_)
            selection_tool_box_x_ = selection_tool_box_size_;
          break;

        case httpd::KEYCODE_D:
          selection_tool_box_x_ += (selection_tool_slide_) ? 16 : 4;
          if (selection_tool_box_x_ > (camera_->getWidth() * 2) - selection_tool_box_size_)
            selection_tool_box_x_ = camera_->getWidth() - selection_tool_box_size_;
          break;

        case httpd::KEYCODE_R:
          selection_tool_box_x_ = camera_->getCenterX();
          selection_tool_box_y_ = camera_->getCenterX();
          selection_tool_box_size_ = 64;
          break;

        default:
        httpd::pushKeyboardKey(key);
        break;
      }

      current_challenge_->visionProcess();

      Rects selection_tool_rects(selection_tool_box);
      cv::rectangle(img, selection_tool_box, cv::Scalar( 0, 0, 255 ), 2, 1 );
      stream_image_->filterMat(selection_tool_rects.getBinaryMatLine(img.size(), 2), 255, 0, 0);
    }
    else
    {
      current_challenge_->visionProcess();
    }
  }
  else
  {
    // send empty frame
    cli::print("frame is empty");
    cv::Mat empty_frame(camera_->getBuffer()->m_RGBInput.size(), CV_8UC3);
    empty_frame = cv::Scalar(0);

    stream_image_->fromMat(empty_frame);
  }

  // stream image
  streamer_->send_image(stream_image_);
  // robot process
  if (!initialized_)
  {
    // cli::clear();

    // cli::printParameter("initialize", (initialize_time_ += delta_sec_));

    // if (mpu_->calibrated_)
    // {
    //   cli::print("done");
      initialized_ = true;

      current_challenge_->resetMode();
    // }
  }
  else if (current_challenge_->isActive())
  {
    current_challenge_->process();
  }
}